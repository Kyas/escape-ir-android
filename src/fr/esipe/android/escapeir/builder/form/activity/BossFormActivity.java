package fr.esipe.android.escapeir.builder.form.activity;

import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;
import fr.esipe.android.escapeir.R;
import fr.esipe.android.escapeir.api.Boss;
import fr.esipe.android.escapeir.api.StartPositionEnum;
import fr.esipe.android.escapeir.builder.DataInterface;
import fr.esipe.android.escapeir.builder.form.dialog.DrawDialog;

/**
 * Class BossFormActivity Represent Boss Form to fill out in order to add one
 * boss
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 */
public class BossFormActivity extends Activity implements DataInterface {

	protected Boss boss;
	private ImageView imageButtonShip1;
	private ImageView imageButtonShip2;
	private ImageView imageButtonShip3;
	private ImageView buttonSave;
	private EditText editTextLife;
	private EditText editTextShoot;
	private Spinner spinnerPosition;
	private ImageView imageButtonWeapon1;
	private ImageView imageButtonWeapon2;
	private ImageView imageButtonWeapon3;
	private ImageView imageButtonWeapon4;
	private ArrayList<Point> listPoint = new ArrayList<Point>();

	private int nbLife;
	private int nbDuration;

	private StartPositionEnum startPosition = StartPositionEnum.TOPLEFT;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.builder_form_boss);

		// Hide the virtual keyboard.
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		imageButtonShip1 = (ImageView) findViewById(R.id.imageView1);
		imageButtonShip2 = (ImageView) findViewById(R.id.imageView2);
		imageButtonShip3 = (ImageView) findViewById(R.id.imageView3);
		imageButtonWeapon1 = (ImageView) findViewById(R.id.imageView4);
		imageButtonWeapon2 = (ImageView) findViewById(R.id.imageView5);
		imageButtonWeapon3 = (ImageView) findViewById(R.id.imageView6);
		imageButtonWeapon4 = (ImageView) findViewById(R.id.imageView7);
		editTextLife = (EditText) findViewById(R.id.editText4);
		editTextShoot = (EditText) findViewById(R.id.editText5);
		buttonSave = (ImageView) findViewById(R.id.imageView9);
		spinnerPosition = (Spinner) findViewById(R.id.spinner1);

		boss = CurrentLevel.getCurrentLevel().level.getBoss();
		load();

		imageButtonShip1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				imageButtonShip1.setImageResource(R.drawable.boss1_clicked);
				imageButtonShip2.setImageResource(R.drawable.boss2);
				imageButtonShip3.setImageResource(R.drawable.boss3);
				imageButtonShip1.setSelected(true);
				imageButtonShip2.setSelected(false);
				imageButtonShip3.setSelected(false);
			}
		});

		imageButtonShip2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				imageButtonShip1.setImageResource(R.drawable.boss1);
				imageButtonShip2.setImageResource(R.drawable.boss2_clicked);
				imageButtonShip3.setImageResource(R.drawable.boss3);
				imageButtonShip1.setSelected(false);
				imageButtonShip2.setSelected(true);
				imageButtonShip3.setSelected(false);
			}
		});

		imageButtonShip3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				imageButtonShip1.setImageResource(R.drawable.boss1);
				imageButtonShip2.setImageResource(R.drawable.boss2);
				imageButtonShip3.setImageResource(R.drawable.boss3_clicked);
				imageButtonShip1.setSelected(false);
				imageButtonShip2.setSelected(false);
				imageButtonShip3.setSelected(true);
			}
		});

		imageButtonWeapon1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (!imageButtonWeapon1.isSelected()) {
					imageButtonWeapon1
							.setImageResource(R.drawable.icon_missile_clicked);
					imageButtonWeapon1.setSelected(true);
				} else {
					imageButtonWeapon1
							.setImageResource(R.drawable.icon_missile);
					imageButtonWeapon1.setSelected(false);
				}
			}
		});

		imageButtonWeapon2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (!imageButtonWeapon2.isSelected()) {
					imageButtonWeapon2
							.setImageResource(R.drawable.icon_fireball_clicked);
					imageButtonWeapon2.setSelected(true);
				} else {
					imageButtonWeapon2
							.setImageResource(R.drawable.icon_fireball);
					imageButtonWeapon2.setSelected(false);
				}
			}
		});

		imageButtonWeapon3.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (!imageButtonWeapon3.isSelected()) {
					imageButtonWeapon3
							.setImageResource(R.drawable.icon_shiboleet_clicked);
					imageButtonWeapon3.setSelected(true);
				} else {
					imageButtonWeapon3
							.setImageResource(R.drawable.icon_shiboleet);
					imageButtonWeapon3.setSelected(false);
				}
			}
		});

		imageButtonWeapon4.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (!imageButtonWeapon4.isSelected()) {
					imageButtonWeapon4.setImageResource(R.drawable.icon_laser);
					imageButtonWeapon4.setSelected(true);
				} else {
					imageButtonWeapon4.setImageResource(R.drawable.icon_laser);
					imageButtonWeapon4.setSelected(false);
				}
			}
		});

		buttonSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (checkSave()) {
					save();
					finish();
				}
			}
		});

		Button movementButton = (Button) findViewById(R.id.builder_form_boss_movement);

		final Activity act = this;
		movementButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				listPoint = DrawDialog.popup_draw(act, getApplicationContext());
				Toast.makeText(act, "Movement add", Toast.LENGTH_SHORT).show();
			}
		});

		spinnerPosition = (Spinner) findViewById(R.id.spinner1);
		spinnerPosition.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {

				startPosition.setValue(arg0.getAdapter().getItem(arg2));
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}
		});
	}

	@SuppressWarnings("static-access")
	@Override
	public void load() {
		if (boss != null) {
			int resource = boss.getPath();
			if (resource == R.drawable.boss1) {
				imageButtonShip1.setImageResource(R.drawable.boss1_clicked);
				imageButtonShip2.setImageResource(R.drawable.boss2);
				imageButtonShip3.setImageResource(R.drawable.boss3);
				imageButtonShip1.setSelected(true);
				imageButtonShip2.setSelected(false);
				imageButtonShip3.setSelected(false);
			} else if (resource == R.drawable.boss2) {
				imageButtonShip1.setImageResource(R.drawable.boss1);
				imageButtonShip2.setImageResource(R.drawable.boss2_clicked);
				imageButtonShip3.setImageResource(R.drawable.boss3);
				imageButtonShip1.setSelected(false);
				imageButtonShip2.setSelected(true);
				imageButtonShip3.setSelected(false);
			} else if (resource == R.drawable.boss3) {
				imageButtonShip1.setImageResource(R.drawable.boss1);
				imageButtonShip2.setImageResource(R.drawable.boss2);
				imageButtonShip3.setImageResource(R.drawable.boss3_clicked);
				imageButtonShip1.setSelected(false);
				imageButtonShip2.setSelected(false);
				imageButtonShip3.setSelected(true);
			}
			editTextLife.setText(boss.getLife() + "");
			editTextShoot.setText(boss.getShoot() + "");
			listPoint = boss.getMovement();
			boolean[] weapons = boss.getWeapons();
			if (weapons != null) {

				if (weapons[0] == true) {
					imageButtonWeapon1
							.setImageResource(R.drawable.icon_missile_clicked);
					imageButtonWeapon1.setSelected(true);
				}
				if (weapons[1] == true) {
					imageButtonWeapon2
							.setImageResource(R.drawable.icon_fireball_clicked);
					imageButtonWeapon2.setSelected(true);
				}
				if (weapons[2] == true) {
					imageButtonWeapon3
							.setImageResource(R.drawable.icon_shiboleet_clicked);
					imageButtonWeapon3.setSelected(true);
				}
				if (weapons[3] == true) {
					imageButtonWeapon4
							.setImageResource(R.drawable.icon_laser_clicked);
					imageButtonWeapon4.setSelected(true);
				}

			}

			initSpinner();
		}
	}

	/**
	 * Init spinner
	 */
	public void initSpinner() {
		if (startPosition.getValue().equals("top - left")) {
			spinnerPosition.setSelection(0);
		} else if (startPosition.getValue().equals("top - center")) {
			spinnerPosition.setSelection(1);
		} else if (startPosition.getValue().equals("top - right")) {
			spinnerPosition.setSelection(2);
		} else if (startPosition.getValue().equals("center - left")) {
			spinnerPosition.setSelection(3);
		} else if (startPosition.getValue().equals("center")) {
			spinnerPosition.setSelection(4);

		} else if (startPosition.getValue().equals("center - right")) {
			spinnerPosition.setSelection(5);
		} else if (startPosition.getValue().equals("bottom - left")) {
			spinnerPosition.setSelection(6);
		} else if (startPosition.getValue().equals("bottom - center")) {
			spinnerPosition.setSelection(7);
		} else {
			spinnerPosition.setSelection(8);

		}

	}

	@Override
	public void save() {
		if (listPoint == null || listPoint.size() == 0) {
			listPoint = new ArrayList<Point>();
			listPoint.add(new Point(0, 0));
		}
		boolean[] weapons = new boolean[4];
		if (imageButtonWeapon1.isSelected())
			weapons[0] = true;
		if (imageButtonWeapon2.isSelected())
			weapons[1] = true;
		if (imageButtonWeapon3.isSelected())
			weapons[2] = true;
		if (imageButtonWeapon4.isSelected())
			weapons[3] = true;
		boss = new Boss("Boss", getPath(), nbLife, nbDuration, weapons);
		boss.setMovement(listPoint);
		CurrentLevel.getCurrentLevel().level.setBoss(boss);

	}

	/**
	 * 
	 * @return spaceship Path
	 */
	private int getPath() {
		if (imageButtonShip1.isSelected()) {
			return R.drawable.boss1;
		} else if (imageButtonShip2.isSelected()) {
			return R.drawable.boss2;
		} else
			return R.drawable.boss3;
	}

	/**
	 * Check if the user has done correctly the form.
	 * 
	 * @return <code>true</code> if all is filled, </code>false</code>
	 *         otherwise.
	 */
	public boolean checkSave() {
		try {
			nbLife = (int) Integer.parseInt(editTextLife.getText().toString());
			nbDuration = (int) Integer.parseInt(editTextShoot.getText()
					.toString());
			int nbweapon = 0;
			if (getPath() > 0 && nbLife >= 1 && nbDuration > 0) {
				if (imageButtonWeapon1.isSelected()) {

					nbweapon++;
				}
				if (imageButtonWeapon2.isSelected()) {

					nbweapon++;
				}
				if (imageButtonWeapon3.isSelected()) {

					nbweapon++;
				}
				if (imageButtonWeapon4.isSelected()) {
					nbweapon++;
				}
				if (nbweapon != 0) {
					return true;
				}
			}
			Toast.makeText(this, "Your form is invalid !", Toast.LENGTH_SHORT)
					.show();
			return false;
		} catch (Exception e) {
			Toast.makeText(this, "Your form is invalid !", Toast.LENGTH_SHORT)
					.show();
			return false;
		}

	}

}
