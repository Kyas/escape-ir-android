package fr.esipe.android.escapeir.builder.form.activity;

import java.util.ArrayList;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import fr.esipe.android.escapeir.R;
import fr.esipe.android.escapeir.api.Enemy;
import fr.esipe.android.escapeir.api.Scenario;
import fr.esipe.android.escapeir.api.Scenario.TimeEnemy;
import fr.esipe.android.escapeir.builder.DataInterface;
import fr.esipe.android.escapeir.builder.form.adapter.ArrayScenarioAdapter;

/**
 * Class ScenarioFormActivity Represent Scenario Form to fill out
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 */
public class ScenarioFormActivity extends Activity implements DataInterface {

	private static DisplayMetrics metrics = new DisplayMetrics();
	private static int SCREEN_WIDTH;
	private static int SCREEN_HEIGHT;
	private Spinner spinner;
	private LinearLayout linearLayout;
	private Scenario scenario;
	private Button button;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		SCREEN_HEIGHT = metrics.heightPixels;
		SCREEN_WIDTH = metrics.widthPixels;
		setContentView(R.layout.builder_form_scenario);

		Toast.makeText(getApplicationContext(),
				"To add an enemy, make a click long", Toast.LENGTH_SHORT)
				.show();

		scenario = CurrentLevel.getCurrentLevel().level.getScenario();
		linearLayout = (LinearLayout) findViewById(R.id.linearLayout_scenario);
		spinner = (Spinner) findViewById(R.id.builder_form_scenario_last);
		button = (Button) findViewById(R.id.button1);
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				save();
				finish();

			}
		});

		
		spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				scenario.setTime(calculNumber((String) arg0.getAdapter()
						.getItem(arg2)));
				updateScrollview(scenario.getTime());
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}
		});
		load();
	}

	/**
	 * update the scrollView with the time spinner
	 * @param time
	 */
	private void updateScrollview(long time) {
		long nbSlot = time / 10;
		linearLayout.removeAllViews();
		linearLayout.setMinimumHeight(SCREEN_HEIGHT);
		linearLayout.setMinimumWidth(SCREEN_WIDTH);
		int compteur = 0;
		final ArrayList<TimeEnemy> al = scenario.getPlay();

		for (int i = 0; i <= nbSlot; i++) {
			final int indice = i;
			LinearLayout l = new LinearLayout(getApplicationContext());
			l.setOrientation(LinearLayout.VERTICAL);
			l.setMinimumWidth(SCREEN_WIDTH);
			l.setOnLongClickListener(new OnLongClickListener() {

				@Override
				public boolean onLongClick(View arg0) {
					// TODO Auto-generated method stub
					popupListEnemy(indice * 10);
					return true;
				}
			});

			TextView t1 = new TextView(getApplicationContext());
			t1.setText(((i) * 10) + " secondes");
			t1.setTextSize(12);

			t1.setOnLongClickListener(new OnLongClickListener() {

				@Override
				public boolean onLongClick(View arg0) {
					// TODO Auto-generated method stub
					popupListEnemy(indice * 10);
					return true;
				}
			});
			l.addView(t1);

			ScrollView sV = new ScrollView(getApplicationContext());
			sV.setOnLongClickListener(new OnLongClickListener() {

				@Override
				public boolean onLongClick(View arg0) {
					// TODO Auto-generated method stub
					popupListEnemy(indice * 10);
					return true;
				}
			});
			LinearLayout l2 = new LinearLayout(getApplicationContext());

			l2.setMinimumHeight(SCREEN_HEIGHT / 5);
			l2.setOrientation(LinearLayout.HORIZONTAL);
			l2.setOnLongClickListener(new OnLongClickListener() {

				@Override
				public boolean onLongClick(View arg0) {
					popupListEnemy(indice * 10);
					return true;
				}
			});

			while (al.size() > compteur
					&& al.get(compteur).getTime() == (i * 10)) {
				LinearLayout l3 = new LinearLayout(getApplicationContext());
				l3.setOrientation(LinearLayout.VERTICAL);
				l3.setOnLongClickListener(new OnLongClickListener() {

					@Override
					public boolean onLongClick(View arg0) {
						// TODO Auto-generated method stub
						popupListEnemy(indice * 10);
						return true;
					}
				});
				l3.setPadding(5, 5, 5, 5);

				TextView t = new TextView(getApplicationContext());
				t.setText(al.get(compteur).getEnemy().getName());
				t.setTextSize(12);

				ImageView imageView = new ImageView(getApplicationContext());
				imageView.setImageResource(al.get(compteur).getEnemy()
						.getPath());
				final int compte = compteur;
				imageView.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						popup_delete(al.get(compte));
					}
				});

				l3.addView(imageView);
				l3.addView(t);
				l2.addView(l3);

				compteur++;
			}

			sV.addView(l2);
			l.addView(sV);
			linearLayout.addView(l);
		}
	}

	/**
	 * Initialize spinner
	 */
	public void initSpinner() {
		if (scenario.getTime() == 30) {
			spinner.setSelection(0);
		} else if (scenario.getTime() == 60) {
			spinner.setSelection(1);
		} else if (scenario.getTime() == 90) {
			spinner.setSelection(2);
		} else if (scenario.getTime() == 120) {
			spinner.setSelection(3);
		} else if (scenario.getTime() == 150) {
			spinner.setSelection(4);

		} else {
			spinner.setSelection(5);
		}

	}

	private int calculNumber(String time) {
		if (time.equals("30 sec")) {
			return 30;
		} else if (time.equals("1 min")) {
			return 60;
		} else if (time.equals("1 min 30 sec")) {
			return 90;
		} else if (time.equals("2 min")) {
			return 120;
		} else if (time.equals("2 min 30 sec")) {
			return 150;
		} else {
			return 180;
		}
	}

	@Override
	public void load() {
		if (scenario != null) {
			initSpinner();
		} else {
			scenario = new Scenario();
			scenario.setTime(30);
		}
	}

	@Override
	public void save() {
		CurrentLevel.getCurrentLevel().level.setScenario(scenario);
	}

	/**
	 * Window for the deletion.
	 */
	private void popupListEnemy(final int time) {

		LayoutInflater inflater = LayoutInflater.from(this);
		View dialogView = inflater.inflate(R.layout.builder_form_scenario_list,
				null);
		ListView listView = (ListView) dialogView
				.findViewById(R.id.builder_form_scenario_listview);
		/**
		 * Show the dialogue box
		 */
		AlertDialog.Builder ad = new AlertDialog.Builder(this);

		ad.setTitle(R.string.select_an_enemy);
		ad.setView(dialogView);

		final Dialog d = ad.show();
		
		ArrayScenarioAdapter adapter = new ArrayScenarioAdapter(this,
				CurrentLevel.getCurrentLevel().level.getEnemy());
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				(Toast.makeText(getApplicationContext(), ((Enemy) arg0
						.getAdapter().getItem(arg2)).getName(),
						Toast.LENGTH_SHORT)).show();
				scenario.add(time, ((Enemy) arg0.getAdapter().getItem(arg2)));

				d.dismiss();
				updateScrollview(scenario.getTime());

			}
		});

	}

	/**
	 * Window for the deletion.
	 */
	private void popup_delete(final TimeEnemy timeE) {

		LayoutInflater inflater = LayoutInflater.from(this);
		View dialogView = inflater.inflate(R.layout.popup_delete, null);

		/**
		 * Show the dialogue box
		 */
		AlertDialog.Builder ad = new AlertDialog.Builder(this);
		ad.setTitle(R.string.popup_delete_enemy);
		ad.setIcon(android.R.drawable.ic_dialog_info);
		ad.setView(dialogView);

		ad.setNegativeButton(R.string.popup_delete_cancel,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						return;
					}

				});

		ad.setPositiveButton(R.string.popup_delete_ok,
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int whichButton) {
						scenario.getPlay().remove(timeE);
						updateScrollview(scenario.getTime());
					}

				});

		ad.show();
	}

}
