package fr.esipe.android.escapeir.builder.form.activity;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import fr.esipe.android.escapeir.R;
import fr.esipe.android.escapeir.api.Map;
import fr.esipe.android.escapeir.builder.DataInterface;
import fr.esipe.android.escapeir.builder.form.adapter.ArrayMapsAdapter;


/**
 * Class MapsFormActivity Represent Map Form to fill out
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 */
public class MapsFormActivity extends Activity implements DataInterface,
		OnClickListener, OnItemClickListener {


	/**
	 * Add an enemy (button)
	 */
	private ImageButton addEnemyButton;

	/**
	 * Maps list view.
	 */
	private ListView listView;

	/**
	 * Enemies list empty view.
	 */
	private TextView emptyMapList;

	private ArrayMapsAdapter adapter;

	private ArrayList<Map> demoListMaps;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.builder_form_maps);

		addEnemyButton = (ImageButton) findViewById(R.id.builder_form_maps_add_button);

		demoListMaps = new ArrayList<Map>();
		Map map1 = new Map("earth_mini", R.drawable.earth_mini);
		Map map2 = new Map("moon_mini", R.drawable.moon_mini);
		Map map3 = new Map("jupiter_mini", R.drawable.jupiter_mini);
		demoListMaps.add(map1);
		demoListMaps.add(map2);
		demoListMaps.add(map3);

		listView = (ListView) findViewById(R.id.builder_form_maps_list);

		
		emptyMapList = (TextView) findViewById(R.id.builder_form_maps_list_empty);
		listView.setEmptyView(emptyMapList);

		adapter = new ArrayMapsAdapter(this, demoListMaps);
		listView.setAdapter(adapter);
		addEnemyButton.setOnClickListener(this);
		listView.setOnItemClickListener(this);
		load();

	}

	@Override
	protected void onPause() {
		super.onPause();
		CurrentLevel.getCurrentLevel().level.setMaps(demoListMaps);
	}

	@Override
	protected void onResume() {
		super.onResume();
		CurrentLevel.getCurrentLevel().level.setMaps(demoListMaps);
		listView.invalidateViews();

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.builder_form_maps_add_button:
			// Go to the detailed enemy form.
			startActivity(new Intent(this, MapsGridFormActivity.class));
			break;
		}

	}

	@Override
	public void load() {
		CurrentLevel.getCurrentLevel().level.setMaps(demoListMaps);
	}

	@Override
	public void save() {
		CurrentLevel.getCurrentLevel().level.setMaps(demoListMaps);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {

		Bundle oEnemy = new Bundle();
		oEnemy.putInt("mapID", position);
		final Intent intent = new Intent(MapsFormActivity.this,
				MapsGridFormActivity.class);
		intent.putExtras(oEnemy);
		startActivity(intent);

	}

}
