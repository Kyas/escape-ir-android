package fr.esipe.android.escapeir.builder.form.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.widget.GridView;
import fr.esipe.android.escapeir.R;
import fr.esipe.android.escapeir.api.Map;
import fr.esipe.android.escapeir.builder.form.adapter.ArrayMapsImportAdapter;

/**
 * 
 * @author J. Lor C.Perillous
 *
 */

public class MapsGridFormActivity extends Activity {

	/**
	 * Maps grid view.
	 */
	private GridView gridView;

	/**
	 * Adapter
	 */
	private ArrayMapsImportAdapter adapter;

	private List<Map> demoListMapsImport;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.builder_form_maps_import);
		demoListMapsImport = new ArrayList<Map>();
		Map map1 = new Map("earth_mini", R.drawable.earth_mini);
		Map map2 = new Map("moon_mini", R.drawable.moon_mini);
		Map map3 = new Map("jupiter_mini", R.drawable.jupiter_mini);
		demoListMapsImport.add(map1);
		demoListMapsImport.add(map2);
		demoListMapsImport.add(map3);
		gridView = (GridView) findViewById(R.id.builder_form_map_import_gridview);
		adapter = new ArrayMapsImportAdapter(this, demoListMapsImport);
		gridView.setAdapter(adapter);
	}

}
