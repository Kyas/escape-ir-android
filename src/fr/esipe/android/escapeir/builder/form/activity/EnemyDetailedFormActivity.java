package fr.esipe.android.escapeir.builder.form.activity;

import java.util.ArrayList;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;
import fr.esipe.android.escapeir.R;
import fr.esipe.android.escapeir.api.Enemy;
import fr.esipe.android.escapeir.api.Level;
import fr.esipe.android.escapeir.api.Scenario;
import fr.esipe.android.escapeir.api.Scenario.TimeEnemy;
import fr.esipe.android.escapeir.api.StartPositionEnum;
import fr.esipe.android.escapeir.builder.DataInterface;
import fr.esipe.android.escapeir.builder.form.dialog.DrawDialog;


/**
 * Class EnnemyDetailedeFormActivity Represent Enemy Form to fill out in order
 * to add one enemy
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 */
public class EnemyDetailedFormActivity extends Activity implements
DataInterface, OnClickListener {

	private Enemy oldEnemy;
	private Enemy newEnemy;

	private ImageView saveButton;
	private EditText editName;
	private ImageView enemySimple;
	private ImageView enemyMedium;
	private ImageView enemyHard;
	private EditText editLife;
	private EditText editShootDuration;
	private ImageView missile;
	private ImageView fireball;
	private ImageView shiboleet;
	private ImageView laser;
	private Spinner startPositionSpinner;
	private StartPositionEnum startPosition;
	private CheckBox verticalCheckbox;
	private Button movement;

	/**
	 * Ships
	 */
	private boolean isEnemySimpleClicked;
	private boolean isEnemyMediumClicked;
	private boolean isEnemyHardClicked;

	/**
	 * Weapons
	 */
	private boolean isMissileClicked;
	private boolean isFireballClicked;
	private boolean isShiboleetClicked;
	private boolean isLaserClicked;

	/**
	 * Item for enemy
	 */
	private String name;
	private int path;
	private int nbLife;
	private int nbDuration;
	private boolean[] weapons;
	private int nbWeapons = 0;
	private boolean verticalScrolling;

	/**
	 * Level
	 */
	private Level level = CurrentLevel.getCurrentLevel().level;
	private ArrayList<Point> listPoint;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.builder_form_enemy);

		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		Bundle oBundle = this.getIntent().getExtras();
		int enemyId = oBundle.getInt("enemyID");

		if (enemyId > -1) {
			// If we modify a new enemy.
			oldEnemy = level.getEnemy().get(enemyId);
		} else {
			// If we add a new enemy.
			oldEnemy = null;
		}
		saveButton = (ImageView) findViewById(R.id.heading_form_enemy_save);
		editName = (EditText) findViewById(R.id.builder_form_enemy_name_edit);
		enemySimple = (ImageView) findViewById(R.id.builder_form_enemy_ship1);
		enemyMedium = (ImageView) findViewById(R.id.builder_form_enemy_ship2);
		enemyHard = (ImageView) findViewById(R.id.builder_form_enemy_ship3);
		editLife = (EditText) findViewById(R.id.builder_form_enemy_life_edit);

		editShootDuration = (EditText) findViewById(R.id.builder_form_enemy_shoot_duration_edit);
		missile = (ImageView) findViewById(R.id.builder_form_enemy_missile);
		fireball = (ImageView) findViewById(R.id.builder_form_enemy_fireball);
		shiboleet = (ImageView) findViewById(R.id.builder_form_enemy_shiboleet);
		laser = (ImageView) findViewById(R.id.builder_form_enemy_laser);

		verticalCheckbox = (CheckBox) findViewById(R.id.builder_form_enemy_vertical_checkbox);
		movement = (Button) findViewById(R.id.builder_form_enemy_movement);

		startPositionSpinner = (Spinner) findViewById(R.id.builder_form_enemy_select_position_item);
		startPositionSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {

				startPosition.setValue(arg0.getAdapter().getItem(arg2));
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}
		});

		saveButton.setOnClickListener(this);
		enemySimple.setOnClickListener(this);
		enemyMedium.setOnClickListener(this);
		enemyHard.setOnClickListener(this);
		editName.setOnClickListener(this);
		editLife.setOnClickListener(this);
		editShootDuration.setOnClickListener(this);
		missile.setOnClickListener(this);
		fireball.setOnClickListener(this);
		shiboleet.setOnClickListener(this);
		laser.setOnClickListener(this);
		verticalCheckbox.setOnClickListener(this);
		movement.setOnClickListener(this);

		isEnemyHardClicked = false;
		isEnemyMediumClicked = false;
		isEnemySimpleClicked = false;

		isMissileClicked = false;
		isFireballClicked = false;
		isShiboleetClicked = false;
		isLaserClicked = false;

		if (oldEnemy != null) {
			// Modification of an enemy.
			load();
		} else {
			// Add an enemy.
			addEnemy();
		}
		if (verticalScrolling)
			verticalCheckbox.setChecked(true);
		initSpinner();

	}

	public void addEnemy() {
		name = "";
		path = 0;
		nbLife = 1;
		nbDuration = 10;
		weapons = new boolean[4];
		for (int i = 0; i < 3; i++) {
			weapons[i] = false;
		}
		// TODO A faire l'utilisation du start position.
		startPosition = StartPositionEnum.TOPLEFT;
		verticalScrolling = false;
	}

	@Override
	protected void onResume() {
		super.onResume();
		enemySimple.invalidate();
		enemyMedium.invalidate();
		enemyHard.invalidate();
	}

	public void initSpinner() {
		if(startPosition.getValue().equals("top - left"))
		{
			startPositionSpinner.setSelection(0);
		}
		else if(startPosition.getValue().equals("top - center"))
		{
			startPositionSpinner.setSelection(1);
		}
		else if(startPosition.getValue().equals("top - right"))
		{
			startPositionSpinner.setSelection(2);
		}
		else if(startPosition.getValue().equals("center - left"))
		{
			startPositionSpinner.setSelection(3);			
		}
		else if(startPosition.getValue().equals("center"))
		{
			startPositionSpinner.setSelection(4);
			
		}
		else if(startPosition.getValue().equals("center - right"))
		{
			startPositionSpinner.setSelection(5);			
		}
		else if(startPosition.getValue().equals("bottom - left"))
		{
			startPositionSpinner.setSelection(6);			
		}
		else if(startPosition.getValue().equals("bottom - center"))
		{
			startPositionSpinner.setSelection(7);
		}
		else
		{
			startPositionSpinner.setSelection(8);
		}
		
	}


	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.heading_form_enemy_save:
			save();
			break;

		case R.id.builder_form_enemy_ship1:
			if (!isEnemySimpleClicked) {
				enemySimple.setImageResource(R.drawable.enemy_simple_clicked);
				path = R.drawable.enemy_simple;
				isEnemySimpleClicked = true;
			}
			enemyMedium.setImageResource(R.drawable.enemy_medium);
			isEnemyMediumClicked = false;
			enemyHard.setImageResource(R.drawable.enemy_hard);
			isEnemyHardClicked = false;
			break;

		case R.id.builder_form_enemy_ship2:
			if (!isEnemyMediumClicked) {
				enemyMedium.setImageResource(R.drawable.enemy_medium_clicked);
				path = R.drawable.enemy_medium;
				isEnemyMediumClicked = true;
			}
			enemySimple.setImageResource(R.drawable.enemy_simple);
			isEnemySimpleClicked = false;
			enemyHard.setImageResource(R.drawable.enemy_hard);
			isEnemyHardClicked = false;
			break;

		case R.id.builder_form_enemy_ship3:
			if (!isEnemyHardClicked) {
				enemyHard.setImageResource(R.drawable.enemy_hard_clicked);
				path = R.drawable.enemy_hard;
				isEnemyHardClicked = true;
			}
			enemySimple.setImageResource(R.drawable.enemy_simple);
			isEnemySimpleClicked = false;
			enemyMedium.setImageResource(R.drawable.enemy_medium);
			isEnemyMediumClicked = false;
			break;

		case R.id.builder_form_enemy_missile:
			if (!isMissileClicked) {
				missile.setImageResource(R.drawable.icon_missile_clicked);
				nbWeapons++;
				isMissileClicked = true;
			} else {
				missile.setImageResource(R.drawable.icon_missile);
				nbWeapons--;
				isMissileClicked = false;
			}
			break;

		case R.id.builder_form_enemy_fireball:
			if (!isFireballClicked) {
				fireball.setImageResource(R.drawable.icon_fireball_clicked);
				nbWeapons++;
				isFireballClicked = true;
			} else {
				fireball.setImageResource(R.drawable.icon_fireball);
				nbWeapons--;
				isFireballClicked = false;
			}
			break;

		case R.id.builder_form_enemy_shiboleet:
			if (!isShiboleetClicked) {
				shiboleet.setImageResource(R.drawable.icon_shiboleet_clicked);
				nbWeapons++;
				isShiboleetClicked = true;
			} else {
				shiboleet.setImageResource(R.drawable.icon_shiboleet);
				nbWeapons--;
				isShiboleetClicked = false;
			}
			break;

		case R.id.builder_form_enemy_laser:
			if (!isLaserClicked) {
				nbWeapons++;
				laser.setImageResource(R.drawable.icon_laser_clicked);
				isLaserClicked = true;
			} else {
				laser.setImageResource(R.drawable.icon_laser);
				nbWeapons--;
				isLaserClicked = false;
			}
			break;

		case R.id.builder_form_enemy_vertical_checkbox:
			Toast.makeText(this, "Checkbox", Toast.LENGTH_SHORT).show();
			break;

		case R.id.builder_form_enemy_movement:
			listPoint=DrawDialog.popup_draw(this, getApplicationContext());
			Toast.makeText(this, "Movement add", Toast.LENGTH_SHORT).show();
			break;

		}

	}

	@Override
	public void load() {
		name = oldEnemy.getName();
		editName.setText(name);

		path = oldEnemy.getPath();
		
		if(path== R.drawable.enemy_simple){
			isEnemySimpleClicked = true;
			enemySimple.setImageResource(R.drawable.enemy_simple_clicked);
		}
		else if(path== R.drawable.enemy_medium){
			isEnemyMediumClicked = true;
			enemyMedium.setImageResource(R.drawable.enemy_medium_clicked);
		}
		else{
			isEnemyHardClicked = true;
			enemyHard.setImageResource(R.drawable.enemy_hard_clicked);
		}

		nbLife = oldEnemy.getLife();
		editLife.setText(String.valueOf(nbLife));
		nbDuration = oldEnemy.getShootTime();
		editShootDuration.setText(String.valueOf(nbDuration));
		weapons = oldEnemy.getWeapons();

		if (weapons[0] == true) {
			isMissileClicked = true;
			missile.setImageResource(R.drawable.icon_missile_clicked);
			nbWeapons++;
		}
		if (weapons[1] == true) {
			isFireballClicked = true;
			missile.setImageResource(R.drawable.icon_fireball_clicked);
			nbWeapons++;
		}
		if (weapons[2] == true) {
			isShiboleetClicked = true;
			shiboleet.setImageResource(R.drawable.icon_shiboleet_clicked);
			nbWeapons++;
		}
		if (weapons[3] == true) {
			isLaserClicked = true;
			laser.setImageResource(R.drawable.icon_laser_clicked);
			nbWeapons++;
		}

		startPosition = oldEnemy.getStartPosition();
		listPoint=oldEnemy.getListPoint();
		verticalScrolling = oldEnemy.isVerticalScrolling();

		onResume();
	}

	@Override
	public void save() {
		if (checkSave()) {
			if(listPoint==null || listPoint.size()==0){
				listPoint=new ArrayList<Point>();
				listPoint.add(new Point(0,0));
			}
			
			int id=CurrentLevel.generateID();
			newEnemy = new Enemy(name,id, path, nbLife, nbDuration, weapons, startPosition, verticalScrolling);
			
			newEnemy.setListPoint(listPoint);
			if(oldEnemy!=null){
				int oldID=oldEnemy.getId();
				Scenario scenario= CurrentLevel.getCurrentLevel().level.getScenario();
				if(scenario!=null){
					ArrayList<TimeEnemy> list=scenario.getPlay();
					for(TimeEnemy te:list){
						if(te.getEnemy().getId()==oldID){
							te.setEnemy(newEnemy);
						}
					}
				}				
			}
			
			
			
			CurrentLevel.getCurrentLevel().level.getEnemy().remove(oldEnemy);
			CurrentLevel.getCurrentLevel().level.getEnemy().add(0, newEnemy);
			startActivity(new Intent(this, EnemyFormActivity.class)
			.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK));
		}
	}

	/**
	 * Check if the user has done correctly the form.
	 * 
	 * @return <code>true</code> if all is filled, </code>false</code>
	 *         otherwise.
	 */
	public boolean checkSave() {
		try{
			name = editName.getText().toString();
			nbLife = (int) Integer.parseInt(editLife.getText().toString());
			nbDuration = (int) Integer.parseInt(editShootDuration.getText()
					.toString());
			verticalScrolling=verticalCheckbox.isChecked();
			if (name != null && !("").equals(name) && path > 0 && nbLife >= 1 && nbDuration > 0 && nbWeapons > 0) {
				if (isMissileClicked) {
					weapons[0] = true;
				}
				if (isFireballClicked) {
					weapons[1] = true;
				}
				if (isShiboleetClicked) {
					weapons[2] = true;
				}
				if (isLaserClicked) {
					weapons[3] = true;
				}
				return true;
			}
			Toast.makeText(this, "Your form is invalid !", Toast.LENGTH_SHORT)
			.show();
			return false;
		}
		catch(Exception e){
			Toast.makeText(this, "Your form is invalid !", Toast.LENGTH_SHORT)
			.show();
			return false;
		}
		

	}


	
}
