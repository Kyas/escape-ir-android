package fr.esipe.android.escapeir.builder.form.activity;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import fr.esipe.android.escapeir.R;
import fr.esipe.android.escapeir.api.Enemy;
import fr.esipe.android.escapeir.api.Level;
import fr.esipe.android.escapeir.builder.DataInterface;
import fr.esipe.android.escapeir.builder.form.adapter.ArrayEnemiesAdapter;


/**
 * Class EnemyFormActivity Represent all Enemies added in to the game
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 */
public class EnemyFormActivity extends Activity implements DataInterface,
		OnClickListener, OnItemClickListener {


	/**
	 * Add an enemy (button)
	 */
	private ImageButton addEnemyButton;

	/**
	 * Enemies list view.
	 */
	private ListView listView;

	/**
	 * Enemies list empty view.
	 */
	private TextView emptyEnemyList;

	private ArrayEnemiesAdapter adapter;

	private List<Enemy> listEnemies;

	private Level level = CurrentLevel.getCurrentLevel().level;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.builder_form_enemies);

		addEnemyButton = (ImageButton) findViewById(R.id.builder_form_enemies_add_button);
		listEnemies = level.getEnemy();
		listView = (ListView) findViewById(R.id.builder_form_enemies_list);

		emptyEnemyList = (TextView) findViewById(R.id.builder_form_enemies_list_empty);
		listView.setEmptyView(emptyEnemyList);
		adapter = new ArrayEnemiesAdapter(this, listEnemies);
		listView.setAdapter(adapter);

		addEnemyButton.setOnClickListener(this);

		listView.setOnItemClickListener(this);

	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		listView.invalidateViews();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.builder_form_enemies_add_button:
			Bundle oEnemy = new Bundle();
			// Put a negative number to set up a new Enemy
			oEnemy.putInt("enemyID", -1);

			final Intent intent = new Intent(this,
					EnemyDetailedFormActivity.class);
			intent.putExtras(oEnemy);
			startActivity(intent);
			break;
		}

	}

	@Override
	public void load() {
		// TODO Auto-generated method stub

	}

	@Override
	public void save() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {

		if (view.getId() == R.id.builder_form_enemies_add_button) {

		} else {

			// Launch of EnemyDetailedFormActivity
			Bundle oEnemy = new Bundle();
			oEnemy.putInt("enemyID", position);
			final Intent intent = new Intent(EnemyFormActivity.this,
					EnemyDetailedFormActivity.class)
					.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.putExtras(oEnemy);
			startActivity(intent);
		}

	}

}
