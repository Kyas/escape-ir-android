package fr.esipe.android.escapeir.builder.form.activity;

import fr.esipe.android.escapeir.api.Level;

/**
 * Current Level for Builder of for Game
 * @author J. Lor C. Perillous
 *
 */
public class CurrentLevel{
	
	protected Level level;
	private int nbEnnemi=0;
	private static CurrentLevel singleton;
	
	private CurrentLevel(){
		level=new Level();
	}
	
	/**
	 * create singleton
	 */
	public static void createLevel(){
		if(singleton==null){
			singleton=new CurrentLevel();
		}
	}
	
	public static CurrentLevel getCurrentLevel(){
		return singleton;
	}
	
	/**
	 * change current level
	 * @param l
	 */
	public static void setCurrentLevel(Level l){
		createLevel();
		singleton.level=l;
		if(l.getEnemy().size()>1){
		singleton.nbEnnemi=l.getEnemy().get(l.getEnemy().size()-1).getId()+1;
		}
		else{
			singleton.nbEnnemi=0;
		}
	}
	
	/**
	 * remove Level
	 */
	public static void removeCurrentLevel(){
		singleton=null;
	}
	
	/**
	 * generate id Enemy
	 * @return
	 */
	public static int generateID(){
		singleton.nbEnnemi++;
		return singleton.nbEnnemi;
	}
	
	/**
	 * get Level
	 * @return
	 */
	public Level getLevel(){
		return singleton.level;
	}
}