package fr.esipe.android.escapeir.builder.form.activity;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import fr.esipe.android.escapeir.EscapeIRApp;
import fr.esipe.android.escapeir.R;
import fr.esipe.android.escapeir.api.Level;
import fr.esipe.android.escapeir.builder.DataInterface;
import fr.esipe.android.escapeir.builder.XMLTools;

/**
 * Class LevelFormActivity Represent Level Form to fill out
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 */

public class LevelFormActivity extends Activity implements DataInterface,
		OnClickListener {

	Level level;
	private EditText editName;
	private Button buttonMap;
	private Button buttonPlayer;
	private Button buttonEnemy;
	private Button buttonBoss;
	private Button buttonScenario;
	private Button buttonSaveScenario;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.builder_form_activity);

		Bundle oBundle = this.getIntent().getExtras();
		int levelType = oBundle.getInt("levelType");

		// Hide the virtual keyboard.
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		editName = (EditText) findViewById(R.id.builder_form_name_edit);
		buttonMap = (Button) findViewById(R.id.builder_form_map_button);
		buttonPlayer = (Button) findViewById(R.id.builder_form_player_button);
		buttonEnemy = (Button) findViewById(R.id.builder_form_enemy_button);
		buttonBoss = (Button) findViewById(R.id.builder_form_boss_button);
		buttonScenario = (Button) findViewById(R.id.builder_form_scenario_button);
		buttonSaveScenario = (Button) findViewById(R.id.builder_form_save_scenario_button);

		editName.setOnClickListener(this);
		buttonMap.setOnClickListener(this);
		buttonPlayer.setOnClickListener(this);
		buttonEnemy.setOnClickListener(this);
		buttonBoss.setOnClickListener(this);
		buttonScenario.setOnClickListener(this);
		buttonSaveScenario.setOnClickListener(this);

		if (levelType == 0) {
			// Creation nouveau Level
			CurrentLevel.createLevel();
			level = CurrentLevel.getCurrentLevel().level;
		} else {
			level = CurrentLevel.getCurrentLevel().level;
			load();
		}

	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.builder_form_name_edit:
			break;
		case R.id.builder_form_map_button:
			startActivity(new Intent(this, MapsFormActivity.class));
			break;
		case R.id.builder_form_player_button:
			startActivity(new Intent(this, PlayerFormActivity.class));
			break;
		case R.id.builder_form_enemy_button:
			startActivity(new Intent(this, EnemyFormActivity.class));
			break;
		case R.id.builder_form_boss_button:
			startActivity(new Intent(this, BossFormActivity.class));
			break;
		case R.id.builder_form_scenario_button:
			startActivity(new Intent(this, ScenarioFormActivity.class));
			break;
		case R.id.builder_form_save_scenario_button:
			if (checkSave()) {
				save();
				startActivity(new Intent(this, EscapeIRApp.class)
						.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
								| Intent.FLAG_ACTIVITY_NEW_TASK));
			}
			break;
		}
	}

	@Override
	public void load() {
		editName.setText(level.getName());
	}

	public boolean checkSave() {
		Level level = CurrentLevel.getCurrentLevel().level;
		if (level.getName().equals(""))
			return false;
		if (level.getPlayer() == null)
			return false;
		if (level.getBoss() == null)
			return false;
		if (level.getEnemy() == null)
			return false;
		if (level.getMaps() == null)
			return false;
		if (level.getScenario() == null)
			return false;
		return true;
	}

	@Override
	public void save() {
		level.setName(editName.getText().toString());
		try {
			XMLTools.leveltoFile(getApplicationContext(), level);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}

}
