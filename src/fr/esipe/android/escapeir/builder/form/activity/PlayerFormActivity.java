package fr.esipe.android.escapeir.builder.form.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import fr.esipe.android.escapeir.R;
import fr.esipe.android.escapeir.api.Player;
import fr.esipe.android.escapeir.builder.DataInterface;

/**
 * Class PlayerFormActivity Represent Player Form to fill out
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 */

public class PlayerFormActivity extends Activity implements DataInterface {

	protected Player player;
	private ImageView imageButtonShip1;
	private ImageView imageButtonShip2;
	private ImageView imageButtonShip3;
	private EditText editTextMunition1;
	private EditText editTextMunition2;
	private EditText editTextMunition3;
	private EditText editTextMunition4;
	private EditText editTextLife;
	private ImageView buttonSave;

	private int life;
	private int munition1;
	private int munition2;
	private int munition3;
	private int munition4;
	private int path;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.builder_form_player);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		imageButtonShip1 = (ImageView) findViewById(R.id.imageView2);
		imageButtonShip2 = (ImageView) findViewById(R.id.imageView3);
		imageButtonShip3 = (ImageView) findViewById(R.id.imageView6);
		editTextMunition1 = (EditText) findViewById(R.id.editText1);
		editTextMunition2 = (EditText) findViewById(R.id.editText2);
		editTextMunition3 = (EditText) findViewById(R.id.editText3);
		editTextMunition4 = (EditText) findViewById(R.id.editText4);
		editTextLife = (EditText) findViewById(R.id.editText5);
		buttonSave = (ImageView) findViewById(R.id.heading_form_player_save);
		player = CurrentLevel.getCurrentLevel().level.getPlayer();
		load();

		imageButtonShip1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				imageButtonShip1
						.setImageResource(R.drawable.enemy_simple_clicked);
				imageButtonShip2.setImageResource(R.drawable.enemy_medium);
				imageButtonShip3.setImageResource(R.drawable.enemy_hard);
				imageButtonShip1.setSelected(true);
				imageButtonShip2.setSelected(false);
				imageButtonShip3.setSelected(false);
			}
		});

		imageButtonShip2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				imageButtonShip1.setImageResource(R.drawable.enemy_simple);
				imageButtonShip2
						.setImageResource(R.drawable.enemy_medium_clicked);
				imageButtonShip3.setImageResource(R.drawable.enemy_hard);
				imageButtonShip1.setSelected(false);
				imageButtonShip2.setSelected(true);
				imageButtonShip3.setSelected(false);
			}
		});

		imageButtonShip3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				imageButtonShip1.setImageResource(R.drawable.enemy_simple);
				imageButtonShip2.setImageResource(R.drawable.enemy_medium);
				imageButtonShip3
						.setImageResource(R.drawable.enemy_hard_clicked);
				imageButtonShip1.setSelected(false);
				imageButtonShip2.setSelected(false);
				imageButtonShip3.setSelected(true);
			}
		});

		buttonSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (checkResult()) {
					save();
					finish();
				}
			}
		});

		editTextLife.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				editTextLife.setText("");
			}
		});

	}

	/**
	 * check the form before saving
	 * @return
	 */
	public boolean checkResult() {
		try {
			life = Integer.parseInt(editTextLife.getText().toString());
			munition1 = Integer
					.parseInt(editTextMunition1.getText().toString());
			munition2 = Integer
					.parseInt(editTextMunition2.getText().toString());
			munition3 = Integer
					.parseInt(editTextMunition3.getText().toString());
			munition4 = Integer
					.parseInt(editTextMunition4.getText().toString());
			path = getPath();
			return true;

		} catch (Exception e) {
			Toast t = Toast.makeText(getApplicationContext(), "Form Invalid",
					Toast.LENGTH_SHORT);
			t.show();
			return false;
		}
	}

	
	@SuppressWarnings("static-access")
	@Override
	public void load() {
		if (player != null) {
			int resource = player.getPath();
			if (resource == R.drawable.enemy_simple) {
				imageButtonShip1
						.setImageResource(R.drawable.enemy_simple_clicked);
				imageButtonShip2.setImageResource(R.drawable.enemy_medium);
				imageButtonShip3.setImageResource(R.drawable.enemy_hard);
				imageButtonShip1.setSelected(true);
				imageButtonShip2.setSelected(false);
				imageButtonShip3.setSelected(false);
			} else if (resource == R.drawable.enemy_medium) {
				imageButtonShip1.setImageResource(R.drawable.enemy_simple);
				imageButtonShip2
						.setImageResource(R.drawable.enemy_medium_clicked);
				imageButtonShip3.setImageResource(R.drawable.enemy_hard);
				imageButtonShip1.setSelected(false);
				imageButtonShip2.setSelected(true);
				imageButtonShip3.setSelected(false);
			} else if (resource == R.drawable.enemy_hard) {
				imageButtonShip1.setImageResource(R.drawable.enemy_simple);
				imageButtonShip2.setImageResource(R.drawable.enemy_medium);
				imageButtonShip3
						.setImageResource(R.drawable.enemy_hard_clicked);
				imageButtonShip1.setSelected(false);
				imageButtonShip2.setSelected(false);
				imageButtonShip3.setSelected(true);
			}

			editTextLife.setText(player.getLife() + "");
			editTextMunition1.setText(player.getMissileCount() + "");
			editTextMunition2.setText(player.getFireballCount() + "");
			editTextMunition3.setText(player.getShiboleetCount() + "");
			editTextMunition4.setText(player.getLaserCount() + "");
		}
	}

	
	@Override
	public void save() {
		// TODO Auto-generated method stub
		player = new Player(path, life, munition1, munition2, munition3,
				munition4);
		CurrentLevel.getCurrentLevel().level.setPlayer(player);
	}

	/**
	 * 
	 * @return spaceship path
	 */
	private int getPath() {
		if (imageButtonShip1.isSelected()) {
			return R.drawable.player_simple;

		} else if (imageButtonShip2.isSelected()) {

			return R.drawable.player_medium;
		} else
			return R.drawable.player_hard;
	}
}
