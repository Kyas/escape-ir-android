package fr.esipe.android.escapeir.builder.form.dialog;

import java.util.ArrayList;

import fr.esipe.android.escapeir.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.SurfaceHolder.Callback;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;


/**
* Class DrawDialog
* Dialog box use for drawing movement of enemi
* @author J. LOR C. PERILLOUS
* @version 1.0.0
*/
public class DrawDialog {
	
	public static  ArrayList<Point> popup_draw(Activity activity,Context context){
		
		SurfaceView view;
		final ArrayList<Point> listPoint = new ArrayList<Point>();
		final Paint paint = new Paint();
		
		LayoutInflater inflater = LayoutInflater.from(context);
		View dialogView = inflater.inflate(R.layout.popup_draw, null);
		final AlertDialog.Builder ad = new AlertDialog.Builder(activity);

		paint.setColor(Color.CYAN);
		ad.setTitle(R.string.popup_draw_enemy);
		ad.setIcon(android.R.drawable.ic_dialog_info);
		ad.setView(dialogView);
		final Dialog d=ad.show();

		Button buttonDrop = (Button) dialogView.findViewById(R.id.button_drop);
		Button buttonSave = (Button) dialogView.findViewById(R.id.button_save);

		buttonDrop.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				synchronized (listPoint) {
					listPoint.clear();
				}
			}
		});

		buttonSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				ad.create().cancel();
				d.dismiss();
			}
		});

		view = (SurfaceView) dialogView.findViewById(R.id.surfaceView1);
		final SurfaceHolder holder = view.getHolder();
		view.getHolder().addCallback(new Callback() {

			@Override
			public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void surfaceCreated(final SurfaceHolder arg0) {
				// TODO Auto-generated method stub
				new Thread(new Runnable() {
					@Override
					public void run() {
						Canvas canvas = holder.lockCanvas(null);
						while (canvas!=null) {
							try {
								canvas.drawColor(Color.LTGRAY);	
								synchronized (listPoint) {
									for (Point p : listPoint) {
										canvas.drawCircle(p.x, p.y, 3, paint);
									}
								}
								holder.unlockCanvasAndPost(canvas);
								canvas = holder.lockCanvas(null);								
								Thread.sleep(50);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
					}
				}).start();
			}

			@Override
			public void surfaceDestroyed(SurfaceHolder arg0) {

			}

		});
		view.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View arg0, MotionEvent event) {
				// TODO Auto-generated method stub
				int action = event.getAction() & MotionEvent.ACTION_MASK;
				switch (action) {
				case MotionEvent.ACTION_DOWN: {
					synchronized (listPoint) {
						listPoint.add(new Point((int) event.getX(), (int) event
								.getY()));
					}
					break;
				}
				case MotionEvent.ACTION_MOVE: {
					synchronized (listPoint) {
						listPoint.add(new Point((int) event.getX(), (int) event
								.getY()));
					}
					break;
				}
				case MotionEvent.ACTION_UP: {

					break;
				}
				}
				return true;
			}

		});

		return listPoint;
	}

}
