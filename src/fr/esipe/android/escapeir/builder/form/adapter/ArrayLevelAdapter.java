package fr.esipe.android.escapeir.builder.form.adapter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import android.R.drawable;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import fr.esipe.android.escapeir.R;
import fr.esipe.android.escapeir.api.Map;
import fr.esipe.android.escapeir.builder.XMLTools;
import fr.esipe.android.escapeir.builder.form.activity.CurrentLevel;
import fr.esipe.android.escapeir.builder.form.activity.LevelFormActivity;

/**
 * Class ArrayLevelAdapter - Represent level from the list
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 */
public class ArrayLevelAdapter extends ArrayAdapter<String> {

	/**
	 * The Enemies List.
	 */
	private List<String> levels;

	/**
	 * The Inflater.
	 */
	private LayoutInflater inflater;

	/**
	 * The View of the Enemy.
	 */
	private View rowView;

	private String level;

	private Context context;

	/**
	 * Static class (doesn't depend on the instance of the current class). Used
	 * for store references.
	 */
	static class ViewHolder {
		public ImageView iconView;
		public TextView nameView;
		public ImageView deleteView;
	}

	public ArrayLevelAdapter(Context context, List<String> levels) {
		super(context, R.layout.builder_activity_raw, levels);
		this.levels = levels;
		this.context = context;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return levels.size();
	}

	@Override
	public String getItem(int position) {
		return levels.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public void remove(String level) {
		super.remove(level);
		levels.remove(level);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		rowView = convertView;
		if (rowView == null) {
			// Creation of the View object from the Layout ressource
			rowView = inflater.inflate(R.layout.builder_activity_raw, null);
			holder = new ViewHolder();
			// Display of the corresponding icon of the enemy
			holder.iconView = (ImageView) rowView
					.findViewById(R.id.builder_select_level_icon);
			// Display of the corresponding title of the enemy
			holder.nameView = (TextView) rowView
					.findViewById(R.id.builder_select_level_name);

			// Display of the corresponding icon of the deletion
			holder.deleteView = (ImageView) rowView
					.findViewById(R.id.builder_select_level_remove);

			rowView.setTag(holder);
		} else {
			holder = (ViewHolder) rowView.getTag();
		}

		level=(String)getItem(position);
		holder.iconView.setImageResource(R.drawable.icon_map_empty);
		// Get the identifying enemy by its position in the List
		
		try {
			ArrayList<Map> maps=XMLTools.fileToLevelLitleMap(context, (String)getItem(position));
			Random r=new Random();
			int a= Math.abs(r.nextInt()%maps.size());
			holder.iconView.setImageResource(maps.get(a).getPath());
			
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		

		holder.nameView.setText(level);


		holder.deleteView.setImageResource(drawable.ic_menu_delete);

		final int positionToRemove = position;

		holder.iconView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				try {

					CurrentLevel.setCurrentLevel(XMLTools.fileToLevel(context, (String)getItem(positionToRemove)));

					Intent intent = new Intent(context, LevelFormActivity.class)
							.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
									| Intent.FLAG_ACTIVITY_NEW_TASK);

					intent.putExtra("levelType", 1);
					context.startActivity(intent);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

		holder.nameView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				try {

					CurrentLevel.setCurrentLevel(XMLTools.fileToLevel(context, (String)getItem(positionToRemove)));
				
					Intent intent = new Intent(context, LevelFormActivity.class);
					intent.putExtra("levelType", 1).setFlags(
							Intent.FLAG_ACTIVITY_CLEAR_TOP
									| Intent.FLAG_ACTIVITY_NEW_TASK);
					;

					context.startActivity(intent);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

		holder.deleteView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				popupDelete(positionToRemove);
				notifyDataSetChanged();
			}
		});
		return rowView;
	}

	/**
	 * Window for the deletion.
	 */
	private void popupDelete(final int position) {

		LayoutInflater inflater = LayoutInflater.from(this.getContext());
		View dialogView = inflater.inflate(R.layout.popup_delete, null);

		/**
		 * Show the dialogue box
		 */
		AlertDialog.Builder ad = new AlertDialog.Builder(this.getContext());
		ad.setTitle(R.string.popup_delete_enemy);
		ad.setIcon(android.R.drawable.ic_dialog_info);
		ad.setView(dialogView);

		ad.setNegativeButton(R.string.popup_delete_cancel,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						return;
					}

				});

		ad.setPositiveButton(R.string.popup_delete_ok,
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int whichButton) {
						String level = levels.get(position);
						ArrayLevelAdapter.this.remove(level);
						File dir = context.getDir("levels",
								Context.MODE_PRIVATE);
						File xml = new File(dir.getAbsolutePath()
								+ File.separator + level);
						if (xml.exists()) {
							xml.delete();
						}
						ArrayLevelAdapter.this.notifyDataSetChanged();
						Toast.makeText(context, level + " has been deleted.",
								Toast.LENGTH_SHORT).show();
					}

				});

		ad.show();
	}

}
