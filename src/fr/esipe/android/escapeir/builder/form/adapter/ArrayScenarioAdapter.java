package fr.esipe.android.escapeir.builder.form.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import fr.esipe.android.escapeir.R;
import fr.esipe.android.escapeir.api.Enemy;

/**
 * Class ArrayEnemiesAdapter - Represent scenario from the list
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 */
public class ArrayScenarioAdapter extends ArrayAdapter<Enemy> {

	/**
	 * The Enemies List.
	 */
	private List<Enemy> enemies;
	/**
	 * The Inflater.
	 */
	private LayoutInflater inflater;

	/**
	 * The View of the Enemy.
	 */
	private View rowView;
	private Enemy enemy;

	/**
	 * Static class (doesn't depend on the instance of the current class). Used
	 * for store references.
	 */
	static class ViewHolder {
		public ImageView iconView;
		public TextView nameView;
	}

	public ArrayScenarioAdapter(Context context, List<Enemy> enemies) {
		super(context, R.layout.builder_form_scenario_list_row, enemies);
		this.enemies = enemies;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return enemies.size();
	}

	@Override
	public Enemy getItem(int position) {
		return enemies.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		rowView = convertView;
		if (rowView == null) {
			// Creation of the View object from the Layout ressource
			rowView = inflater.inflate(R.layout.builder_form_scenario_list_row, null);
			holder = new ViewHolder();
			// Display of the corresponding icon of the enemy
			holder.iconView = (ImageView) rowView
					.findViewById(R.id.builder_form_enemies_row_icon);
			// Display of the corresponding title of the enemy
			holder.nameView = (TextView) rowView
					.findViewById(R.id.builder_form_enemies_row_title);
			rowView.setTag(holder);
		} else {
			holder = (ViewHolder) rowView.getTag();
		}

		// Get the identifying enemy by its position in the List
		enemy = (Enemy) getItem(position);
		holder.iconView.setImageResource(R.drawable.icon_map_empty);
		if (enemy.getPath() > 0) {
			holder.iconView.setImageResource(enemy.getPath());
		}
		holder.nameView.setText(enemy.getName());
		return rowView;
	}

}