package fr.esipe.android.escapeir.builder.form.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import fr.esipe.android.escapeir.R;
import fr.esipe.android.escapeir.api.Map;
import fr.esipe.android.escapeir.builder.form.activity.MapsFormActivity;


/**
 * Class ArrayMapsImportAdapter - Represent maps from the list for the import
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 */
public class ArrayMapsImportAdapter extends ArrayAdapter<Map> {

	/**
	 * The Maps List.
	 */
	private List<Map> maps;

	/**
	 * The Inflater.
	 */
	private LayoutInflater inflater;

	/**
	 * The View of the Enemy.
	 */
	private View rowView;

	private Map map;

	/**
	 * Static class (doesn't depend on the instance of the current class). Used
	 * for store references.
	 */
	static class ViewHolder {
		public ImageView iconView;
		public TextView nameView;
	}

	public ArrayMapsImportAdapter(Context context, List<Map> maps) {
		super(context, R.layout.builder_form_maps_row_item, maps);
		this.maps = maps;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return maps.size();
	}

	@Override
	public Map getItem(int position) {
		return maps.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public void remove(Map map) {
		super.remove(map);

		maps.remove(map);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder;
		rowView = convertView;

		if (rowView == null) {

			// Creation of the View object from the Layout ressource
			rowView = inflater.inflate(R.layout.builder_form_maps_row_item,
					null);

			holder = new ViewHolder();

			// Display of the corresponding icon of the map
			holder.iconView = (ImageView) rowView
					.findViewById(R.id.builder_form_maps_row_item);

			// Display of the corresponding title of the map
			holder.nameView = (TextView) rowView
					.findViewById(R.id.builder_form_maps_row_item_text);

			rowView.setTag(holder);
		} else {
			holder = (ViewHolder) rowView.getTag();
		}

		// Get the identifying map by its position in the List
		map = (Map) getItem(position);

		holder.iconView.setImageResource(R.drawable.icon_map_empty);
		if (map.getPath() > 0) {
			holder.iconView.setImageResource(map.getPath());
		}

		holder.nameView.setText(map.getName());

		final int positionSelected = position;

		holder.iconView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				addMap(positionSelected);
				v.getContext().startActivity(
						new Intent(v.getContext(), MapsFormActivity.class)
								.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
										| Intent.FLAG_ACTIVITY_NEW_TASK));
				notifyDataSetChanged();
			}
		});

		return rowView;
	}

	public boolean addMap(int position) {
		Map mapImport = maps.get(position);

		List<Map> maps = new ArrayList<Map>();
		Map map1 = new Map("earth_mini", R.drawable.earth_mini);
		Map map2 = new Map("moon_mini", R.drawable.moon_mini);
		Map map3 = new Map("jupiter_mini", R.drawable.jupiter_mini);
		maps.add(map1);
		maps.add(map2);
		maps.add(map3);

		for (Map map : maps) {
			if (map.getName().equals(mapImport.getName())) {
				Toast.makeText(this.getContext(), "Map is already added !",
						Toast.LENGTH_SHORT).show();
				return false;
			}
		}

		maps.add(mapImport);
		Toast.makeText(this.getContext(),
				"Map " + mapImport.getName() + " added.", Toast.LENGTH_SHORT)
				.show();
		return true;
	}
}

