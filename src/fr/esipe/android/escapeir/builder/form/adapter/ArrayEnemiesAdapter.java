package fr.esipe.android.escapeir.builder.form.adapter;

import java.util.List;

import android.R.drawable;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import fr.esipe.android.escapeir.R;
import fr.esipe.android.escapeir.api.Enemy;

/**
 * Class ArrayEnemiesAdapter - Represent enemies from the list
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 */
public class ArrayEnemiesAdapter extends ArrayAdapter<Enemy> {

	/**
	 * The Enemies List.
	 */
	private List<Enemy> enemies;

	/**
	 * The Inflater.
	 */
	private LayoutInflater inflater;

	/**
	 * The View of the Enemy.
	 */
	private View rowView;

	private Enemy enemy;

	private Context context;

	/**
	 * Static class (doesn't depend on the instance of the current class). Used
	 * for store references.
	 */
	static class ViewHolder {
		public ImageView iconView;
		public TextView nameView;
		public ImageView deleteView;
	}

	public ArrayEnemiesAdapter(Context context, List<Enemy> enemies) {
		super(context, R.layout.builder_form_enemies_row, enemies);
		this.enemies = enemies;
		this.context = context;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return enemies.size();
	}

	@Override
	public Enemy getItem(int position) {
		return enemies.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public void remove(Enemy enemy) {
		super.remove(enemy);

		enemies.remove(enemy);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder;
		rowView = convertView;

		if (rowView == null) {

			// Creation of the View object from the Layout ressource
			rowView = inflater.inflate(R.layout.builder_form_enemies_row, null);

			holder = new ViewHolder();

			// Display of the corresponding icon of the enemy
			holder.iconView = (ImageView) rowView
					.findViewById(R.id.builder_form_enemies_row_icon);

			// Display of the corresponding title of the enemy
			holder.nameView = (TextView) rowView
					.findViewById(R.id.builder_form_enemies_row_title);

			// Display of the corresponding icon of the deletion
			holder.deleteView = (ImageView) rowView
					.findViewById(R.id.builder_form_enemies_row_delete);

			rowView.setTag(holder);
		} else {
			holder = (ViewHolder) rowView.getTag();
		}

		// Get the identifying enemy by its position in the List
		enemy = (Enemy) getItem(position);

		holder.iconView.setImageResource(R.drawable.icon_map_empty);
		if (enemy.getPath() > 0) {
			holder.iconView.setImageResource(enemy.getPath());
		}

		holder.nameView.setText(enemy.getName());

		holder.deleteView.setImageResource(drawable.ic_menu_delete);

		final int positionToRemove = position;

		holder.deleteView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				popupDelete(positionToRemove);
				notifyDataSetChanged();
			}
		});
		return rowView;
	}

	/**
	 * Window for the deletion.
	 */
	private void popupDelete(final int position) {

		LayoutInflater inflater = LayoutInflater.from(this.getContext());
		View dialogView = inflater.inflate(R.layout.popup_delete, null);

		/**
		 * Show the dialogue box
		 */
		AlertDialog.Builder ad = new AlertDialog.Builder(this.getContext());
		ad.setTitle(R.string.popup_delete_enemy);
		ad.setIcon(android.R.drawable.ic_dialog_info);
		ad.setView(dialogView);

		ad.setNegativeButton(R.string.popup_delete_cancel,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						return;
					}

				});

		ad.setPositiveButton(R.string.popup_delete_ok,
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int whichButton) {
						Enemy enemy = enemies.get(position);
						ArrayEnemiesAdapter.this.remove(enemy);

						ArrayEnemiesAdapter.this.notifyDataSetChanged();
						Toast.makeText(context, enemy.getName() + " has been deleted.",
								Toast.LENGTH_SHORT).show();
					}

				});

		ad.show();
	}

}