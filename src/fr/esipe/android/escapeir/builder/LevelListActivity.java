package fr.esipe.android.escapeir.builder;

import android.app.Activity;
import android.os.Bundle;

/**
 * Class BuilderActivity Main activity in the builder
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 */
public class LevelListActivity extends Activity {

	// TODO Cette activit� correspond au spec o� il y a les 2 boutons (ajouter
	// et modifier un niveau) + la liste des items "Terre" etc.

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// setContentView(R.layout.level_list_activity);
	}

}
