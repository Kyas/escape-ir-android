package fr.esipe.android.escapeir.builder;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import android.content.Context;
import android.graphics.Point;

import fr.esipe.android.escapeir.api.Boss;
import fr.esipe.android.escapeir.api.Enemy;
import fr.esipe.android.escapeir.api.Level;
import fr.esipe.android.escapeir.api.Map;
import fr.esipe.android.escapeir.api.Player;
import fr.esipe.android.escapeir.api.Scenario;
import fr.esipe.android.escapeir.api.StartPositionEnum;

/**
 * Class XMLTools This class allows to load xml File and create correponding
 * object, and translate Level objects in one xml File
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 */
public class XMLTools {

	/**
	 * File To Level
	 * 
	 * @param context
	 * @param path
	 * @return
	 * @throws Exception
	 */
	public static Level fileToLevel(Context context, String path)
			throws Exception {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		File dir = context.getDir("levels", Context.MODE_PRIVATE);
		File xml = new File(dir.getAbsolutePath() + File.separator + path);
		Document document = builder.parse(xml);

		Level level = new Level();
		if (!document.hasChildNodes()) {
			throw new Exception();
		} else {
			Element root = document.getDocumentElement();
			if (!root.hasChildNodes()) {
				throw new Exception();
			} else {
				if (root != null) {
					String nameLevel = ((Element) root).getAttribute("name");
					NodeList playerListe = root
							.getElementsByTagName("player");
					Player playerLevel = null;
					if (playerListe != null) {
						Node player = playerListe.item(0);
						NodeList playerNode = player.getChildNodes();

						int pathPlayer = Integer.parseInt(playerNode.item(0)
								.getTextContent());

						int lifePlayer = Integer.parseInt(playerNode.item(1)
								.getTextContent());

						int missilePlayer = Integer.parseInt(playerNode.item(2)
								.getTextContent());

						int fireballPlayer = Integer.parseInt(playerNode
								.item(3).getTextContent());

						int shiboleetPlayer = Integer.parseInt(playerNode.item(
								4).getTextContent());

						int lazerPlayer = Integer.parseInt(playerNode.item(5)
								.getTextContent());

						playerLevel = new Player(pathPlayer, lifePlayer,
								missilePlayer, fireballPlayer, shiboleetPlayer,
								lazerPlayer);

					}
					NodeList enemyNodeList = root
							.getElementsByTagName("enemy_list");
					Node enemyList = enemyNodeList.item(0);
					int numberEnemy = Integer.parseInt(((Element) enemyList)
							.getAttribute("number_enemy"));
					NodeList enemies = root.getElementsByTagName("enemy");
					ArrayList<Enemy> array = new ArrayList<Enemy>();
					for (int i = 0; i < numberEnemy; i++) {
						Node enemy = enemies.item(i);
						NodeList enemyNode = enemy.getChildNodes();
						String nameEnemi = ((Element) enemy)
								.getAttribute("name");
						int idEnemi = Integer.parseInt(((Element) enemy)
								.getAttribute("id"));
						int pathEnemy = Integer.parseInt(enemyNode.item(0)
								.getTextContent());
						int lifeEnemy = Integer.parseInt(enemyNode.item(1)
								.getTextContent());
						int shootTime = Integer.parseInt(enemyNode.item(2)
								.getTextContent());
						Node weapons = enemyNode.item(3);
						NodeList weaponsList = weapons.getChildNodes();
						boolean[] weap = new boolean[4];
						for (int j = 0; j < 4; j++) {
							weap[j] = Boolean.parseBoolean(weaponsList.item(j)
									.getTextContent());

						}
						String start_position = enemyNode.item(4)
								.getTextContent();
						StartPositionEnum speEnum = StartPositionEnum.TOPLEFT;
						speEnum.setValue(start_position);
						boolean scroll = Boolean.parseBoolean(enemyNode.item(5)
								.getTextContent());
						Node nodeTrajectoire = enemyNode.item(6);
						NodeList nodeListTrajectoire = nodeTrajectoire
								.getChildNodes();
						ArrayList<Point> listPoint = new ArrayList<Point>();
						for (int l = 0; l < nodeListTrajectoire.getLength(); l++) {
							String text = nodeListTrajectoire.item(l)
									.getTextContent();
							String[] pointStr = text.split("-");
							int a = Integer.parseInt(pointStr[0]);
							int b = Integer.parseInt(pointStr[1]);
							listPoint.add(new Point(a, b));
						}
						Enemy enemy2 = new Enemy(nameEnemi, idEnemi, pathEnemy,
								lifeEnemy, shootTime, weap, speEnum, scroll);
						enemy2.setListPoint(listPoint);
						array.add(enemy2);
					}

					NodeList bossList = root.getElementsByTagName("boss");
					Node boss = bossList.item(0);
					NodeList bossAtt = boss.getChildNodes();
					String nameBoss = ((Element) boss).getAttribute("name");
					int pathBoss = Integer.parseInt(bossAtt.item(0)
							.getTextContent());
					int lifeBoss = Integer.parseInt(bossAtt.item(1)
							.getTextContent());
					int shootTimeBoss = Integer.parseInt(bossAtt.item(2)
							.getTextContent());
					Node weaponsBoss = bossAtt.item(3);
					NodeList weaponsListBoss = weaponsBoss.getChildNodes();
					boolean[] weapBoss = new boolean[4];
					for (int j = 0; j < 4; j++) {
						weapBoss[j] = Boolean.parseBoolean(weaponsListBoss
								.item(j).getTextContent());

					}
					Node nodeTrajectoire = bossAtt.item(4);
					NodeList nodeListTrajectoire = nodeTrajectoire
							.getChildNodes();
					ArrayList<Point> listPoint = new ArrayList<Point>();
					for (int l = 0; l < nodeListTrajectoire.getLength(); l++) {
						String text = nodeListTrajectoire.item(l)
								.getTextContent();
						String[] pointStr = text.split("-");
						int a = Integer.parseInt(pointStr[0]);
						int b = Integer.parseInt(pointStr[1]);
						listPoint.add(new Point(a, b));
					}
					Boss bossLevel = new Boss(nameBoss, pathBoss, lifeBoss,
							shootTimeBoss, weapBoss);
					bossLevel.setMovement(listPoint);

					NodeList mapsList = root.getElementsByTagName("map");
					ArrayList<Map> arrayMap = new ArrayList<Map>();
					for (int i = 0; i < mapsList.getLength(); i++) {
						int pathMap = Integer.parseInt(mapsList.item(0)
								.getTextContent());
						String nameMap = mapsList.item(1).getTextContent();
						Map map = new Map(nameMap, pathMap);
						arrayMap.add(map);
					}

					NodeList scnenariosList = root
							.getElementsByTagName("scenario");
					Node scenarioList = scnenariosList.item(0);
					int time = Integer.parseInt(((Element) scenarioList)
							.getAttribute("time"));

					NodeList timeEnemyList = root
							.getElementsByTagName("time_enemy");
					Scenario scenario = new Scenario();
					scenario.setTime(time);
					for (int i = 0; i < timeEnemyList.getLength(); i++) {
						Node node = timeEnemyList.item(i);
						NodeList attributList = node.getChildNodes();
						int time_enemi = Integer.parseInt(attributList.item(0)
								.getTextContent());
						int id = Integer.parseInt(attributList.item(1)
								.getTextContent());
						for (int j = 0; j < array.size(); j++) {
							if (array.get(j).getId() == id) {
								scenario.add(time_enemi, array.get(j));
								break;
							}
						}

					}
					level.setName(nameLevel);
					level.setPlayer(playerLevel);
					level.setEnnemi(array);
					level.setBoss(bossLevel);
					level.setMaps(arrayMap);
					level.setScenario(scenario);

				}
			}
		}

		return level;
	}

	/**
	 * get Map from file
	 * 
	 * @param context
	 * @param path
	 * @return
	 * @throws Exception
	 */
	public static ArrayList<Map> fileToLevelLitleMap(Context context,
			String path) throws Exception {

		DocumentBuilderFactory fabrique = DocumentBuilderFactory.newInstance();
		DocumentBuilder constructeur = fabrique.newDocumentBuilder();
		File dir = context.getDir("levels", Context.MODE_PRIVATE);
		File xml = new File(dir.getAbsolutePath() + File.separator + path);
		Document document = constructeur.parse(xml);
		if (document.hasChildNodes()) {
			Element racine = document.getDocumentElement();
			if (!racine.hasChildNodes()) {
				throw new Exception();
			} else {
				if (racine != null) {

					NodeList mapsList = racine.getElementsByTagName("map");
					ArrayList<Map> arrayMap = new ArrayList<Map>();
					for (int i = 0; i < mapsList.getLength(); i++) {
						int pathMap = Integer.parseInt(mapsList.item(0)
								.getTextContent());
						String nameMap = mapsList.item(1).getTextContent();
						Map map = new Map(nameMap, pathMap);
						arrayMap.add(map);
					}

					return arrayMap;
				}
			}

		}
		return null;
	}

	/**
	 * Convert level in xml file
	 * 
	 * @param context
	 * @param level
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 * @throws TransformerException
	 */
	@SuppressWarnings("static-access")
	public static void leveltoFile(Context context, Level level)
			throws ParserConfigurationException, SAXException, IOException,
			TransformerException {
		DocumentBuilderFactory fabrique = DocumentBuilderFactory.newInstance();
		DocumentBuilder constructeur = fabrique.newDocumentBuilder();
		Document document = constructeur.newDocument();
		Element levelElement = document.createElement("level");
		levelElement.setAttribute("name", level.getName());

		Player player = level.getPlayer();

		if (player != null) {
			Element playerElement = document.createElement("player");
			Element player_resource = document.createElement("resource");
			player_resource.setTextContent(player.getPath() + "");
			Element player_life = document.createElement("life");
			player_life.setTextContent(player.getLife() + "");
			Element player_missile = document.createElement("missile_munition");
			player_missile.setTextContent(player.getMissileCount() + "");
			Element fireballMunition = document
					.createElement("fireball_munition");
			fireballMunition.setTextContent(player.getFireballCount() + "");
			Element shiboleetMunition = document
					.createElement("shiboleet_munition");
			shiboleetMunition.setTextContent(player.getShiboleetCount() + "");
			Element laserMunition = document.createElement("lazer_munition");
			laserMunition.setTextContent(player.getLaserCount() + "");

			playerElement.appendChild(player_resource);
			playerElement.appendChild(player_life);
			playerElement.appendChild(player_missile);
			playerElement.appendChild(fireballMunition);
			playerElement.appendChild(shiboleetMunition);
			playerElement.appendChild(laserMunition);

			levelElement.appendChild(playerElement);
		}

		ArrayList<Enemy> enemyList = level.getEnemy();
		Element enemyListElement = document.createElement("enemy_list");
		if (enemyList != null) {
			enemyListElement
					.setAttribute("number_enemy", enemyList.size() + "");
			for (int i = 0; i < enemyList.size(); i++) {
				Enemy enemy = enemyList.get(i);
				Element enemyElement = document.createElement("enemy");
				enemyElement.setAttribute("name", enemy.getName());
				enemyElement.setAttribute("id", enemy.getId() + "");
				Element enemy_resource = document.createElement("resource");
				enemy_resource.setTextContent(enemy.getPath() + "");
				Element enemy_life = document.createElement("life");
				enemy_life.setTextContent(enemy.getLife() + "");
				Element enemy_shoot = document.createElement("shoot_time");
				enemy_shoot.setTextContent(enemy.getShootTime() + "");

				Element weapons = document.createElement("weapons");
				for (int j = 0; j < 4; j++) {
					Element weapons_item = document
							.createElement("weapons" + j);
					weapons_item.setTextContent(enemy.getWeapons()[j] + "");
					weapons.appendChild(weapons_item);
				}

				Element startPosition = document
						.createElement("start_position");
				startPosition.setTextContent(enemy.getStartPosition() + "");
				Element scrollingVertical = document
						.createElement("scroll_position");
				scrollingVertical.setTextContent(enemy.isVerticalScrolling()
						+ "");

				Element listPoint = document.createElement("trajectoire");
				ArrayList<Point> list = enemy.getListPoint();
				for (int k = 0; k < list.size(); k++) {
					Element point_item = document.createElement("point");
					point_item.setTextContent(list.get(k).x + "-"
							+ list.get(k).y);
					listPoint.appendChild(point_item);
				}

				enemyElement.appendChild(enemy_resource);
				enemyElement.appendChild(enemy_life);
				enemyElement.appendChild(enemy_shoot);
				enemyElement.appendChild(weapons);
				enemyElement.appendChild(startPosition);
				enemyElement.appendChild(scrollingVertical);
				enemyElement.appendChild(listPoint);
				enemyListElement.appendChild(enemyElement);
			}

		} else {
			enemyListElement.setAttribute("number_enemy", "0");
		}

		Boss boss = level.getBoss();

		if (boss != null) {
			Element bossElement = document.createElement("boss");
			bossElement.setAttribute("name", boss.getName());
			Element boss_resource = document.createElement("resource");
			boss_resource.setTextContent(boss.getPath() + "");
			Element boss_life = document.createElement("life");
			boss_life.setTextContent(boss.getLife() + "");
			Element boss_shoot = document.createElement("shoot_time");
			boss_shoot.setTextContent(boss.getShoot() + "");

			Element weapons = document.createElement("weapons");
			for (int j = 0; j < 4; j++) {
				Element weapons_item = document.createElement("weapons" + j);
				weapons_item.setTextContent(boss.getWeapons()[j] + "");
				weapons.appendChild(weapons_item);
			}

			Element listPoint = document.createElement("trajectoire");
			ArrayList<Point> list = boss.getMovement();
			for (int k = 0; k < list.size(); k++) {
				Element point_item = document.createElement("point");
				point_item.setTextContent(list.get(k).x + "-" + list.get(k).y);
				listPoint.appendChild(point_item);
			}

			bossElement.appendChild(boss_resource);
			bossElement.appendChild(boss_life);
			bossElement.appendChild(boss_shoot);
			bossElement.appendChild(weapons);
			bossElement.appendChild(listPoint);
			levelElement.appendChild(bossElement);

		}

		ArrayList<Map> mapsList = level.getMaps();
		Element mapListElement = document.createElement("map_list");
		mapListElement.setAttribute("number_map", mapsList.size() + "");
		for (int i = 0; i < mapsList.size(); i++) {
			Element mapElement = document.createElement("map");
			Element pathElement = document.createElement("path_map");
			pathElement.setTextContent(mapsList.get(i).getPath() + "");

			Element nameElement = document.createElement("name_map");
			nameElement.setTextContent(mapsList.get(i).getName() + "");

			mapElement.appendChild(pathElement);
			mapListElement.appendChild(mapElement);
		}

		levelElement.appendChild(enemyListElement);
		levelElement.appendChild(mapListElement);

		Scenario scenario = level.getScenario();
		if (scenario != null) {
			Element scenarioElem = document.createElement("scenario");
			scenarioElem.setAttribute("time", scenario.getTime() + "");
			for (int i = 0; i < scenario.getPlay().size(); i++) {
				Element timeElement = document.createElement("time_enemy");
				Element timeElement_time = document.createElement("time");
				timeElement_time.setTextContent(scenario.getPlay().get(i)
						.getTime()
						+ "");
				Element timeElement_name = document.createElement("id");
				timeElement_name.setTextContent(scenario.getPlay().get(i)
						.getEnemy().getId()
						+ "");
				timeElement.appendChild(timeElement_time);
				timeElement.appendChild(timeElement_name);
				scenarioElem.appendChild(timeElement);

			}

			levelElement.appendChild(scenarioElem);
		}
		document.appendChild(levelElement);

		File dir = context.getDir("levels", Context.MODE_PRIVATE);
		File xml = new File(dir.getAbsolutePath() + File.separator
				+ level.getName() + ".xml");
		if (!xml.exists()) {
			xml.createNewFile();
		} else {
			xml.delete();
			xml.createNewFile();
		}

		TransformerFactory transformerFactory = TransformerFactory
				.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(document);
		StreamResult result = new StreamResult(xml);
		transformer.transform(source, result);
	}

}
