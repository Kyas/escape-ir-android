package fr.esipe.android.escapeir.builder;

/**
 * Interface which propose two methods for loading object in form and modifying
 * object according to filled out form.
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 * 
 */
public interface DataInterface {
	
	/**
	 * Fill out the form accordint to object places in extra of the Activity
	 */
	public void load();
	
	/**
	 * Change the object in extra according to data in form
	 */
	public void save();

}
