package fr.esipe.android.escapeir.builder;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import fr.esipe.android.escapeir.R;
import fr.esipe.android.escapeir.builder.form.activity.CurrentLevel;
import fr.esipe.android.escapeir.builder.form.activity.LevelFormActivity;
import fr.esipe.android.escapeir.builder.form.adapter.ArrayLevelAdapter;

/**
 * Class BuilderActivity Main activity in the builder
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 */

public class BuilderActivity extends Activity  {


	private ImageView buttonAddLevel;
	private TextView textViewAddLevel;
	
	private ArrayLevelAdapter adapter;
	private List<String> listLevel;
	private ListView listView;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.builder_activity);

		buttonAddLevel = (ImageView) findViewById(R.id.imageView1);
		buttonAddLevel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				CurrentLevel.removeCurrentLevel();
				CurrentLevel.createLevel();
				launchAddLevel();
			}
		});
		
		textViewAddLevel = (TextView) findViewById(R.id.button1);
		textViewAddLevel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				launchAddLevel();
			}
		});
		
	
		listView = (ListView) findViewById(R.id.listView1);
		listLevel = new ArrayList<String>();
		
				
		getApplicationContext();
		File f=getApplicationContext().getDir("levels",Context.MODE_PRIVATE);
		String[] listFiles=f.list();
		for(int i=0;i<listFiles.length;i++){
			if(listFiles[i].contains(".xml")){
				listLevel.add(listFiles[i]);
			}
		}
		
		adapter = new ArrayLevelAdapter(this, listLevel);
		listView.setAdapter(adapter);
	}
	
	private void launchAddLevel(){
		
		Intent intent = new Intent(getApplicationContext(),
				LevelFormActivity.class);
		intent.putExtra("levelType", 0);
		startActivity(intent);
	}
	
}
