package fr.esipe.android.escapeir.api.weapon;

/**
 * Class Missile represent Missile Weapon in game, for more informations see
 * Weapon Class
 * 
 * @author J.LOR C.PERILLOUS
 * @version 1.0.0
 */
public class Missile {

	/**
	 * The name.
	 */
	private String name = "Missile";

	/**
	 * The path of the bitmap.
	 */
	private int path;

	/**
	 * If the weapon belongs to the player or not.
	 */
	private boolean isPlayer;

	/**
	 * Constructor.
	 * 
	 * @param path
	 *            The path.
	 * @param isPlayer
	 *            If the weapon belongs to the player or not.
	 */
	public Missile(int path, boolean isPlayer) {
		this.setPath(path);
		this.isPlayer = isPlayer;
	}

	/**
	 * Get the path of the bitmap.
	 * 
	 * @return the path
	 */
	public int getPath() {
		return path;
	}

	/**
	 * Set the path of the bitmap.
	 * 
	 * @param path
	 *            the path to set
	 */
	public void setPath(int path) {
		this.path = path;
	}

	/**
	 * If the weapon belongs to the player or not.
	 * 
	 * @return <code>true</code> if the weapon belongs to the player,
	 *         <code>false</code> otherwise.
	 */
	public boolean isPlayer() {
		return isPlayer;
	}

	/**
	 * Get the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

}
