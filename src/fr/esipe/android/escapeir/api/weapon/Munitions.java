package fr.esipe.android.escapeir.api.weapon;

/**
 * This class implements munitions of each Weapon.
 * 
 * @author J.LOR C.PERILLOUS
 * @version 1.0.0
 * 
 */
public class Munitions {

	/**
	 * The weapon name.
	 */
	private final String weapon;

	/**
	 * Number of ammos.
	 */
	private int nbAmmos;

	/**
	 * If ammos are empty for a weapon.
	 */
	private boolean isEmpty;

	/**
	 * Constructor.
	 * 
	 * @param weapon
	 *            The weapon name.
	 * @param nbAmmos
	 *            The number of ammos.
	 */
	public Munitions(String weapon, int nbAmmos) {
		this.weapon = weapon;
		this.setNbAmmos(nbAmmos);
	}

	/**
	 * Decrements the ammos.
	 */
	public void remove() {
		if (nbAmmos > 1) {
			nbAmmos--;
		} else {
			nbAmmos = 0;
			isEmpty = true;
		}
	}

	/**
	 * If there is no ammo for a weapon.
	 * 
	 * @return <code>true</code> if it's empty for a weapon, <code>false</code>
	 *         otherwise.
	 */
	public boolean isEmpty() {
		return isEmpty;
	}

	/**
	 * Get the weapon.
	 * 
	 * @return the weapon name.
	 */
	public String getWeapon() {
		return weapon;
	}

	/**
	 * Get the number of ammos.
	 * 
	 * @return the number of ammos.
	 */
	public int getNbAmmos() {
		return nbAmmos;
	}

	/**
	 * Set the number of ammos for a weapon.
	 * 
	 * @param nbAmmos
	 *            The number of ammos.
	 */
	public void setNbAmmos(int nbAmmos) {
		if (nbAmmos <= 0) {
			throw new IllegalArgumentException();
		} else {
			this.nbAmmos = nbAmmos;
		}
	}

}
