package fr.esipe.android.escapeir.api.weapon;

/**
 * Class LittleShiboleet represent LittleShiboleet Weapon in game, for more
 * informations see Weapon Class
 * 
 * @author J.LOR C.PERILLOUS
 * @version 1.0.0
 */
public class LittleShiboleet {

	/**
	 * Name.
	 */
	private String name = "LittleShiboleet";

	/**
	 * The path of the Bitmap.
	 */
	private int path;

	/**
	 * If the weapon belongs to the player or not.
	 */
	private boolean isPlayer;

	/**
	 * The id of a LittleShiboleet.
	 */
	private int id;

	/**
	 * The radius.
	 */
	private int radius;

	/**
	 * Constructor.
	 * 
	 * @param path
	 *            The path of the bitmap.
	 * @param isPlayer
	 *            If the weapon belongs to the player.
	 * @param id
	 *            The id of the littleshiboleet.
	 */
	public LittleShiboleet(int path, boolean isPlayer, int id) {
		this.path = path;
		this.isPlayer = isPlayer;
		this.id = id;
		this.radius = 10;
	}

	/**
	 * The path of the bitmap.
	 * 
	 * @return the path
	 */
	public int getPath() {
		return path;
	}

	/**
	 * Set the path of the bitmap.
	 * 
	 * @param path
	 *            the path to set
	 */
	public void setPath(int path) {
		this.path = path;
	}

	/**
	 * If the weapon belongs to the player.
	 * 
	 * @return <code>true</code> if the weapon belongs to the player,
	 *         <code>false</code> otherwise.
	 */
	public boolean isPlayer() {
		return isPlayer;
	}

	/**
	 * Get the id of the littleshiboleet.
	 * 
	 * @return the id the id of the shiboleet.
	 */
	public int getId() {
		return id;
	}

	/**
	 * Get the radius.
	 * 
	 * @return the radius
	 */
	public int getRadius() {
		return radius;
	}

	/**
	 * Set the radius.
	 * 
	 * @param radius
	 *            the radius to set
	 */
	public void setRadius(int radius) {
		this.radius = radius;
	}

	/**
	 * Get the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

}
