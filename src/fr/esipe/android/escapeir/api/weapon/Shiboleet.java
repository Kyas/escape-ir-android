package fr.esipe.android.escapeir.api.weapon;

/**
 * Class Shiboleet represent Shiboleet Weapon in game, for more informations see
 * Weapon Class
 * 
 * @author J.LOR C.PERILLOUS
 * @version 1.0.0
 */
public class Shiboleet {

	/**
	 * The name.
	 */
	private String name = "Shiboleet";

	/**
	 * The path of the bitmap.
	 */
	private int path;

	/**
	 * If the weapon belongs to the player or not.
	 */
	private boolean isPlayer;

	/**
	 * The radius.
	 */
	private int radius;

	/**
	 * Display the littleshiboleet.
	 */
	private boolean displayLittleShibo;

	/**
	 * Constructor.
	 * 
	 * @param path
	 *            The path.
	 * @param isPlayer
	 *            If the weapon belongs to the player.
	 */
	public Shiboleet(int path, boolean isPlayer) {
		this.setPath(path);
		this.isPlayer = isPlayer;
		this.radius = 10;
		this.setDisplayLittleShibo(false);
	}

	/**
	 * Get the path of the bitmap.
	 * 
	 * @return the path
	 */
	public int getPath() {
		return path;
	}

	/**
	 * Set the path.
	 * 
	 * @param path
	 *            the path to set
	 */
	public void setPath(int path) {
		this.path = path;
	}

	/**
	 * If the weapon belongs to the player or not.
	 * 
	 * @return <code>true</code> if the weapon belongs to the player,
	 *         <code>false</code> otherwise.
	 */
	public boolean isPlayer() {
		return isPlayer;
	}

	/**
	 * get the radius.
	 * 
	 * @return the radius
	 */
	public int getRadius() {
		return radius;
	}

	/**
	 * Set the radius.
	 * 
	 * @param radius
	 *            the radius to set
	 */
	public void setRadius(int radius) {
		this.radius = radius;
	}

	/**
	 * If littleshiboleets are displayed.
	 * 
	 * @return the displayLittleShibo
	 */
	public boolean isDisplayLittleShibo() {
		return displayLittleShibo;
	}

	/**
	 * Set the display for littleshiboleets.
	 * 
	 * @param displayLittleShibo
	 *            the displayLittleShibo to set
	 */
	public void setDisplayLittleShibo(boolean displayLittleShibo) {
		this.displayLittleShibo = displayLittleShibo;
	}

	/**
	 * Get the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

}
