package fr.esipe.android.escapeir.api.weapon;

/**
 * Class Firaball represent Fireball Weapon in game, for more informations see
 * Weapon Class
 * 
 * @author J.LOR C.PERILLOUS
 * @version 1.0.0
 */
public class Fireball {

	/**
	 * Name.
	 */
	private String name = "Fireball";

	/**
	 * The path of the Bitmap.
	 */
	private int path;

	/**
	 * If the weapon belongs to the player or not.
	 */
	private boolean isPlayer;

	/**
	 * The radius.
	 */
	private int radius;

	/**
	 * Constructor.
	 * 
	 * @param path
	 *            The path
	 * @param isPlayer
	 *            If the weapon belongs to the player.
	 */
	public Fireball(int path, boolean isPlayer) {
		this.path = path;
		this.isPlayer = isPlayer;
		this.setRadius(10);
	}

	/**
	 * Return the path of the weapon.
	 * 
	 * @return the path.
	 */
	public int getPath() {
		return path;
	}

	/**
	 * Return if the weapon belongs to the plaer.
	 * 
	 * @return the isPlayer.
	 */
	public boolean isPlayer() {
		return isPlayer;
	}

	/**
	 * Return the name.
	 * 
	 * @return the name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Return the radius.
	 * 
	 * @return the radius.
	 */
	public int getRadius() {
		return radius;
	}

	/**
	 * @param path
	 *            the path to set
	 */
	public void setPath(int path) {
		this.path = path;
	}

	/**
	 * @param radius
	 *            the radius to set
	 */
	public void setRadius(int radius) {
		this.radius = radius;
	}

}
