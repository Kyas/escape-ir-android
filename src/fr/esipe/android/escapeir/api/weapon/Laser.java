package fr.esipe.android.escapeir.api.weapon;

/**
 * Class Laser represent Laser Weapon in game, for more informations see Weapon
 * Class
 * 
 * @author J.LOR C.PERILLOUS
 * @version 1.0.0
 */
public class Laser {

	/**
	 * The name.
	 */
	private String name = "Laser";

	/**
	 * The path of the Bitmap.
	 */
	private int path;

	/**
	 * If the weapon belongs to the player or not.
	 */
	private boolean isPlayer;

	/**
	 * Constructor.
	 * 
	 * @param path
	 *            The path.
	 * @param isPlayer
	 *            The isPlayer.
	 */
	public Laser(int path, boolean isPlayer) {
		this.path = path;
		this.isPlayer = isPlayer;
	}

	/**
	 * Return the path.
	 * 
	 * @return the path
	 */
	public int getPath() {
		return path;
	}

	/**
	 * Set the path.
	 * 
	 * @param path
	 *            the path to set
	 */
	public void setPath(int path) {
		this.path = path;
	}

	/**
	 * Return the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Return if the weapon belongs to the player.
	 * 
	 * @return the isPlayer
	 */
	public boolean isPlayer() {
		return isPlayer;
	}

	/**
	 * Set the isPlayer.
	 * 
	 * @param isPlayer
	 *            the isPlayer to set
	 */
	public void setPlayer(boolean isPlayer) {
		this.isPlayer = isPlayer;
	}

}
