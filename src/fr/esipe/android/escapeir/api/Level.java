package fr.esipe.android.escapeir.api;

import java.util.ArrayList;


/**
 * Class Level represent Level Object in game
 * 
 * @author J.LOR C.PERILLOUS
 * @version 1.0.0
 */
public class Level {


	/**
	 * Level's name
	 */
	String name;

	/**
	 * table of maps, the map chosen depends on the size of screen
	 */
	ArrayList<Map> maps;

	/**
	 * the player of level
	 */
	Player player;

	/**
	 * table of enemies in the current level
	 */
	ArrayList<Enemy> enemyList;

	/**
	 * Boss final
	 */
	Boss boss;

	/**
	 * scenario of the level
	 */
	Scenario scenario;
	
	
	/**
	 * duration of scenario int sec
	 */
	int duration;
	
	public Level(){
		this.name = "Level";
		this.maps = new ArrayList<Map>();
		this.player = null;
		this.enemyList = new ArrayList<Enemy>();
		this.boss = null;
		this.scenario = null;
	}

	/**
	 * Builder
	 * 
	 * @param name
	 * @param maps
	 * @param player
	 * @param enemy
	 * @param boss
	 * @param scenario
	 */
	public Level(String name, ArrayList<Map> maps, Player player, ArrayList<Enemy> enemy,
			Boss boss, Scenario scenario) {
		this.name = name;
		this.maps = maps;
		this.player = player;
		this.enemyList = enemy;
		this.boss = boss;
		this.scenario = scenario;
	}

	
	/**
	 * 
	 * @return name item
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @return maps item
	 */
	public ArrayList<Map> getMaps() {
		return maps;
	}

	/**
	 * 
	 * @return player item
	 */
	public Player getPlayer() {
		return player;
	}

	/**
	 * 
	 * @return ennemi item
	 */
	public ArrayList<Enemy> getEnemy() {
		return enemyList;
	}

	/**
	 * 
	 * @return boss item
	 */
	public Boss getBoss() {
		return boss;
	}

	/**
	 * 
	 * @return scenario item
	 */
	public Scenario getScenario() {
		return scenario;
	}

	
	/**
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @param maps
	 */
	public void setMaps(ArrayList<Map> maps) {
		this.maps = maps;
	}

	/**
	 * 
	 * @param player
	 */
	public void setPlayer(Player player) {
		this.player = player;
	}

	/**
	 * 
	 * @param enemy
	 */
	public void setEnnemi(ArrayList<Enemy> enemy) {
		this.enemyList = enemy;
	}

	/**
	 * 
	 * @param boss
	 */
	public void setBoss(Boss boss) {
		this.boss = boss;
	}

	/**
	 * 
	 * @param scenario
	 */
	public void setScenario(Scenario scenario) {
		this.scenario = scenario;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}
	
	
}
