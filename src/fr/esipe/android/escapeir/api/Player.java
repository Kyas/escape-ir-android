package fr.esipe.android.escapeir.api;

/**
 * Class Player Represents caracteristics of player.
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 * 
 */
public class Player {


	/**
	 * path of image Resource
	 */
	int path;

	/**
	 * initial number of life
	 */
	static int life;

	/**
	 * number of missile weapon
	 */
	int missileCount;

	/**
	 * number of fireball weapon
	 */
	int fireballCount;

	/**
	 * number of shiboleet weapon
	 */
	int shiboleetCount;

	/**
	 * number of laser weapon
	 */
	int laserCount;

	/**
	 * If the enemy is alive.
	 */
	boolean alive;




	/**
	 * Builder
	 * 
	 * @param path
	 *            The path.
	 * @param life
	 *            The life.
	 * @param missileCount
	 *            The number of missile.
	 * @param fireballCount
	 *            The number of fireball.
	 * @param shiboleetCount
	 *            The number of Shiboleet.
	 * @param laserCount
	 *            The number of laser.
	 */
	public Player(int path, int life, int missilCount, int fireballCount,
			int shiboleetCount, int laserCount) {
		this.path = path;
		Player.life = life;
		this.missileCount = missilCount;
		this.fireballCount = fireballCount;
		this.shiboleetCount = shiboleetCount;
		this.laserCount = laserCount;
	}



	/**
	 * GETTERS
	 */



	/**
	 * Return the player path.
	 * 
	 * @return path item
	 */
	public int getPath() {
		return path;
	}

	/**
	 * Return the initial number of life.
	 * 
	 * @return initial number of life
	 */
	public static int getLife() {
		return life;
	}

	/**
	 * Return the number of missile.
	 * 
	 * @return number of missile
	 */
	public int getMissileCount() {
		return missileCount;
	}

	/**
	 * Return the number of fireball.
	 * 
	 * @return number of fireball
	 */
	public int getFireballCount() {
		return fireballCount;
	}

	/**
	 * Return the number of shiboleet.
	 * 
	 * @return number of shiboleet
	 */
	public int getShiboleetCount() {
		return shiboleetCount;
	}

	/**
	 * Return the number of laser
	 * 
	 * @return number of laser
	 */
	public int getLaserCount() {
		return laserCount;
	}

	/**
	 * SETTERS
	 */



	/**
	 * Set the player path.
	 * 
	 * @param path
	 *            The player path.
	 */
	public void setPath(int path) {
		this.path = path;
	}

	/**
	 * Set the player life.
	 * 
	 * @param life
	 *            The player life.
	 */
	public static void setLife(int life) {
		Player.life = life;
	}

	/**
	 * Set the number of missile.
	 * 
	 * @param missileCount
	 *            The number of missile.
	 */
	public void setMissileCount(int missileCount) {
		this.missileCount = missileCount;
	}

	/**
	 * Set the number of fireball.
	 * 
	 * @param fireballCount
	 *            The number of fireball.
	 */
	public void setFireballCount(int fireballCount) {
		this.fireballCount = fireballCount;
	}

	/**
	 * Set the number of shiboleet.
	 * 
	 * @param shiboleetCount
	 *            The number of shiboleet.
	 */
	public void setShiboleetCount(int shiboleetCount) {
		this.shiboleetCount = shiboleetCount;
	}

	/**
	 * Set the number of laser.
	 * 
	 * @param laserCount
	 *            The number of laser.
	 */
	public void setLaserCount(int laserCount) {
		this.laserCount = laserCount;
	}

	/**
	 * @return the alive
	 */
	public boolean isAlive() {
		return alive;
	}

	/**
	 * @param alive
	 *            the alive to set
	 */
	public void setAlive(boolean alive) {
		this.alive = alive;
	}

}
