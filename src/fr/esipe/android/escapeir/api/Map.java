package fr.esipe.android.escapeir.api;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * Class Map Map is a backgroung image of level.
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 * 
 */
public class Map {

	/**
	 * name for Map
	 */
	
	final String name;

	/**
	 * path for Image
	 */
	final int path;

	/**
	 * Bitmap for the Image
	 */
	final Bitmap bitmap;

	

	/**
	 * Builder
	 * 
	 * @param name
	 * @param path
	 * @param height
	 * @param width
	 */
	public Map(String name, int path) {
		
		this.name=name;
		this.path = path;
		this.bitmap = null;
	}

	public Map(Context context,String name, int path) {
		this.name =name;
		this.path = path;
		BitmapFactory.Options opts = new BitmapFactory.Options();
		opts.inPreferredConfig = Bitmap.Config.RGB_565;
		this.bitmap = BitmapFactory.decodeResource(context.getResources(),
				path, opts);
	}

	/**
	 * 
	 * @return name map
	 */
	public String getName(){
		return name;
	}
	

	/**
	 * 
	 * @return path Map
	 */
	public int getPath() {
		return path;
	}


	/**
	 * 
	 * @return bitmap
	 */
	public Bitmap getBitmap() {
		return bitmap;
	}

}
