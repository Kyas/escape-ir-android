package fr.esipe.android.escapeir.api;


import java.util.ArrayList;
import java.util.Random;

import android.graphics.Point;

/**
 * Class Ennemi represent Ennemi Object in game, Ennemi was a ship which must be
 * kill during the game
 * 
 * @author J.LOR C.PERILLOUS
 * @version 1.0.0
 */
public class Enemy {
	
	/**
	 * id of enemy
	 */
	int id;

	/**
	 * name of enemy
	 */
	String name;

	/**
	 * path of image resource
	 */
	int path;


	/**
	 * number life before being kill
	 */
	int life;

	/**
	 * period of time between two shoots
	 */
	int shootTime;

	/**
	 * available weapons chosen randomly
	 */
	boolean[] weapons;


	/**
	 * represents the start position for the enemy
	 */
	StartPositionEnum startPosition;

	/**
	 * for the movement (vertical scrolling).
	 */
	boolean verticalScrolling;
	
	/**
	 * If the enemy is alive.
	 */
	boolean alive;
	
	ArrayList<Point> listPoint;
	
	public Enemy() {
		// For Serialization
	}

	/**
	 * Builder
	 * 
	 * @param name
	 * @param type
	 * @param path
	 * @param life
	 * @param shootTime
	 * @param weapons
	 * @param bonus
	 */
	public Enemy(String name,int id, int path, int life, int shootTime,
			boolean[] weapons, StartPositionEnum startPosition,
			boolean verticalScrolling) {
		this.name=name;
		this.path=path;
		this.life=life;
		this.shootTime=shootTime;
		this.weapons=weapons;
		this.startPosition=startPosition;
		this.verticalScrolling=verticalScrolling;
		this.id=id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the path
	 */
	public int getPath() {
		return path;
	}

	/**
	 * @param path
	 *            the path to set
	 */
	public void setPath(int path) {
		this.path = path;
	}



	/**
	 * @return the life
	 */
	public int getLife() {
		return life;
	}

	/**
	 * @param life
	 *            the life to set
	 */
	public void setLife(int life) {
		this.life = life;
	}

	/**
	 * @return the shootTime
	 */
	public int getShootTime() {
		return shootTime;
	}

	/**
	 * @param shootTime
	 *            the shootTime to set
	 */
	public void setShootTime(int shootTime) {
		this.shootTime = shootTime;
	}

	/**
	 * @return the weapons
	 */
	public boolean[] getWeapons() {
		return weapons;
	}

	/**
	 * @param weapons
	 *            the weapons to set
	 */
	public void setWeapons(boolean[] weapons) {
		this.weapons = weapons;
	}


	/**
	 * @return the startPosition
	 */
	public StartPositionEnum getStartPosition() {
		return startPosition;
	}

	/**
	 * @param startPosition
	 *            the startPosition to set
	 */
	public void setStartPosition(StartPositionEnum startPosition) {
		this.startPosition = startPosition;
	}

	/**
	 * @return the verticalScrolling
	 */
	public boolean isVerticalScrolling() {
		return verticalScrolling;
	}

	/**
	 * @param verticalScrolling
	 *            the verticalScrolling to set
	 */
	public void setVerticalScrolling(boolean verticalScrolling) {
		this.verticalScrolling = verticalScrolling;
	}

	/**
	 * @return the alive
	 */
	public boolean isAlive() {
		return alive;
	}

	/**
	 * @param alive the alive to set
	 */
	public void setAlive(boolean alive) {
		this.alive = alive;
	}
	

	/**
	 * 
	 * @return Movement of enemy
	 */
	public ArrayList<Point> getListPoint() {
		return listPoint;
	}
	
	/**
	 * change movement of enemy
	 * @param listPoint
	 */
	public void setListPoint(ArrayList<Point> listPoint) {
		this.listPoint = listPoint;
	}

	/**
	 * 
	 * @return id field
	 */
	public int getId() {
		return id;
	}

	/**
	 * 
	 * @param id field
	 */
	public void setId(int id) {
		this.id = id;
	}


	public int giveWeapon(){
		Random r=new Random();
		while(true){
			int a=Math.abs(r.nextInt()%4);
			if(weapons[a]){
				return a;
			}
		}
	}
}
