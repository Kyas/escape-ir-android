package fr.esipe.android.escapeir.api;

/**
 * Class Bonus represent Bonus Object in game, bonus give more life or munitions
 * to the player if he catchs them
 * 
 * @author J.LOR C.PERILLOUS
 * @version 1.0.0
 */
public class Bonus {

	/**
	 * id of bonus
	 */
	int id;

	/**
	 * path of image resource
	 */
	int path;

	/**
	 * Builder
	 * 
	 * @param id
	 * @param path
	 */
	public Bonus(int id, int path) {
		this.id = id;
		this.path = path;
	}

	/**
	 * 
	 * @return id field
	 */
	public int getId() {
		return id;
	}

	/**
	 * 
	 * @return path field
	 */
	public int getPath() {
		return path;
	}

	/**
	 * change id field
	 * 
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * change path field
	 * 
	 * @param path
	 */
	public void setPath(int path) {
		this.path = path;
	}
}
