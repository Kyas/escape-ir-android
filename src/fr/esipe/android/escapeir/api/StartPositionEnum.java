package fr.esipe.android.escapeir.api;

/**
 * Enum class - Represent the start position to add one enemy
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 */
public enum StartPositionEnum {
	TOPLEFT("top - left"), TOPCENTER("top - center"), TOPRIGHT("top - right"), CENTERLEFT(
			"center - left"), CENTER("center"), CENTERRIGHT("center - right"), BOTTOMLEFT(
			"bottom - left"), BOTTOMCENTER("bottom - center"), BOTTOMRIGHT(
			"bottom - right");

	/**
	 * String Value of enum
	 */
	private String value;

	/**
	 * initialise Value of enum
	 * 
	 * @param value
	 */
	private StartPositionEnum(final String value) {
		this.value = value;
	}

	/**
	 * change value of enum
	 * 
	 * @param value
	 */
	public void setValue(final String value) {
		this.value = value;
	}

	/**
	 * change value of enum
	 * 
	 * @param o
	 */
	public void setValue(final Object o) {
		if (o instanceof String) {
			this.value = (String) o;
		}
	}

	/**
	 * get value of Enum
	 * 
	 * @return
	 */
	public String getValue() {
		return value;
	}

	/**
	 * print the value of Enum
	 */
	@Override
	public String toString() {
		return value;
	}
}