package fr.esipe.android.escapeir.api;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Class Scenario The scenario describes the last of the game It indicate the
 * placement of ennemis according to time
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 */
public class Scenario {

	/**
	 * The last of level
	 */
	int time;

	/**
	 * The scenario
	 */
	ArrayList<TimeEnemy> play;

	/**
	 * Builder
	 * 
	 * @param time
	 */
	public Scenario(int time, ArrayList<TimeEnemy> play) {
		this.time = time;
		this.play = play;
	}

	public Scenario() {
		this.time = 0;
		this.play = new ArrayList<TimeEnemy>();
	}

	/**
	 * 
	 * @return time item
	 */
	public int getTime() {
		return time;
	}

	/**
	 * 
	 * @return play item
	 */
	public ArrayList<TimeEnemy> getPlay() {
		return play;
	}

	/**
	 * change time
	 * 
	 * @param time
	 */
	public void setTime(int time) {
		this.time = time;
	}

	/**
	 * add new Enemy to the scenario
	 * 
	 * @param time
	 * @param enemy
	 */
	public void add(int time, Enemy enemy) {
		play.add(new TimeEnemy(time, enemy));
		Collections.sort(play);
	}

	/**
	 * TimeEnemy is an Enemy associate to a time. Enemy appear on the map at the
	 * signaled time
	 * 
	 * @author celine
	 * 
	 */
	public class TimeEnemy implements Comparable<TimeEnemy> {
		int time;
		Enemy enemy;

		public TimeEnemy(int time, Enemy enemy) {

			this.time = time;
			this.enemy = enemy;
		}

		@Override
		public int compareTo(TimeEnemy arg0) {
			if (arg0.time < time) {
				return 1;
			}
			if (arg0.time > time) {
				return -1;
			}
			return 0;
		}

		public int getTime() {
			return time;
		}

		public void setTime(int time) {
			this.time = time;
		}

		public Enemy getEnemy() {
			return enemy;
		}

		public void setEnemy(Enemy enemy) {
			this.enemy = enemy;
		}

	}

}
