package fr.esipe.android.escapeir.api;

import java.util.ArrayList;
import java.util.Random;

import android.graphics.Point;

/**
 * Class Boss represent Boss Object in game
 * 
 * @author J.LOR C.PERILLOUS
 * @version 1.0.0
 */
public class Boss {

	/**
	 * Boss' name
	 */
	String name;

	/**
	 * path for image Resource
	 */
	int path;

	/**
	 * number of life
	 */
	static int life;

	/**
	 * period of time for shooting weapon
	 */
	int shootTime;

	/**
	 * available weapons
	 */
	boolean[] weapons;

	/**
	 * represents the start position for the boss
	 */
	StartPositionEnum startPosition;

	/**
	 * Movement of boss
	 */
	ArrayList<Point> movement;

	/**
	 * Builder
	 * 
	 * @param name
	 * @param path
	 * @param life
	 * @param shoot
	 * @param weapons
	 * @param bonus
	 */
	public Boss(String name, int path, int life, int shoot, boolean[] weapons) {
		this.name = name;
		this.path = path;
		Boss.life = life;
		this.shootTime = shoot;
		this.weapons = weapons;
		movement = new ArrayList<Point>();
	}

	/**
	 * GETTERS
	 */

	/**
	 * 
	 * @return name item
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @return path item
	 */
	public int getPath() {
		return path;
	}

	/**
	 * 
	 * @return life item
	 */
	public static int getLife() {
		return life;
	}

	/**
	 * Return shoot.
	 * 
	 * @return shoot item
	 */
	public int getShoot() {
		return shootTime;
	}

	/**
	 * Return weapons.
	 * 
	 * @return weapons item
	 */
	public boolean[] getWeapons() {
		return weapons;
	}

	/**
	 * SETTERS
	 */

	/**
	 * Set the string name.
	 * 
	 * @param name
	 *            The boss name.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Set the path.
	 * 
	 * @param path
	 *            the path.
	 */
	public void setPath(int path) {
		this.path = path;
	}

	/**
	 * Set the boss life
	 * 
	 * @param life
	 *            The life.
	 */
	public static void setLife(int life) {
		Boss.life = life;
	}

	/**
	 * Set the shoot time.
	 * 
	 * @param shootTime
	 *            the shoot time.
	 */
	public void setShoot(int shootTime) {
		this.shootTime = shootTime;
	}

	/**
	 * Set the list of weapons.
	 * 
	 * @param weapons
	 *            The weapons.
	 */
	public void setWeapons(boolean[] weapons) {
		this.weapons = weapons;
	}

	/**
	 * 
	 * @return movement field
	 */
	public ArrayList<Point> getMovement() {
		return movement;
	}

	/**
	 * change movement field
	 * 
	 * @param movement
	 */
	public void setMovement(ArrayList<Point> movement) {
		this.movement = movement;
	}

	/**
	 * give a random weapon
	 * 
	 * @return
	 */
	public int giveWeapon() {
		Random r = new Random();
		while (true) {
			int a = Math.abs(r.nextInt() % 4);
			if (weapons[a]) {
				return a;
			}
		}
	}

	/**
	 * 
	 * @return startPosition of boss
	 */
	public StartPositionEnum getStartPosition() {
		return startPosition;
	}

}
