package fr.esipe.android.escapeir;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import fr.esipe.android.escapeir.builder.BuilderActivity;
import fr.esipe.android.escapeir.game.SelectLevelActivity;
import fr.esipe.android.escapeir.menu.CreditsActivity;
import fr.esipe.android.escapeir.menu.HelpActivity;


/**
 * Class EscapeIRApp - Main activity for the game
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 */
public class EscapeIRApp extends Activity implements OnClickListener {

	private Button buttonNewGame;
	private Button buttonBuildLevel;
	private Button buttonOptions;
	private Button buttonCredits;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu_activity);

		buttonNewGame = (Button) findViewById(R.id.menu_button_new_game);
		buttonBuildLevel = (Button) findViewById(R.id.menu_button_build_level);
		buttonOptions = (Button) findViewById(R.id.menu_button_help);
		buttonCredits = (Button) findViewById(R.id.menu_button_credits);

		buttonNewGame.setOnClickListener(this);
		buttonBuildLevel.setOnClickListener(this);
		buttonOptions.setOnClickListener(this);
		buttonCredits.setOnClickListener(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.menu_button_new_game:
			// TODO A impl�menter pour le sc�nario (passe d'abord par la
			// selection d'un level - select_level)
			// A la selection d'un level sur la liste, on passe � GameActivity
			startActivity(new Intent(this, SelectLevelActivity.class)
					.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_NEW_TASK));
			break;
		case R.id.menu_button_build_level:
			// TODO A savoir ! Passe d'avord par la LevelListActivity (add /
			// modify level) ensuite � la selection d'une map, on passe dans
			// LevelFormActivity !
			startActivity(new Intent(this, BuilderActivity.class)
					.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_NEW_TASK));
			break;
		case R.id.menu_button_help:
			startActivity(new Intent(this, HelpActivity.class)
					.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_NEW_TASK));
			break;
		case R.id.menu_button_credits:
			startActivity(new Intent(this, CreditsActivity.class)
					.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_NEW_TASK));
			break;
		}

	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}
}
