package fr.esipe.android.escapeir.box2d.collisions;

import org.jbox2d.callbacks.ContactImpulse;
import org.jbox2d.callbacks.ContactListener;
import org.jbox2d.collision.Manifold;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.contacts.Contact;

import fr.esipe.android.escapeir.api.Bonus;
import fr.esipe.android.escapeir.api.Boss;
import fr.esipe.android.escapeir.api.Player;
import fr.esipe.android.escapeir.box2d.BonusDrawableEntity;
import fr.esipe.android.escapeir.box2d.BossDrawableEntity;
import fr.esipe.android.escapeir.box2d.EnemyDrawableEntity;
import fr.esipe.android.escapeir.box2d.PlayerDrawableEntity;
import fr.esipe.android.escapeir.box2d.WeaponDrawableEntity;
import fr.esipe.android.escapeir.box2d.WorldBox2D;
import fr.esipe.android.escapeir.game.GameActivity;

/**
 * Manage all collisions in the World environment.
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 */
public class Collision implements ContactListener {

	/**
	 * Get the highscore of the game.
	 */
	private int highscore = WorldBox2D.getHighscore();

	/**
	 * To permit to the player to touch once the boss and not die directly.
	 */
	private boolean isTouchBoss = false;

	/**
	 * Manage the beginning of collisions of two fixtures.
	 */
	@Override
	public void beginContact(Contact contact) {

		try {

			// Object with fixture A.
			EntityType tA = (EntityType) contact.getFixtureA().getUserData();

			// Object with fixture B.
			EntityType tB = (EntityType) contact.getFixtureB().getUserData();

			/**
			 * Player <-> Enemies or WeaponEnemies or Bonus.
			 */
			if (tA == EntityType.PLAYER) {
				PlayerDrawableEntity playerEntity = (PlayerDrawableEntity) contact
						.getFixtureA().getBody().getUserData();

				int newLife = Player.getLife() - 1;
				switch (tB) {
				case ENEMIES:
					EnemyDrawableEntity enemyEntity = (EnemyDrawableEntity) contact
							.getFixtureB().getBody().getUserData();

					// The enemy dies directly.
					enemyEntity.setLife(0);
					// Partie Bonus
					if (!playerEntity.hasShield()) {
						Player.setLife(newLife);
						highscore += 50;
					} else {
						// The player has never the shield anymore.
						highscore += 200;
						playerEntity.setShield(false);
					}

					Player.setLife(newLife);
					WorldBox2D.setHighscore(highscore);
				case BOSS:
					int newLifeB = Boss.getLife() - 1;
					if (!isTouchBoss) {
						Boss.setLife(newLifeB);
						isTouchBoss = true;
					}
					// Partie Bonus
					if (!playerEntity.hasShield()) {
						if (!isTouchBoss) {
							Player.setLife(newLife);
							isTouchBoss = true;
						}
						highscore += 50;
					} else {
						// The player has never the shield anymore.
						highscore += 200;
						playerEntity.setShield(false);
					}

					WorldBox2D.setHighscore(highscore);

					break;
				// WeaponEnemy
				case WEAPONENEMY:

					WeaponDrawableEntity weaponEnemy = (WeaponDrawableEntity) contact
							.getFixtureB().getBody().getUserData();
					Player.setLife(newLife);
					weaponEnemy.setAlive(false);
					break;
				// WALL
				case WALL:
					break;
				// BONUS
				case BONUS:
					BonusDrawableEntity bonusEntity = (BonusDrawableEntity) contact
							.getFixtureB().getBody().getUserData();
					Bonus bonus = bonusEntity.getBonus();
					playerEntity.takeBonus(bonus);
					bonusEntity.setAlive(false);
					highscore += 1000;
					WorldBox2D.setHighscore(highscore);
					break;
				default:
					break;
				}
			}

			/**
			 * Enemies <-> Player or WeaponPlayer or Wall
			 */
			if (tA == EntityType.ENEMIES) {
				EnemyDrawableEntity enemyEntity = (EnemyDrawableEntity) contact
						.getFixtureA().getBody().getUserData();
				int newLifeE = enemyEntity.getLife() - 1;

				switch (tB) {
				// Player
				case PLAYER:
					// Partie Bonus
					PlayerDrawableEntity playerEntity = (PlayerDrawableEntity) contact
							.getFixtureB().getBody().getUserData();
					int newLifeP = Player.getLife() - 1;
					if (!playerEntity.hasShield()) {
						Player.setLife(newLifeP);
					} else {
						playerEntity.setShield(false);
					}

					enemyEntity.setLife(newLifeE);
					highscore += 500;
					WorldBox2D.setHighscore(highscore);
					break;
				// WeaponPlayer
				case MISSILEPLAYER:
				case FIREBALLPLAYER:
				case SHIBOLEETPLAYER:
					WeaponDrawableEntity weaponPlayer = (WeaponDrawableEntity) contact
							.getFixtureB().getBody().getUserData();
					enemyEntity.setLife(newLifeE);
					weaponPlayer.setAlive(false);
					highscore += 500;
					WorldBox2D.setHighscore(highscore);
					break;
				case LASERPLAYER:
					enemyEntity.setLife(newLifeE);
					highscore += 1000;
					WorldBox2D.setHighscore(highscore);
					break;
				// Wall
				case WALL:
					enemyEntity.setLife(0);
					enemyEntity.getEnemy().setAlive(false);
					break;
				default:
					break;
				}
			}

			/**
			 * Boss <-> Player or WeaponPlayer or Wall
			 */
			if (tA == EntityType.BOSS) {
				int newLifeB = Boss.getLife() - 1;

				switch (tB) {
				// Player
				case PLAYER:
					PlayerDrawableEntity playerEntity = (PlayerDrawableEntity) contact
							.getFixtureB().getBody().getUserData();
					int newLifeP = Player.getLife() - 1;
					if (!playerEntity.hasShield()) {
						if (!isTouchBoss) {
							Player.setLife(newLifeP);
							isTouchBoss = true;
						}
					} else {
						playerEntity.setShield(false);
					}

					if (!isTouchBoss) {
						Boss.setLife(newLifeB);
					}
					highscore += 10000;
					WorldBox2D.setHighscore(highscore);
					break;
				// WeaponPlayer
				case MISSILEPLAYER:
				case FIREBALLPLAYER:
				case SHIBOLEETPLAYER:
					WeaponDrawableEntity weaponPlayer = (WeaponDrawableEntity) contact
							.getFixtureB().getBody().getUserData();
					weaponPlayer.setAlive(false);

					Boss.setLife(newLifeB);

					highscore += 500;
					WorldBox2D.setHighscore(highscore);
					break;
				case LASERPLAYER:
					WeaponDrawableEntity laser = (WeaponDrawableEntity) contact
							.getFixtureB().getBody().getUserData();
					laser.setAlive(false);

					Boss.setLife(newLifeB);

					highscore += 1000;
					WorldBox2D.setHighscore(highscore);
					break;
				// Wall
				case WALL:
					break;
				default:
					break;
				}
			}

			/**
			 * WeaponPlayer <-> Enemies or WeaponEnemy or Wall
			 */
			if (tA == EntityType.MISSILEPLAYER
					|| tA == EntityType.FIREBALLPLAYER
					|| tA == EntityType.SHIBOLEETPLAYER
					|| tA == EntityType.LASERPLAYER) {
				WeaponDrawableEntity weaponPlayer = (WeaponDrawableEntity) contact
						.getFixtureA().getBody().getUserData();
				switch (tB) {
				// Enemies
				case ENEMIES:
				case BOSS:
					weaponPlayer.setAlive(false);
					EnemyDrawableEntity enemyEntity = (EnemyDrawableEntity) contact
							.getFixtureB().getBody().getUserData();
					// Enemy enemy = enemyEntity.getEnemy();
					// int newLifeEnemy = enemy.getLife() - 1;
					int newLifeEnemy = enemyEntity.getLife() - 1;

					enemyEntity.setLife(newLifeEnemy);

					highscore += 10000;
					WorldBox2D.setHighscore(highscore);
					break;
				// WeaponEnemy
				case WEAPONENEMY:
					WeaponDrawableEntity weaponEnemy = (WeaponDrawableEntity) contact
							.getFixtureA().getBody().getUserData();

					weaponPlayer.setAlive(false);
					weaponEnemy.setAlive(false);
					highscore += 10;
					WorldBox2D.setHighscore(highscore);
					break;
				// Wall
				case WALL:
					weaponPlayer.setAlive(false);
					break;
				default:
					break;
				}
			}

			/**
			 * weaponEnemy <-> Player or WeaponPlayer or Wall
			 */
			if (tA == EntityType.WEAPONENEMY) {
				WeaponDrawableEntity weaponEnemy = (WeaponDrawableEntity) contact
						.getFixtureA().getBody().getUserData();
				switch (tB) {
				// Player
				case PLAYER:
					weaponEnemy.setAlive(false);

					PlayerDrawableEntity playerEntity = (PlayerDrawableEntity) contact
							.getFixtureB().getBody().getUserData();
					int newLifeP = Player.getLife() - 1;
					if (!playerEntity.hasShield()) {
						Player.setLife(newLifeP);
					} else {
						playerEntity.setShield(false);
					}

					highscore += 50;
					WorldBox2D.setHighscore(highscore);
					break;
				// WeaponPlayer
				case MISSILEPLAYER:
				case FIREBALLPLAYER:
				case SHIBOLEETPLAYER:
				case LASERPLAYER:
					WeaponDrawableEntity weaponPlayer = (WeaponDrawableEntity) contact
							.getFixtureB().getBody().getUserData();
					weaponPlayer.setAlive(false);
					weaponEnemy.setAlive(false);
					highscore += 10;
					WorldBox2D.setHighscore(highscore);
					break;
				// Wall
				case WALL:
					weaponEnemy.setAlive(false);
					break;
				default:
					break;
				}
			}

			/**
			 * Wall <-> Player or Enemies Boss or WeaponPlayer or WeaponEnemies
			 * or Bonus
			 */
			if (tA == EntityType.WALL) {
				switch (tB) {
				case PLAYER:
					break;
				// Enemies
				case ENEMIES:
					EnemyDrawableEntity enemyEntity = (EnemyDrawableEntity) contact
							.getFixtureB().getBody().getUserData();
					enemyEntity.setLife(0);
					break;
				case BOSS:
					BossDrawableEntity bossEntity = (BossDrawableEntity) contact
							.getFixtureB().getBody().getUserData();
					if (bossEntity.getPosition().x < 0
							&& bossEntity.getPosition().y < 0) {
						bossEntity.setDirection(new Vec2(
								GameActivity.SCREEN_WIDTH / 2,
								GameActivity.SCREEN_HEIGHT / 2));
					} else {
						bossEntity.setDirection(new Vec2(
								-GameActivity.SCREEN_WIDTH / 2,
								-GameActivity.SCREEN_HEIGHT / 2));
					}
					break;
				// Weapon
				case MISSILEPLAYER:
				case FIREBALLPLAYER:
				case SHIBOLEETPLAYER:
				case LASERPLAYER:
				case WEAPONENEMY:
					WeaponDrawableEntity weapon = (WeaponDrawableEntity) contact
							.getFixtureB().getBody().getUserData();
					weapon.setAlive(false);
					break;
				case BONUS:
					BonusDrawableEntity bonusEntity = (BonusDrawableEntity) contact
							.getFixtureB().getBody().getUserData();
					bonusEntity.setAlive(false);
					break;
				default:
					break;
				}

				/**
				 * Bonus <-> Player or Wall
				 */
				if (tA == EntityType.BONUS) {
					BonusDrawableEntity bonusEntity = (BonusDrawableEntity) contact
							.getFixtureA().getBody().getUserData();
					Bonus bonus = bonusEntity.getBonus();

					switch (tB) {
					// Player
					case PLAYER:
						PlayerDrawableEntity playerEntity = (PlayerDrawableEntity) contact
								.getFixtureB().getBody().getUserData();
						playerEntity.takeBonus(bonus);
						bonusEntity.setAlive(false);
						break;
					// Wall
					case WALL:
						bonusEntity.setAlive(false);
						break;
					default:
						break;
					}
				}
			}
		} catch (Exception e) {
			// Do nothing.
		}

	}

	/**
	 * Manage the end of collisions in a contact.
	 */
	@Override
	public void endContact(Contact contact) {

	}

	/**
	 * Manage the preSolve of collisions.
	 */
	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {

	}

	/**
	 * Manage the postSolve of collisions.
	 */
	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
	}

}
