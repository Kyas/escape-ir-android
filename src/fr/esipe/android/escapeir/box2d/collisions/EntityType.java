package fr.esipe.android.escapeir.box2d.collisions;

/**
 * This enum class is used to input an enum in our fixture definition of our
 * bodies. It is mainly used for collisions to see in details which EntityType
 * we are looking for.
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 * 
 */
public enum EntityType {

	/**
	 * Wall from the WorldBox2D
	 */
	WALL("Wall"),

	/**
	 * SpaceshipPlayer.
	 */
	PLAYER("Player"),

	/**
	 * Spaceshipenemies
	 */
	ENEMIES("Enemies"), BOSS("Boss"),

	/**
	 * Weapons From Player
	 */
	MISSILEPLAYER("MissilePlayer"), FIREBALLPLAYER("FireballPlayer"), SHIBOLEETPLAYER(
			"ShiboleetPlayer"), LASERPLAYER("LaserPlayer"),

	/**
	 * Weapons From Enemy
	 */
	WEAPONENEMY("WeaponEnemy"),

	/**
	 * Bonus Type.
	 */
	BONUS("Bonus");

	/**
	 * The type.
	 */
	private String type;

	/**
	 * Set the EntityType.
	 * 
	 * @param type
	 *            The type.
	 */
	EntityType(String type) {
		this.type = type;
	}

	/**
	 * Get the type.
	 * 
	 * @return the type.
	 */
	public String getType() {
		return this.type;
	}
}
