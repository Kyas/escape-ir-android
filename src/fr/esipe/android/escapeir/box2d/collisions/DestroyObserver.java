package fr.esipe.android.escapeir.box2d.collisions;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import fr.esipe.android.escapeir.R;
import fr.esipe.android.escapeir.api.Bonus;
import fr.esipe.android.escapeir.api.Boss;
import fr.esipe.android.escapeir.api.Enemy;
import fr.esipe.android.escapeir.api.Player;
import fr.esipe.android.escapeir.box2d.BonusDrawableEntity;
import fr.esipe.android.escapeir.box2d.BossDrawableEntity;
import fr.esipe.android.escapeir.box2d.EnemyDrawableEntity;
import fr.esipe.android.escapeir.box2d.PlayerDrawableEntity;
import fr.esipe.android.escapeir.box2d.WeaponDrawableEntity;
import fr.esipe.android.escapeir.box2d.WorldBox2D;
import fr.esipe.android.escapeir.builder.form.activity.CurrentLevel;
import fr.esipe.android.escapeir.game.window.TimeWindow;

/**
 * This is a design pattern as an Observer where it is used to be a monitor. Its
 * task is to check if a body is dead in the World and we desactive / don't
 * display them in the future.
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 */
public class DestroyObserver {

	/**
	 * Monitors bodies in the world if they are still alive.
	 * 
	 * @param context
	 *            The context of the activity.
	 */
	public static void update(Context context) {

		// Remove Player.
		PlayerDrawableEntity playerEntity = WorldBox2D.getPlayer();
		if (playerEntity.isAlive()) {
			if (Player.getLife() <= 0) {
				WorldBox2D.getPlayer().setAlive(false);
				Log.d("Lose", "Lose !");
				// Fail !
				WorldBox2D.setLose(true);
			}
		}

		// Remove enemies.
		ArrayList<EnemyDrawableEntity> enemyToRemove = new ArrayList<EnemyDrawableEntity>();
		for (EnemyDrawableEntity enemy : WorldBox2D.getEnemyList()) {
			Enemy e = enemy.getEnemy();

			if (enemy.getLife() <= 0) {
				// If the enemy has a bonus.
				if (enemy.hasBonus()) {
					int bonus = enemy.getBonus();
					BonusDrawableEntity bonusEntity = null;
					switch (bonus) {
					case 0:
						Bonus ammo = new Bonus(0, R.drawable.ammo);
						bonusEntity = new BonusDrawableEntity(ammo,
								context.getResources(), enemy.getPosition());
						break;
					case 1:
						Bonus health = new Bonus(1, R.drawable.health);
						bonusEntity = new BonusDrawableEntity(health,
								context.getResources(), enemy.getPosition());
						break;
					case 2:
						Bonus shield = new Bonus(2, R.drawable.shield);
						bonusEntity = new BonusDrawableEntity(shield,
								context.getResources(), enemy.getPosition());
						break;
					}
					WorldBox2D.getBonusList().add(bonusEntity);
				}
				e.setAlive(false);
				enemyToRemove.add(enemy);
			}
		}
		for (EnemyDrawableEntity enemy : enemyToRemove) {
			WorldBox2D.remove(enemy);
		}

		// Remove boss.
		int maxTime = (int) (TimeWindow.getCurrentTime() / 1000);
		if (maxTime > CurrentLevel.getCurrentLevel().getLevel().getScenario()
				.getTime()) {
			BossDrawableEntity bossEntity = WorldBox2D.getBoss();
			if (bossEntity.isAlive()) {
				if (Boss.getLife() <= 0) {
					Log.d("Victory", "Victory !");
					WorldBox2D.getBoss().setAlive(false);
					// Victory !
					WorldBox2D.setWin(true);
				}
			}
		}

		// Remove weapons
		ArrayList<WeaponDrawableEntity> weaponsToRemove = new ArrayList<WeaponDrawableEntity>();
		for (WeaponDrawableEntity w : WorldBox2D.getWeaponsList()) {
			if (!w.isAlive()) {
				weaponsToRemove.add(w);
			}
		}
		for (WeaponDrawableEntity w : weaponsToRemove) {
			WorldBox2D.remove(w);
		}

		// Remove bonuses
		for (BonusDrawableEntity bonus : WorldBox2D.getBonusList()) {
			if (!bonus.isAlive()) {
				WorldBox2D.remove(bonus);
			}
		}
	}
}
