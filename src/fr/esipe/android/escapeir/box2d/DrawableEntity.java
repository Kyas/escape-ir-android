package fr.esipe.android.escapeir.box2d;

import org.jbox2d.common.Vec2;

import android.graphics.Bitmap;
import android.graphics.Canvas;

/**
 * Interface DrawableEntity give for all object which implements this interface,
 * a behaviour of physics item for JBOX 2D
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 */
public abstract class DrawableEntity {

	/**
	 * The bitmap.
	 */
	Bitmap bitmap;

	/**
	 * The bodyBox2D.
	 */
	BodyBox2D bodyBox2d;

	/**
	 * The direction.
	 */
	Vec2 direction;

	public DrawableEntity(Bitmap bitmap, Vec2 vec, float linearDamping,
			float density, float friction, float restitution) {
		this.bitmap = bitmap;
		direction = new Vec2(0, 0);
		bodyBox2d = new BodyBox2D(bitmap.getWidth(), bitmap.getHeight(), vec,
				bitmap, linearDamping, density, friction, restitution);
	}

	/**
	 * Get the position.
	 * 
	 * @return the position.
	 */
	public Vec2 getPosition() {
		return bodyBox2d.body.getPosition();
	}

	/**
	 * Set the direction.
	 * 
	 * @param direction
	 *            the direction.
	 */
	public void setDirection(Vec2 direction) {
		this.direction = direction;
	}

	/**
	 * Get the size of the entity.
	 * 
	 * @return
	 */
	public Vec2 getSize() {
		return new Vec2(bitmap.getWidth(), bitmap.getHeight());
	}

	/**
	 * Abstract method for move.
	 * 
	 * @param direction
	 *            The direction.
	 */
	public abstract void move(Vec2 direction);

	/**
	 * Abstract method for draw.
	 * 
	 * @param canvas
	 *            The canvas.
	 */
	public abstract void draw(Canvas canvas);
}
