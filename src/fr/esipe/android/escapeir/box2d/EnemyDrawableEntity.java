package fr.esipe.android.escapeir.box2d;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import fr.esipe.android.escapeir.api.Enemy;
import fr.esipe.android.escapeir.box2d.collisions.EntityType;
import fr.esipe.android.escapeir.box2d.collisions.Mask;
import fr.esipe.android.escapeir.game.GameActivity;

/**
 * Class EnemyDrawableEntity - The entity for Enemy.
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 */
public class EnemyDrawableEntity extends DrawableEntity {

	/**
	 * The enemy.
	 */
	Enemy enemy;

	/**
	 * The bitmap.
	 */
	Bitmap bitmap;

	/**
	 * The list of Point for movement.
	 */
	List<Point> listPoint = Collections
			.synchronizedList(new ArrayList<Point>());

	/**
	 * The cursor.
	 */
	int cursor;

	/**
	 * If the enemy entity is alive.
	 */
	boolean alive;

	/**
	 * Bonus.
	 */
	int bonus;

	/**
	 * Life.
	 */
	int life;

	/**
	 * Constructor.
	 * 
	 * @param enemy
	 *            The enemy.
	 * @param res
	 *            The resources.
	 * @param position
	 *            The position.
	 */
	public EnemyDrawableEntity(Enemy enemy, Resources res, Vec2 position) {
		super(BitmapFactory.decodeResource(res, enemy.getPath()), position,
				0.5f, 0.3f, 0.3f, 0.3f);
		this.enemy = enemy;
		this.bitmap = BitmapFactory.decodeResource(res, enemy.getPath());
		this.cursor = 0;
		this.listPoint = enemy.getListPoint();
		this.alive = true;
		listPoint = enemy.getListPoint();
		life = enemy.getLife();
		boolean putBonus = new Random().nextBoolean();
		if (putBonus == false) {
			bonus = -1;
		} else {
			bonus = Math.abs(new Random().nextInt() % 3);
		}
		bodyBox2d.shapeDef = new PolygonShape();
		bodyBox2d.shapeDef.setAsBox(bitmap.getWidth() / 2,
				bitmap.getHeight() / 2);
		bodyBox2d.body.setUserData(this);
		bodyBox2d.fixtureDef.shape = bodyBox2d.shapeDef;
		bodyBox2d.fixtureDef.userData = EntityType.ENEMIES;
		bodyBox2d.fixtureDef.filter.categoryBits = Mask.CATEGORY_ENEMIES;
		bodyBox2d.fixtureDef.filter.maskBits = Mask.MASK_ENEMIES;
		bodyBox2d.body.createFixture(bodyBox2d.fixtureDef);

	}

	/**
	 * Draw the enemy entity.
	 */
	public void draw(Canvas canvas) {
		bodyBox2d.draw(canvas, bitmap);
		move(direction);
	}

	/**
	 * Move the enemy entity.
	 */
	@Override
	public void move(Vec2 direction) {
		bodyBox2d.move(direction);
	}

	/**
	 * Get the enemy.
	 * 
	 * @return the enemy.
	 */
	public Enemy getEnemy() {
		return enemy;
	}

	/**
	 * If the enemy has a bonus.
	 * 
	 * @return <code>true</code> if the enemy has a bonus, <code>false</code>
	 *         otherwise.
	 */
	public boolean hasBonus() {
		return bonus != -1;
	}

	/**
	 * Get the bonus.
	 * 
	 * @return the bonus.
	 */
	public int getBonus() {
		return bonus;
	}

	/**
	 * Move the enemy entity for the movement.
	 */
	public void shift() {
		if (GameActivity.SCREEN_WIDTH >= 720) {
			int newCurseur = 0;
			if (cursor != listPoint.size() - 1) {
				newCurseur = cursor + 1;
			}
			direction = new Vec2(
					(listPoint.get(newCurseur).x - listPoint.get(cursor).x) * 100,
					(listPoint.get(newCurseur).y - listPoint.get(cursor).y) * 100);
			cursor = newCurseur;
		} else {
			int newCurseur = 0;
			if (cursor != listPoint.size() - 1) {
				newCurseur = cursor + 1;
			}
			direction = new Vec2(
					(listPoint.get(newCurseur).x - listPoint.get(cursor).x) * 5,
					(listPoint.get(newCurseur).y - listPoint.get(cursor).y) * 5);
			cursor = newCurseur;
		}
	}

	/**
	 * Get the life.
	 * 
	 * @return the life.
	 */
	public int getLife() {
		return life;
	}

	/**
	 * Set the life
	 * 
	 * @param life
	 *            the life to set.
	 */
	public void setLife(int life) {
		this.life = life;
	}

}
