package fr.esipe.android.escapeir.box2d;

import org.jbox2d.collision.shapes.CircleShape;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;

import android.graphics.Bitmap;
import android.graphics.Canvas;

/**
 * Class BodyBox2D
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 */
public class BodyBox2D {

	/**
	 * The BodyDef.
	 */
	BodyDef bodyDef;

	/**
	 * The body.
	 */
	Body body;

	/**
	 * The fixtureDef.
	 */
	FixtureDef fixtureDef;

	/**
	 * The polygonShape.
	 */
	PolygonShape shapeDef;

	/**
	 * TheCircleShape.
	 */
	CircleShape cShapeDef;

	/**
	 * Constructor.
	 * 
	 * @param width
	 *            width of the entity.
	 * @param height
	 *            height of the entity.
	 * @param position
	 *            the position.
	 * @param bitmap
	 *            the bitmap.
	 * @param linearDamping
	 *            the linearDamping.
	 * @param density
	 *            the density.
	 * @param friction
	 *            the friction.
	 * @param restitution
	 *            the restitution.
	 */
	public BodyBox2D(float width, float height, Vec2 position, Bitmap bitmap,
			float linearDamping, float density, float friction,
			float restitution) {
		bodyDef = new BodyDef();
		bodyDef.type = BodyType.DYNAMIC;
		bodyDef.position = position;
		bodyDef.linearDamping = linearDamping;
		bodyDef.userData = this;
		fixtureDef = new FixtureDef();
		shapeDef = new PolygonShape();
		shapeDef.setAsBox(1.0f, 1.0f);
		fixtureDef.shape = shapeDef;
		fixtureDef.density = density;
		fixtureDef.friction = friction;
		fixtureDef.restitution = restitution;
		body = WorldBox2D.getWorldBox(null).getWorld().createBody(bodyDef);
		body.createFixture(fixtureDef);
	}

	/**
	 * Move the body.
	 * 
	 * @param direction
	 *            the direction.
	 */
	public void move(Vec2 direction) {
		body.setLinearVelocity(direction);

	}

	/**
	 * Draw the body.
	 * 
	 * @param canvas
	 *            the canvas.
	 * @param bitmap
	 *            the bitmap.
	 */
	public void draw(Canvas canvas, Bitmap bitmap) {
		canvas.drawBitmap(bitmap, body.getPosition().x, body.getPosition().y,
				null);
	}
}
