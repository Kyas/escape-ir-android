package fr.esipe.android.escapeir.box2d;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.World;

import android.content.Context;
import android.content.res.Resources;
import fr.esipe.android.escapeir.R;
import fr.esipe.android.escapeir.api.Level;
import fr.esipe.android.escapeir.api.Player;
import fr.esipe.android.escapeir.api.weapon.Fireball;
import fr.esipe.android.escapeir.api.weapon.Laser;
import fr.esipe.android.escapeir.api.weapon.Missile;
import fr.esipe.android.escapeir.api.weapon.Munitions;
import fr.esipe.android.escapeir.api.weapon.Shiboleet;
import fr.esipe.android.escapeir.box2d.collisions.Collision;
import fr.esipe.android.escapeir.box2d.collisions.DestroyObserver;
import fr.esipe.android.escapeir.box2d.collisions.EntityType;
import fr.esipe.android.escapeir.box2d.collisions.Mask;
import fr.esipe.android.escapeir.game.GameActivity;

/**
 * Class WorldBox2D The World Box2D class manages all physics entities, dynamic
 * simulation
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 */
public class WorldBox2D {

	/**
	 * The world.
	 */
	private static World world;

	/**
	 * The singleton.
	 */
	private static WorldBox2D singleton;

	/**
	 * The level.
	 */
	private Level level;

	/**
	 * The spaceshipEnemyList.
	 */
	private static List<EnemyDrawableEntity> enemyList = Collections
			.synchronizedList(new ArrayList<EnemyDrawableEntity>());

	/**
	 * The weapon List.
	 */
	private static LinkedBlockingQueue<WeaponDrawableEntity> weaponsList = new LinkedBlockingQueue<WeaponDrawableEntity>();

	/**
	 * The weaponsTable for munitions.
	 */
	private static ConcurrentHashMap<String, Munitions> weaponsTable = new ConcurrentHashMap<String, Munitions>(
			4);

	/**
	 * The boss
	 */
	private static BossDrawableEntity boss = null;

	/**
	 * The Player.
	 */
	private static PlayerDrawableEntity player = null;

	/**
	 * The bonus List.
	 */
	private static LinkedBlockingQueue<BonusDrawableEntity> bonusList = new LinkedBlockingQueue<BonusDrawableEntity>();

	/**
	 * The Highscore.
	 */
	private static int highscore;

	/**
	 * The boolean win.
	 */
	private static boolean win = false;

	/**
	 * The boolean lose.
	 */
	private static boolean lose = false;

	/**
	 * The active weapon.
	 */
	private static int weaponActive = 0;

	/**
	 * The WorldBox2D.
	 * 
	 * @param level
	 *            The level.
	 */
	private WorldBox2D(Level level) {
		Vec2 vec = new Vec2(0, 0);
		world = new World(vec, true);
		world.setContinuousPhysics(true);
		world.setContactListener(new Collision());

		this.level = level;
		Player p = level.getPlayer();

		if (getWeaponsList() != null)
			getWeaponsList().clear();

		if (getWeaponsTable() != null) {
			getWeaponsTable().clear();
		}

		if (getEnemyList() != null) {
			getEnemyList().clear();
		}

		if (getBonusList() != null) {
			getBonusList().clear();
		}

		/**
		 * Ammos for weapons.
		 */
		weaponsTable.put("Missile",
				new Munitions("Missile", p.getMissileCount()));
		weaponsTable.put("Fireball",
				new Munitions("Fireball", p.getFireballCount()));
		weaponsTable.put("Shiboleet",
				new Munitions("Shiboleet", p.getShiboleetCount()));
		weaponsTable.put("Laser", new Munitions("Laser", p.getLaserCount()));

		/**
		 * Map bounds.
		 */
		// TOP CORNER
		Body body;
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.STATIC;
		bodyDef.position.set(0, -50);
		PolygonShape blockShape = new PolygonShape();
		if (GameActivity.SCREEN_WIDTH >= 720) {
			blockShape.setAsBox(GameActivity.SCREEN_WIDTH, 15 / 2);
		} else {
			blockShape.setAsBox(GameActivity.SCREEN_WIDTH, 0);
		}
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = blockShape;
		fixtureDef.userData = EntityType.WALL;
		fixtureDef.filter.categoryBits = Mask.CATEGORY_WALL;
		fixtureDef.filter.maskBits = Mask.MASK_WALL;
		fixtureDef.density = 1.f;
		fixtureDef.friction = 1.f;
		fixtureDef.restitution = .1f;

		body = world.createBody(bodyDef);
		body.createFixture(fixtureDef);

		// BOTTOM CORNER
		bodyDef = new BodyDef();
		bodyDef.type = BodyType.STATIC;
		bodyDef.position.set(GameActivity.SCREEN_WIDTH,
				GameActivity.SCREEN_HEIGHT);
		bodyDef.userData = this;
		blockShape = new PolygonShape();
		if (GameActivity.SCREEN_WIDTH >= 720) {
			blockShape.setAsBox(GameActivity.SCREEN_WIDTH, 50);
		} else {
			blockShape.setAsBox(GameActivity.SCREEN_WIDTH, 0);
		}
		fixtureDef = new FixtureDef();
		fixtureDef.shape = blockShape;
		fixtureDef.userData = EntityType.WALL;
		fixtureDef.filter.categoryBits = Mask.CATEGORY_WALL;
		fixtureDef.filter.maskBits = Mask.MASK_WALL;
		fixtureDef.density = 1.f;
		fixtureDef.friction = 1.f;
		fixtureDef.restitution = .1f;
		body = world.createBody(bodyDef);
		body.createFixture(fixtureDef);

		// LEFT CORNER
		bodyDef = new BodyDef();
		bodyDef.type = BodyType.STATIC;
		bodyDef.position.set(-30, 0);
		blockShape = new PolygonShape();
		blockShape.setAsBox(0, GameActivity.SCREEN_HEIGHT);
		fixtureDef = new FixtureDef();
		fixtureDef.shape = blockShape;
		fixtureDef.userData = EntityType.WALL;
		fixtureDef.filter.categoryBits = Mask.CATEGORY_WALL;
		fixtureDef.filter.maskBits = Mask.MASK_WALL;
		fixtureDef.density = 1.f;
		fixtureDef.friction = 1.f;
		fixtureDef.restitution = .1f;
		body = world.createBody(bodyDef);
		body.createFixture(fixtureDef);

		// RIGHT CORNER
		bodyDef = new BodyDef();
		bodyDef.type = BodyType.STATIC;
		if (GameActivity.SCREEN_WIDTH >= 720) {
			bodyDef.position.set(GameActivity.SCREEN_WIDTH - 35, 0);
		} else {
			bodyDef.position.set(GameActivity.SCREEN_WIDTH, 0);
		}
		bodyDef.userData = this;
		blockShape = new PolygonShape();
		blockShape.setAsBox(0, GameActivity.SCREEN_HEIGHT);
		fixtureDef = new FixtureDef();
		fixtureDef.shape = blockShape;
		fixtureDef.userData = EntityType.WALL;
		fixtureDef.filter.categoryBits = Mask.CATEGORY_WALL;
		fixtureDef.filter.maskBits = Mask.MASK_WALL;
		fixtureDef.density = 1.f;
		fixtureDef.friction = 1.f;
		fixtureDef.restitution = .1f;
		body = world.createBody(bodyDef);
		body.createFixture(fixtureDef);
	}

	/**
	 * Update the Map (world).
	 */
	public static void updateMap(Context context) {
		world.step(1 / 30f, 15, 8);
		DestroyObserver.update(context);
	}

	/**
	 * Shoot a weapon from player.
	 * 
	 * @param weaponActive
	 *            the active weapon.
	 * @param direction
	 *            The direction.
	 * @param sp
	 *            The Player.
	 */
	public static void shootWeaponFromPlayer(int weaponActive, Vec2 direction,
			PlayerDrawableEntity sp, Resources res) {
		switch (weaponActive) {
		case 0:
			if (!weaponsTable.get("Missile").isEmpty()) {
				final Missile missile = new Missile(R.drawable.icon_missile,
						true);
				final WeaponDrawableEntity missileEntity = new WeaponDrawableEntity(
						missile, res, new Vec2(
								sp.bodyBox2d.body.getPosition().x,
								sp.bodyBox2d.body.getPosition().y), direction);
				addWeapon(missileEntity);
				remove("Missile");
			}
			break;
		case 1:
			if (!weaponsTable.get("Fireball").isEmpty()) {
				final Fireball fireball = new Fireball(
						R.drawable.icon_fireball, true);
				final WeaponDrawableEntity fireballEntity = new WeaponDrawableEntity(
						fireball, res, new Vec2(
								sp.bodyBox2d.body.getPosition().x,
								sp.bodyBox2d.body.getPosition().y), direction);
				addWeapon(fireballEntity);
				remove("Fireball");
			}
			break;
		case 2:
			if (!weaponsTable.get("Shiboleet").isEmpty()) {
				final Shiboleet shiboleet = new Shiboleet(
						R.drawable.icon_shiboleet, true);
				final WeaponDrawableEntity shiboleetEntity = new WeaponDrawableEntity(
						shiboleet, res, new Vec2(
								sp.bodyBox2d.body.getPosition().x,
								sp.bodyBox2d.body.getPosition().y), direction);
				addWeapon(shiboleetEntity);
				remove("Shiboleet");
			}
			break;
		case 3:
			if (!weaponsTable.get("Laser").isEmpty()) {
				final Laser laser = new Laser(R.drawable.icon_laser, true);
				if (GameActivity.SCREEN_WIDTH >= 720) {
					final WeaponDrawableEntity laserEntity = new WeaponDrawableEntity(
							laser, res, new Vec2(
									sp.bodyBox2d.body.getPosition().x,
									sp.bodyBox2d.body.getPosition().y - 300),
							direction);
					addWeapon(laserEntity);
				} else {
					final WeaponDrawableEntity laserEntity = new WeaponDrawableEntity(
							laser, res, new Vec2(
									sp.bodyBox2d.body.getPosition().x,
									sp.bodyBox2d.body.getPosition().y - 100),
							direction);
					addWeapon(laserEntity);
				}
				remove("Laser");
			}
			break;
		}
	}

	/**
	 * The adding of a player.
	 * 
	 * @param p
	 *            The player.
	 */
	public static void addPlayer(PlayerDrawableEntity p) {
		if (player == null) {
			player = p;
		}
	}

	/**
	 * The adding of a weapon in weaponList.
	 * 
	 * @param w
	 *            The weapon.
	 */
	public static void addWeapon(WeaponDrawableEntity w) {
		if (w == null) {
			throw new IllegalArgumentException("Weapon is null !");
		}
		synchronized (weaponsList) {
			weaponsList.add(w);
		}

	}

	/**
	 * Remove a weapon from the weapon List and the world.
	 * 
	 * @param weapon
	 */
	public static void remove(WeaponDrawableEntity weapon) {
		world.destroyBody(weapon.bodyBox2d.body);
		weaponsList.remove(weapon);
	}

	/**
	 * Remove a enemy from the spaceshiplist.
	 * 
	 * @param enemy
	 *            The enemy.
	 */
	public static void remove(EnemyDrawableEntity enemy) {
		world.destroyBody(enemy.bodyBox2d.body);
		enemyList.remove(enemy);
	}

	/**
	 * Remove a Bonus from the bonus List and the world.
	 * 
	 * @param Bonus
	 *            The Bonus.
	 */
	public static void remove(BonusDrawableEntity bonus) {
		world.destroyBody(bonus.bodyBox2d.body);
		bonusList.remove(bonus);
	}

	/**
	 * Remove a weapon from the weapons Table
	 * 
	 * @param weapon
	 *            The weapon.
	 */
	public static void remove(String weapon) {
		if (weaponsTable.containsKey(weapon)) {
			weaponsTable.get(weapon).remove();
		} else {
			throw new IllegalArgumentException();
		}
	}

	/**
	 * Get the weapon List.
	 * 
	 * @return
	 * 
	 * @return the weapon List.
	 */
	public static LinkedBlockingQueue<WeaponDrawableEntity> getWeaponsList() {
		return weaponsList;
	}

	/**
	 * Get the Player.
	 * 
	 * @return the player.
	 */
	public static PlayerDrawableEntity getPlayer() {
		return player;
	}

	/**
	 * Get the enemylist.
	 * 
	 * @return
	 * 
	 * @return the enemy list.
	 */
	public static List<EnemyDrawableEntity> getEnemyList() {
		return enemyList;
	}

	/**
	 * Get the weapons table.
	 * 
	 * @return the weapons table
	 */
	public static ConcurrentHashMap<String, Munitions> getWeaponsTable() {
		return weaponsTable;
	}

	/**
	 * Get the bonus list.
	 * 
	 * @return the bonus list.
	 */
	public static LinkedBlockingQueue<BonusDrawableEntity> getBonusList() {
		return bonusList;
	}

	/**
	 * Get the weaponActive.
	 * 
	 * @return the weapon active.
	 */
	public static int getWeaponActive() {
		return weaponActive;
	}

	/**
	 * Set the weapon Active.
	 * 
	 * @param weaponActive
	 *            the weapon active.
	 */
	public static void setWeaponActive(int weaponActive) {
		WorldBox2D.weaponActive = weaponActive;
	}

	/**
	 * Get the highscore.
	 * 
	 * @return The highscore.
	 */
	public static int getHighscore() {
		return highscore;
	}

	/**
	 * Set the highscore.
	 * 
	 * @param hIGHSCORE
	 *            The highscore.
	 */
	public static void setHighscore(int highscore) {
		WorldBox2D.highscore = highscore;
	}

	/**
	 * If we won the game.
	 * 
	 * @return <code>true</code> if we won, <code>false</code> otherwise.
	 */
	public static boolean isWin() {
		return win;
	}

	/**
	 * Set the victory at true.
	 * 
	 * @param wIN
	 *            The boolean win.
	 */
	public static void setWin(boolean win) {
		WorldBox2D.win = win;
	}

	/**
	 * If we lost the game.
	 * 
	 * @return <code>true</code> if we lost, <code>false</code> otherwise.
	 */
	public static boolean isLose() {
		return lose;
	}

	/**
	 * Set the defeat at true.
	 * 
	 * @param lOSE
	 *            The boolean lose.
	 */
	public static void setLose(boolean lose) {
		WorldBox2D.lose = lose;
	}

	public static void createWorldBox(Level level) {
		if (singleton == null) {
			singleton = new WorldBox2D(level);
		}
	}

	public static WorldBox2D getWorldBox(Level level) {
		if (singleton == null)
			createWorldBox(level);
		return singleton;
	}

	public World getWorld() {
		return world;
	}

	public static void setPlayer(PlayerDrawableEntity player) {
		WorldBox2D.player = player;
	}

	/**
	 * @return the level
	 */
	public Level getLevel() {
		return level;
	}

	/**
	 * @param level
	 *            the level to set
	 */
	public void setLevel(Level level) {
		this.level = level;
	}

	/**
	 * @return the boss
	 */
	public static BossDrawableEntity getBoss() {
		return boss;
	}

	/**
	 * @param boss
	 *            the boss to set
	 */
	public static void setBoss(BossDrawableEntity boss) {
		WorldBox2D.boss = boss;
	}

}
