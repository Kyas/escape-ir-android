package fr.esipe.android.escapeir.box2d;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.jbox2d.collision.shapes.CircleShape;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.FixtureDef;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import fr.esipe.android.escapeir.R;
import fr.esipe.android.escapeir.api.weapon.Fireball;
import fr.esipe.android.escapeir.api.weapon.Laser;
import fr.esipe.android.escapeir.api.weapon.LittleShiboleet;
import fr.esipe.android.escapeir.api.weapon.Missile;
import fr.esipe.android.escapeir.api.weapon.Shiboleet;
import fr.esipe.android.escapeir.box2d.collisions.EntityType;
import fr.esipe.android.escapeir.box2d.collisions.Mask;

/**
 * Class WeaponDrawableEntity - The entity for the Weapon.
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 */
public class WeaponDrawableEntity extends DrawableEntity {

	/**
	 * If the weapon belongs to the player.
	 */
	boolean isPlayer;

	/**
	 * the weapon type.
	 */
	int weaponType;

	/**
	 * The temporary bitmap for littleshibos.
	 */
	Bitmap tmp;

	/**
	 * The bitmap.
	 */
	Bitmap bitmap;

	/**
	 * the fixtureDef.
	 */
	FixtureDef fixtureDef;

	/**
	 * The name.
	 */
	String name;

	/**
	 * If the weapon is alive.
	 */
	boolean alive;

	/**
	 * The object weapon.
	 */
	Object weapon;

	/**
	 * The speed.
	 */
	float speed = 10f;

	/**
	 * The list of Shibos.
	 */
	static List<WeaponDrawableEntity> listOfShibos = Collections
			.synchronizedList(new ArrayList<WeaponDrawableEntity>());

	/**
	 * Missile Constructor.
	 * 
	 * @param missile
	 *            The missile.
	 * @param res
	 *            The resources.
	 * @param position
	 *            The position.
	 * @param direction
	 *            The direction.
	 */
	public WeaponDrawableEntity(Missile missile, Resources res, Vec2 position,
			Vec2 direction) {
		super(BitmapFactory.decodeResource(res, missile.getPath()), position,
				0.5f, 0.3f, 0.3f, 0.3f);
		this.bitmap = BitmapFactory.decodeResource(res, missile.getPath());
		this.weaponType = 0;
		this.alive = true;
		this.setWeapon((Missile) missile);
		this.name = missile.getName();
		this.isPlayer = missile.isPlayer();
		this.direction = direction;
		bodyBox2d.shapeDef = new PolygonShape();
		bodyBox2d.shapeDef.setAsBox(bitmap.getWidth() / 2,
				bitmap.getHeight() / 2);
		bodyBox2d.body.setUserData(this);
		bodyBox2d.fixtureDef.shape = bodyBox2d.shapeDef;
		isPlayer = missile.isPlayer();
		if (isPlayer) {
			bodyBox2d.fixtureDef.userData = EntityType.MISSILEPLAYER;
			bodyBox2d.fixtureDef.filter.categoryBits = Mask.CATEGORY_WEAPONS_PLAYER;
			bodyBox2d.fixtureDef.filter.maskBits = Mask.MASK_WEAPONS_PLAYER;
		} else {
			bodyBox2d.fixtureDef.userData = EntityType.WEAPONENEMY;
			bodyBox2d.fixtureDef.filter.categoryBits = Mask.CATEGORY_WEAPONS_ENEMIES;
			bodyBox2d.fixtureDef.filter.maskBits = Mask.MASK_WEAPONS_ENEMIES;
		}
		this.setFixtureDef(bodyBox2d.fixtureDef);
		bodyBox2d.body.createFixture(bodyBox2d.fixtureDef);
	}

	/**
	 * Fireball Constructor.
	 * 
	 * @param fireball
	 *            The fireball.
	 * @param res
	 *            The resources.
	 * @param position
	 *            The position.
	 * @param direction
	 *            The direction.
	 */
	public WeaponDrawableEntity(Fireball fireball, Resources res,
			Vec2 position, Vec2 direction) {
		super(BitmapFactory.decodeResource(res, fireball.getPath()), position,
				0.5f, 0.3f, 0.3f, 0.3f);

		this.bitmap = BitmapFactory.decodeResource(res, fireball.getPath());
		this.direction = direction;
		this.alive = true;
		this.weaponType = 1;
		this.setWeapon((Fireball) fireball);
		this.name = fireball.getName();
		this.isPlayer = fireball.isPlayer();

		bodyBox2d.cShapeDef = new CircleShape();
		bodyBox2d.shapeDef.m_radius = 1.0f;
		// For Collision
		bodyBox2d.body.setUserData(this);
		bodyBox2d.fixtureDef.shape = bodyBox2d.shapeDef;
		if (isPlayer) {
			bodyBox2d.fixtureDef.userData = EntityType.FIREBALLPLAYER;
			bodyBox2d.fixtureDef.filter.categoryBits = Mask.CATEGORY_WEAPONS_PLAYER;
			bodyBox2d.fixtureDef.filter.maskBits = Mask.MASK_WEAPONS_PLAYER;
		} else {
			bodyBox2d.fixtureDef.userData = EntityType.WEAPONENEMY;
			bodyBox2d.fixtureDef.filter.categoryBits = Mask.CATEGORY_WEAPONS_ENEMIES;
			bodyBox2d.fixtureDef.filter.maskBits = Mask.MASK_WEAPONS_ENEMIES;
		}

		this.setFixtureDef(bodyBox2d.fixtureDef);
		bodyBox2d.body.createFixture(bodyBox2d.fixtureDef);
	}

	/**
	 * Shiboleet Constructor.
	 * 
	 * @param shiboleet
	 *            The shiboleet.
	 * @param res
	 *            The resources.
	 * @param position
	 *            The position.
	 * @param direction
	 *            The direction.
	 */
	public WeaponDrawableEntity(Shiboleet shiboleet, Resources res,
			Vec2 position, Vec2 direction) {
		super(BitmapFactory.decodeResource(res, shiboleet.getPath()), position,
				0.5f, 0.3f, 0.3f, 0.3f);
		this.bitmap = BitmapFactory.decodeResource(res, shiboleet.getPath());
		this.direction = direction;
		this.alive = true;
		this.weaponType = 2;
		this.setWeapon((Shiboleet) shiboleet);
		this.name = shiboleet.getName();
		this.isPlayer = shiboleet.isPlayer();
		bodyBox2d.cShapeDef = new CircleShape();
		bodyBox2d.cShapeDef.m_radius = 1.0f;
		bodyBox2d.body.setUserData(this);
		bodyBox2d.fixtureDef.shape = bodyBox2d.cShapeDef;
		if (isPlayer) {
			bodyBox2d.fixtureDef.userData = EntityType.SHIBOLEETPLAYER;
			bodyBox2d.fixtureDef.filter.categoryBits = Mask.CATEGORY_WEAPONS_PLAYER;
			bodyBox2d.fixtureDef.filter.maskBits = Mask.MASK_WEAPONS_PLAYER;
		} else {
			bodyBox2d.fixtureDef.userData = EntityType.WEAPONENEMY;
			bodyBox2d.fixtureDef.filter.categoryBits = Mask.CATEGORY_WEAPONS_ENEMIES;
			bodyBox2d.fixtureDef.filter.maskBits = Mask.MASK_WEAPONS_ENEMIES;
		}
		this.setFixtureDef(bodyBox2d.fixtureDef);
		bodyBox2d.body.createFixture(bodyBox2d.fixtureDef);

		listOfShibos = new LinkedList<WeaponDrawableEntity>();
		for (int i = 0; i < 5; i++) {
			LittleShiboleet ls = new LittleShiboleet(R.drawable.icon_shiboleet,
					isPlayer, i);
			WeaponDrawableEntity weaponLs = new WeaponDrawableEntity(ls, res,
					position);
			listOfShibos.add(weaponLs);
		}

	}

	/**
	 * LittleShiboleet Constructor.
	 * 
	 * @param ls
	 *            The littleshiboleet.
	 * @param res
	 *            The resources.
	 * @param position
	 *            The position.
	 */
	public WeaponDrawableEntity(LittleShiboleet ls, Resources res, Vec2 position) {
		super(BitmapFactory.decodeResource(res, ls.getPath()), position, 0.5f,
				0.3f, 0.3f, 0.3f);

		this.bitmap = BitmapFactory.decodeResource(res, ls.getPath());
		this.alive = true;
		this.weaponType = 3;
		this.setWeapon((LittleShiboleet) ls);
		this.name = ls.getName();
		this.isPlayer = ls.isPlayer();
		bodyBox2d.cShapeDef = new CircleShape();
		bodyBox2d.cShapeDef.m_radius = 1.0f;
		bodyBox2d.body.setUserData(this);
		bodyBox2d.fixtureDef.shape = bodyBox2d.cShapeDef;
		if (isPlayer) {
			bodyBox2d.fixtureDef.userData = EntityType.SHIBOLEETPLAYER;
			bodyBox2d.fixtureDef.filter.categoryBits = Mask.CATEGORY_WEAPONS_PLAYER;
			bodyBox2d.fixtureDef.filter.maskBits = Mask.MASK_WEAPONS_PLAYER;
		} else {
			bodyBox2d.fixtureDef.userData = EntityType.WEAPONENEMY;
			bodyBox2d.fixtureDef.filter.categoryBits = Mask.CATEGORY_WEAPONS_ENEMIES;
			bodyBox2d.fixtureDef.filter.maskBits = Mask.MASK_WEAPONS_ENEMIES;
		}
		this.setFixtureDef(bodyBox2d.fixtureDef);
		bodyBox2d.body.createFixture(bodyBox2d.fixtureDef);
	}

	/**
	 * Laser Constructor.
	 * 
	 * @param laser
	 *            The laser.
	 * @param res
	 *            The resources.
	 * @param position
	 *            The position.
	 * @param direction
	 *            The direction.
	 */
	public WeaponDrawableEntity(Laser laser, Resources res, Vec2 position,
			Vec2 direction) {
		super(BitmapFactory.decodeResource(res, laser.getPath()), position,
				0.5f, 0.3f, 0.3f, 0.3f);

		this.bitmap = BitmapFactory.decodeResource(res, laser.getPath());
		this.alive = true;
		this.direction = direction;
		this.weaponType = 4;
		this.setWeapon((Laser) laser);
		this.name = laser.getName();
		this.isPlayer = laser.isPlayer();
		bodyBox2d.shapeDef = new PolygonShape();
		bodyBox2d.shapeDef.setAsBox(bitmap.getWidth() / 2,
				bitmap.getHeight() / 2);
		bodyBox2d.body.setUserData(this);
		bodyBox2d.fixtureDef.shape = bodyBox2d.shapeDef;
		if (isPlayer) {
			bodyBox2d.fixtureDef.userData = EntityType.LASERPLAYER;
			bodyBox2d.fixtureDef.filter.categoryBits = Mask.CATEGORY_WEAPONS_PLAYER;
			bodyBox2d.fixtureDef.filter.maskBits = Mask.MASK_WEAPONS_PLAYER;
		} else {
			bodyBox2d.fixtureDef.userData = EntityType.WEAPONENEMY;
			bodyBox2d.fixtureDef.filter.categoryBits = Mask.CATEGORY_WEAPONS_ENEMIES;
			bodyBox2d.fixtureDef.filter.maskBits = Mask.MASK_WEAPONS_ENEMIES;
		}
		this.setFixtureDef(bodyBox2d.fixtureDef);
		bodyBox2d.body.createFixture(bodyBox2d.fixtureDef);
	}

	/**
	 * Draw the littleshiboleet.
	 * 
	 * @param canvas
	 *            The canvas
	 * @param ls
	 *            the littleshiboleet.
	 */
	private void drawLs(Canvas canvas, LittleShiboleet ls) {
		int radius = ls.getRadius();
		tmp = Bitmap.createScaledBitmap(bitmap, radius, radius, false);

		// Update of the radius.
		bodyBox2d.cShapeDef.m_radius = radius;
		canvas.drawBitmap(tmp, bodyBox2d.body.getPosition().x,
				bodyBox2d.body.getPosition().y, null);
		moveLs(ls);
	}

	/**
	 * Draw the weapon entity.
	 */
	@Override
	public void draw(Canvas canvas) {
		switch (weaponType) {
		case 0:
			bodyBox2d.draw(canvas, bitmap);
			bodyBox2d.move(direction);
			break;
		case 1:
			Fireball f = (Fireball) this.getWeapon();
			int radius = f.getRadius();
			if (radius < 60) {
				tmp = Bitmap.createScaledBitmap(bitmap, radius, radius, false);

				radius += 1;
				f.setRadius(radius);
			}
			// Update of the radius.
			bodyBox2d.cShapeDef.m_radius = radius;
			canvas.drawBitmap(tmp, bodyBox2d.body.getPosition().x,
					(int) bodyBox2d.body.getPosition().y, null);
			bodyBox2d.move(direction);
			break;
		case 2:
			Shiboleet s = (Shiboleet) this.getWeapon();
			int radius1 = s.getRadius();
			if (radius1 < 80) {
				tmp = Bitmap
						.createScaledBitmap(bitmap, radius1, radius1, false);
				radius1 += 1;
				s.setRadius(radius1);
			} else {
				s.setDisplayLittleShibo(true);
				for (WeaponDrawableEntity lsEntity : listOfShibos) {
					LittleShiboleet ls = (LittleShiboleet) lsEntity.getWeapon();
					lsEntity.drawLs(canvas, ls);
				}
			}
			// Update of the radius.
			bodyBox2d.cShapeDef.m_radius = radius1;
			if (!s.isDisplayLittleShibo()) {
				// Remove the previous image and draw the final one above.
				canvas.drawBitmap(tmp, bodyBox2d.body.getPosition().x,
						bodyBox2d.body.getPosition().y, null);
			}

			break;
		// case 3 is used in moveLs
		case 4:
			bodyBox2d.draw(canvas, bitmap);
			bodyBox2d.move(direction);

			break;
		}

	}

	/**
	 * Move the LittleShiboleet.
	 * 
	 * @param ls
	 *            the littleshiboleet.
	 */
	public void moveLs(LittleShiboleet ls) {
		// Nothing to do.
		if (ls.isPlayer() == true) {
			switch (ls.getId()) {
			case 0:
				bodyBox2d.move(new Vec2(50, -50));
				break;
			case 1:
				bodyBox2d.move(new Vec2(-50, -50));
				break;
			case 2:
				bodyBox2d.move(new Vec2(0, -75));
				break;
			case 3:
				bodyBox2d.move(new Vec2(-50, -100));
				break;
			case 4:
				bodyBox2d.move(new Vec2(50, -100));
				break;
			}
		} else {
			switch (ls.getId()) {
			case 0:
				bodyBox2d.move(new Vec2(-50, 50));
				break;
			case 1:
				bodyBox2d.move(new Vec2(50, 50));
				break;
			case 2:
				bodyBox2d.move(new Vec2(0, 75));
				break;
			case 3:
				bodyBox2d.move(new Vec2(50, 100));
				break;
			case 4:
				bodyBox2d.move(new Vec2(-50, 100));
				break;
			}
		}
	}

	/**
	 * Move the weapon entity.
	 */
	@Override
	public void move(Vec2 direction) {
		this.direction.x = direction.x;
		this.direction.y = direction.y;
		Vec2 force = new Vec2(speed * direction.x, speed * direction.y);
		force.normalize();
		bodyBox2d.body.applyForce(force, bodyBox2d.body.getPosition());
		// bodyBox2d.body.setLinearVelocity(force);
	}

	/**
	 * @return the weapon
	 */
	public Object getWeapon() {
		return weapon;
	}

	/**
	 * @param weapon
	 *            the weapon to set
	 */
	public void setWeapon(Object weapon) {
		this.weapon = weapon;
	}

	/**
	 * 
	 * @return
	 */
	public int getWeaponType() {
		return this.weaponType;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isAlive() {
		return this.alive;
	}

	/**
	 * 
	 * @param alive
	 */
	public void setAlive(boolean alive) {
		this.alive = alive;
	}

	/**
	 * @return the fixtureDef
	 */
	public FixtureDef getFixtureDef() {
		return fixtureDef;
	}

	/**
	 * @param fixtureDef
	 *            the fixtureDef to set
	 */
	public void setFixtureDef(FixtureDef fixtureDef) {
		this.fixtureDef = fixtureDef;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

}
