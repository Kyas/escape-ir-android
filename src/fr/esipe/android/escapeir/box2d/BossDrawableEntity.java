package fr.esipe.android.escapeir.box2d;

import java.util.List;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import fr.esipe.android.escapeir.api.Boss;
import fr.esipe.android.escapeir.box2d.collisions.EntityType;
import fr.esipe.android.escapeir.box2d.collisions.Mask;
import fr.esipe.android.escapeir.game.GameActivity;

/**
 * Class BossDrawableEntity - The entity for Boss.
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 */
public class BossDrawableEntity extends DrawableEntity {

	/**
	 * The boss.
	 */
	Boss boss;

	/**
	 * List of Point for the movement.
	 */
	List<Point> listPoint;

	/**
	 * The cursor.
	 */
	int cursor;

	/**
	 * If the boss entity is alive.
	 */
	boolean alive;

	/**
	 * The bitmap.
	 */
	Bitmap bitmap;

	/**
	 * Constructor.
	 * 
	 * @param boss
	 *            The boss.
	 * @param res
	 *            The resources.
	 * @param position
	 *            The position.
	 */
	public BossDrawableEntity(Boss boss, Resources res, Vec2 position) {
		super(BitmapFactory.decodeResource(res, boss.getPath()), position,
				0.5f, 0.3f, 0.3f, 0.3f);
		this.boss = boss;
		this.alive = true;
		this.bitmap = BitmapFactory.decodeResource(res, boss.getPath());
		bodyBox2d.shapeDef = new PolygonShape();
		if (GameActivity.SCREEN_WIDTH >= 720) {
			bodyBox2d.shapeDef.setAsBox(bitmap.getWidth() / 4,
					bitmap.getHeight() / 4);
		} else {
			bodyBox2d.shapeDef.setAsBox(1f, 1f);
		}
		bodyBox2d.body.setUserData(this);
		bodyBox2d.fixtureDef.shape = bodyBox2d.shapeDef;
		bodyBox2d.fixtureDef.userData = EntityType.BOSS;
		bodyBox2d.fixtureDef.filter.categoryBits = Mask.CATEGORY_ENEMIES;
		bodyBox2d.fixtureDef.filter.maskBits = Mask.MASK_ENEMIES;
		bodyBox2d.body.createFixture(bodyBox2d.fixtureDef);
		listPoint = boss.getMovement();
	}

	/**
	 * Draw the boss entity.
	 */
	public void draw(Canvas canvas) {
		bodyBox2d.draw(canvas, bitmap);
		move(direction);
	}

	/**
	 * Move the boss entity.
	 */
	@Override
	public void move(Vec2 vec) {
		bodyBox2d.move(vec);
	}

	/**
	 * If the boss entity is alive.
	 * 
	 * @return if the boss is alive.
	 */
	public boolean isAlive() {
		return alive;
	}

	/**
	 * Set the boss entity alive or not.
	 * 
	 * @param alive
	 *            <code>true</code>if the boss entity is alive,
	 *            <code>otherwise</code>.
	 */
	public void setAlive(boolean alive) {
		this.alive = alive;
	}

	/**
	 * Move the boss entity.
	 */
	public void shift() {
		if (GameActivity.SCREEN_WIDTH >= 720) {
			int newCurseur = 0;
			if (cursor != listPoint.size() - 1) {
				newCurseur = cursor + 1;
			}
			direction = new Vec2(
					(listPoint.get(newCurseur).x - listPoint.get(cursor).x) * 100,
					(listPoint.get(newCurseur).y - listPoint.get(cursor).y) * 100);
			cursor = newCurseur;
		} else {
			int newCurseur = 0;
			if (cursor != listPoint.size() - 1) {
				newCurseur = cursor + 1;
			}
			direction = new Vec2(
					(listPoint.get(newCurseur).x - listPoint.get(cursor).x) * 10,
					(listPoint.get(newCurseur).y - listPoint.get(cursor).y) * 10);
			cursor = newCurseur;
		}
	}
}
