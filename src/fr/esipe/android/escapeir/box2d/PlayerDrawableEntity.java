package fr.esipe.android.escapeir.box2d;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import fr.esipe.android.escapeir.R;
import fr.esipe.android.escapeir.api.Bonus;
import fr.esipe.android.escapeir.api.Player;
import fr.esipe.android.escapeir.api.weapon.Munitions;
import fr.esipe.android.escapeir.box2d.collisions.EntityType;
import fr.esipe.android.escapeir.box2d.collisions.Mask;
import fr.esipe.android.escapeir.game.GameActivity;

/**
 * Class PlayerDrawableEntity - The entity for the Player.
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 */
public class PlayerDrawableEntity extends DrawableEntity {

	/**
	 * The player.
	 */
	Player player;

	/**
	 * If the player is alive.
	 */
	boolean alive;

	/**
	 * Detect the gesture for the player.
	 */
	boolean dectectGesture = false;

	/**
	 * If the player has a shield.
	 */
	boolean hasShield;

	/**
	 * The list of Point.
	 */
	List<Vec2> listPoint = Collections.synchronizedList(new ArrayList<Vec2>());

	/**
	 * Cursor.
	 */
	int cursor = 0;

	/**
	 * The bitmap.
	 */
	Bitmap bitmap;

	/**
	 * The bitmap with the shield.
	 */
	Bitmap bitmapShield;

	/**
	 * Constructor.
	 * 
	 * @param player
	 *            The player.
	 * @param res
	 *            The resources.
	 */
	public PlayerDrawableEntity(Player player, Resources res) {
		super(BitmapFactory.decodeResource(res, player.getPath()), new Vec2(
				(GameActivity.SCREEN_WIDTH) / 2,
				(GameActivity.SCREEN_HEIGHT) * 3 / 4), 0.5f, 0.5f, 0.3f, 0.5f);
		this.player = player;
		this.alive = true;
		this.hasShield = false;
		this.bitmap = BitmapFactory.decodeResource(res, player.getPath());
		this.bitmapShield = BitmapFactory.decodeResource(res,
				R.drawable.player_shield);
		bodyBox2d.shapeDef = new PolygonShape();
		bodyBox2d.shapeDef.setAsBox(bitmap.getWidth() / 2,
				bitmap.getHeight() / 2);
		bodyBox2d.body.setUserData(this);
		bodyBox2d.fixtureDef.shape = bodyBox2d.shapeDef;
		bodyBox2d.fixtureDef.userData = EntityType.PLAYER;
		bodyBox2d.fixtureDef.filter.categoryBits = Mask.CATEGORY_PLAYER;
		bodyBox2d.fixtureDef.filter.maskBits = Mask.MASK_PLAYER;
		bodyBox2d.body.createFixture(bodyBox2d.fixtureDef);
	}

	/**
	 * Draw the player entity.
	 */
	public void draw(Canvas canvas) {
		if (hasShield) {
			bodyBox2d.draw(canvas, bitmapShield);
		} else {
			bodyBox2d.draw(canvas, bitmap);
		}
		move(direction);
	}

	/**
	 * Move the player entity.
	 */
	@Override
	public void move(Vec2 direction) {
		bodyBox2d.move(direction);
	}

	/**
	 * The player take a bonus.
	 * 
	 * @param bonus
	 *            the bonus.
	 */
	public void takeBonus(Bonus bonus) {
		switch (bonus.getId()) {
		// Ammo.
		case 0:
			Munitions missile = WorldBox2D.getWeaponsTable().get("Missile");
			Munitions fireball = WorldBox2D.getWeaponsTable().get("Fireball");
			Munitions shiboleet = WorldBox2D.getWeaponsTable().get("Shiboleet");
			Munitions laser = WorldBox2D.getWeaponsTable().get("Laser");

			int oldMissile = missile.getNbAmmos();
			int newAmmosMissile = 50 + oldMissile;
			int oldFireball = fireball.getNbAmmos();
			int newAmmosFireball = 50 + oldFireball;
			int oldShiboleet = shiboleet.getNbAmmos();
			int newAmmosShiboleet = 20 + oldShiboleet;
			int oldLaser = laser.getNbAmmos();
			int newAmmosLaser = 10 + oldLaser;

			WorldBox2D.getWeaponsTable().put("Missile",
					new Munitions("Missile", newAmmosMissile));
			WorldBox2D.getWeaponsTable().put("Fireball",
					new Munitions("Fireball", newAmmosFireball));
			WorldBox2D.getWeaponsTable().put("Shiboleet",
					new Munitions("Shiboleet", newAmmosShiboleet));
			WorldBox2D.getWeaponsTable().put("Laser",
					new Munitions("Laser", newAmmosLaser));
			break;
		// Health.
		case 1:
			int life = Player.getLife() + 5;
			Player.setLife(life);
			break;
		// Shield.
		case 2:
			this.setShield(true);
			break;
		}
	}

	/**
	 * Set the shield.
	 * 
	 * @param hasShield
	 *            If the player has a shield or not.
	 */
	public void setShield(boolean hasShield) {
		this.hasShield = hasShield;
	}

	/**
	 * If the player has a shield.
	 * 
	 * @return <code>true</code> if the player has a shield, <code>false</code>
	 *         otherwise.
	 */
	public boolean hasShield() {
		return hasShield;
	}

	/**
	 * Get the player.
	 * 
	 * @return the player.
	 */
	public Player getPlayer() {
		return player;
	}

	/**
	 * If the player entity is alive.
	 * 
	 * @return <code>true</code> if the player entity is alive,
	 *         </code>false</code>.
	 */
	public boolean isAlive() {
		return alive;
	}

	/**
	 * Set the player entity alive or not.
	 * 
	 * @param alive
	 *            if the player is alive or not.
	 */
	public void setAlive(boolean alive) {
		this.alive = alive;
	}

	/**
	 * If the player detects a gesture.
	 * 
	 * @return <code>true</code> if the player detects a gesture,
	 *         </code>otherwise</code>.
	 */
	public boolean isDectectGesture() {
		return dectectGesture;
	}

	/**
	 * Set the detection for a gesture.
	 * 
	 * @param dectectGesture
	 *            detecte a gesture.
	 */
	public void setDectectGesture(boolean dectectGesture) {
		this.dectectGesture = dectectGesture;
	}

	/**
	 * Set the list of point.
	 * 
	 * @param point
	 *            the point.
	 */
	public void setList(List<Vec2> point) {
		listPoint = point;
	}

	/**
	 * Move the player entity for movement.
	 */
	public void shift() {
		if (listPoint != null && listPoint.size() > 0) {
			int newCurseur = 0;
			if (cursor != listPoint.size() - 1) {
				newCurseur = cursor + 1;
			}
			direction = new Vec2(
					(listPoint.get(newCurseur).x - listPoint.get(cursor).x) * 100,
					(listPoint.get(newCurseur).y - listPoint.get(cursor).y) * 100);
			cursor = newCurseur;
		}
	}

	/**
	 * Get the bitmap.
	 * 
	 * @return the bitmap.
	 */
	public Bitmap getBitmap() {
		return bitmap;
	}
}
