package fr.esipe.android.escapeir.box2d;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import fr.esipe.android.escapeir.api.Bonus;
import fr.esipe.android.escapeir.box2d.collisions.EntityType;
import fr.esipe.android.escapeir.box2d.collisions.Mask;

/**
 * Class BonusDrawableEntity - The entity for Bonus.
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 */
public class BonusDrawableEntity extends DrawableEntity {

	/**
	 * Bonus.
	 */
	Bonus bonus;

	/**
	 * If the bonus entity is alive.
	 */
	boolean alive;

	/**
	 * The bitmap.
	 */
	Bitmap bitmap;

	/**
	 * The direction.
	 */
	Vec2 direction;

	/**
	 * Construction.
	 * 
	 * @param bonus
	 *            the bonus.
	 * @param res
	 *            the resources.
	 * @param position
	 *            the position.
	 */
	public BonusDrawableEntity(Bonus bonus, Resources res, Vec2 position) {
		super(BitmapFactory.decodeResource(res, bonus.getPath()), position,
				0.5f, 0.3f, 0.3f, 0.3f);
		this.bonus = bonus;
		this.alive = true;
		this.bitmap = BitmapFactory.decodeResource(res, bonus.getPath());
		// Set the vertical direction.
		this.direction = new Vec2(0, 20);
		bodyBox2d.shapeDef = new PolygonShape();
		bodyBox2d.shapeDef.setAsBox(bitmap.getWidth() / 2,
				bitmap.getHeight() / 2);
		bodyBox2d.body.setUserData(this);
		bodyBox2d.fixtureDef.shape = bodyBox2d.shapeDef;
		bodyBox2d.fixtureDef.userData = EntityType.BONUS;
		bodyBox2d.fixtureDef.filter.categoryBits = Mask.CATEGORY_BONUS;
		bodyBox2d.fixtureDef.filter.maskBits = Mask.MASK_BONUS;
		bodyBox2d.body.createFixture(bodyBox2d.fixtureDef);
	}

	/**
	 * Draw the bonus entity.
	 */
	public void draw(Canvas canvas) {
		bodyBox2d.draw(canvas, bitmap);
		move(direction);
	}

	/**
	 * Move the bonus entity.
	 */
	@Override
	public void move(Vec2 direction) {
		bodyBox2d.move(direction);
	}

	/**
	 * If the bonus entity is alive.
	 * 
	 * @return <code>true</code> if the bonus entity is alive,
	 *         <code>false</code>otherwise.
	 */
	public boolean isAlive() {
		return alive;
	}

	/**
	 * Get the bonus.
	 * 
	 * @return the bonus.
	 */
	public Bonus getBonus() {
		return bonus;
	}

	/**
	 * Set the bonus alive.
	 * 
	 * @param alive
	 */
	public void setAlive(boolean alive) {
		this.alive = alive;
	}
}
