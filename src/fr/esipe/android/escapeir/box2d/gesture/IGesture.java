package fr.esipe.android.escapeir.box2d.gesture;

import java.util.List;

import org.jbox2d.common.*;



/**
 * 
 * Interface that allows to identify gesture after a motion event detection.  
 @author C�line P�rillous <cperillous@etudiant.univ-mlv.fr>
 * @author J�r�my Lor <jlor@etudiant.univ-mlv.fr>
 *
 */
public interface IGesture {

	/**
	 * This method should be implemented to test if recording motion events correspond to the behavior 
	 * of gesture
	 *  
	 * @param listVector contains every points where mouse was detected.
	 * @return true if listVector corresponds to the behavior of gesture false otherwise
	 */
	public boolean detectGesture(List<Vec2> listVector);
	

}
