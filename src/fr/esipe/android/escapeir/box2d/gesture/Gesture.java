package fr.esipe.android.escapeir.box2d.gesture;

public abstract class Gesture {

	/**
	 * Listing of gesture
	 * @author C�line P�rillous <cperillous@etudiant.univ-mlv.fr>
	 * @author J�r�my Lor <jlor@etudiant.univ-mlv.fr>
	 *
	 */
	public enum NameGesture {
		ACTIVEARME,MOVE,LEFT_DRIFT,RIGHT_DRIFT,LEFTLOOPING,RIGTH_LOOPING,BACKOFF,UNKWNON
	}
	
	private final NameGesture name;
	
	public Gesture(NameGesture name){
		this.name=name;
		
	}
	


	public NameGesture getName() {
		return name;
	}

	
	
}
