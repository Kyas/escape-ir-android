package fr.esipe.android.escapeir.box2d.gesture;

import java.util.List;

import org.jbox2d.common.Vec2;

import fr.esipe.android.escapeir.box2d.PlayerDrawableEntity;

/**
 * Used for the active weapon.
 * 
 * @author Celine Perillous <cperillous@etudiant.univ-mlv.fr>
 * @author Jeremy Lor <jlor@etudiant.univ-mlv.fr>
 * 
 */
public class WeaponActive {

	public static boolean detectGesture(Vec2 listVector,
			PlayerDrawableEntity player) {
		int width = player.getBitmap().getWidth();
		int height = player.getBitmap().getHeight();

		int xMin = (int) player.getPosition().x;
		int xMax = xMin + width;
		int yMin = (int) player.getPosition().y;
		int yMax = yMin + height;
		Vec2 lVec = listVector;
		if (lVec.x < xMax - width || lVec.x > xMax + width)
			return false;
		if (lVec.y < yMin - height || lVec.y > yMax + height)
			return false;
		return true;
	}

	public static Vec2 calculPath(List<Vec2> listVector,
			PlayerDrawableEntity player) {
		float firstX = listVector.get(0).x;
		float firstY = listVector.get(0).y;
		float lastX = listVector.get(listVector.size() - 1).x;
		float lastY = listVector.get(listVector.size() - 1).y;
		float forceX, forceY;
		if (firstX < lastX)
			forceX = 1 * ((lastX - firstX) * 60 / 100);
		else
			forceX = -1 * ((firstX - lastX) * 60 / 100);
		if (firstY < lastY)
			forceY = 1 * ((lastY - firstY) * 60 / 100);
		else
			forceY = -1 * ((firstY - lastY) * 60 / 100);
		return new Vec2(forceX, forceY);
	}

}
