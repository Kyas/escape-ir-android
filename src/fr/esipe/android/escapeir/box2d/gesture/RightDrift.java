package fr.esipe.android.escapeir.box2d.gesture;

import java.util.List;

import org.jbox2d.common.Vec2;

import android.util.Log;

import fr.esipe.android.escapeir.game.GameActivity;




/**
 * This class implements Gesture implementation. It allows to detect RightDrift Gesture.
 * RightDrigt corresponds to a line from the bottom to the high in diagonale to the right
 * 
 * @author C�line P�rillous <cperillous@etudiant.univ-mlv.fr>
 * @author J�r�my Lor <jlor@etudiant.univ-mlv.fr>
 *
 */
public class RightDrift extends Gesture implements IGesture {

	public RightDrift() {
		super(NameGesture.RIGHT_DRIFT);	}

	@Override
	public boolean detectGesture(List<Vec2> listVector) {
		
		
		if(listVector.size()<10) return false;
		
		Vec2 first=listVector.get(0);
		Vec2 last=listVector.get(listVector.size()-1);
				
		if((last.x-first.x)<(3*GameActivity.SCREEN_HEIGHT/100)) return false;
	
		if(first.x>last.x||first.y<last.y) return false;
		
		
		Vec2 lX=new Vec2(first.x-(2*GameActivity.SCREEN_HEIGHT/100),first.y);
		if(!lX.isValid()){
			lX=new Vec2(0,first.y);
		}
		
		
		Vec2 hY=new Vec2(last.x,last.y-(2*GameActivity.SCREEN_WIDTH/100));
		if(!hY.isValid()){
			hY=new Vec2(last.x,0);
		}
	
		Vec2 rX=new Vec2(last.x+(2*GameActivity.SCREEN_HEIGHT/100),last.y);
		if(!rX.isValid()){
			rX=new Vec2(GameActivity.SCREEN_HEIGHT,last.y);
		}
		
		Vec2 bY=new Vec2(first.x,first.y+(2*GameActivity.SCREEN_WIDTH/100));
		if(!bY.isValid()){
			bY=new Vec2(first.x,GameActivity.SCREEN_WIDTH);
		}
		
		for(Vec2 vec : listVector){
			if(vec.y<hY.y||vec.y>bY.y||vec.x<lX.x||vec.x>rX.x) return false;
		}
		
		Log.d("ListApresDetectionAvantmisenMarche", listVector.toString());
		return true;
		
	}



}
