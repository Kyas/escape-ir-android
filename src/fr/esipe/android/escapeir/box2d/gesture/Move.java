package fr.esipe.android.escapeir.box2d.gesture;

import java.util.List;

import org.jbox2d.common.Vec2;


import fr.esipe.android.escapeir.game.GameActivity;




/**
 * This class implements Gesture implementation. It allows to detect Move Gesture.
 * Move corresponds to a line from the bottom to the high.
 * 
 * @author C�line P�rillous <cperillous@etudiant.univ-mlv.fr>
 * @author J�r�my Lor <jlor@etudiant.univ-mlv.fr>
 *
 */
public class Move extends Gesture implements IGesture {


	public Move() {
		super(NameGesture.MOVE);
	}

	@Override
	public boolean detectGesture(List<Vec2> listVector) {

		if(listVector.isEmpty()) return false;

		if(listVector.size()<10) return false;
		
		
		
		Vec2 first=listVector.get(0);
		Vec2 last=listVector.get(listVector.size()-1);
		if(Math.abs(first.x-last.x)>(3*GameActivity.SCREEN_HEIGHT/100)||last.y>first.y) return false;
		float minX=last.x,maxX=first.x;
		if(first.x<last.x){
			minX=first.x;
			maxX=last.x;
		}
		
				
		for(Vec2 vec : listVector){
			if(vec.x<(minX-(2*GameActivity.SCREEN_HEIGHT/100))) return false;
			if(vec.x>(maxX+(2*GameActivity.SCREEN_HEIGHT/100))) return false;
			if((vec.y-last.y)<-10||vec.y-first.y>10) return false;
		}
		return true;
	}


}
