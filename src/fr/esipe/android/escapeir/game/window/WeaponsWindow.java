package fr.esipe.android.escapeir.game.window;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import fr.esipe.android.escapeir.R;
import fr.esipe.android.escapeir.api.Player;
import fr.esipe.android.escapeir.box2d.WorldBox2D;
import fr.esipe.android.escapeir.game.GameActivity;

/**
 * WeaponsWindow - Displays the weapons of the game.
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 */
public class WeaponsWindow {

	/**
	 * The height of the weapon bar.
	 */
	private static int HEIGHT_LARGE = 200;
	private static int HEIGHT_SMALL = 50;

	private static Bitmap weaponMissile;
	private static Bitmap weaponFireball;
	private static Bitmap weaponShiboleet;
	private static Bitmap weaponLaser;

	public static void create(Context context) {
		weaponMissile = BitmapFactory.decodeResource(context.getResources(),
				R.drawable.weapon_activity_missile);
		weaponFireball = BitmapFactory.decodeResource(context.getResources(),
				R.drawable.weapon_activity_fireball);
		weaponShiboleet = BitmapFactory.decodeResource(context.getResources(),
				R.drawable.weapon_activity_shiboleet);
		weaponLaser = BitmapFactory.decodeResource(context.getResources(),
				R.drawable.weapon_activity_laser);
	}

	public static void draw(Player player, Canvas canvas, int weaponActive) {

		int missileAmmo = WorldBox2D.getWeaponsTable().get("Missile")
				.getNbAmmos();
		int fireballAmmo = WorldBox2D.getWeaponsTable().get("Fireball")
				.getNbAmmos();
		int shiboleetAmmo = WorldBox2D.getWeaponsTable().get("Shiboleet")
				.getNbAmmos();
		int laserAmmo = WorldBox2D.getWeaponsTable().get("Laser").getNbAmmos();

		switch (weaponActive) {
		case 0:
			if (GameActivity.SCREEN_WIDTH >= 720) {
				canvas.drawBitmap(weaponMissile, canvas.getWidth() - 60,
						canvas.getHeight() / 2 - HEIGHT_LARGE, null);
			} else {
				canvas.drawBitmap(weaponMissile, canvas.getWidth()
						- (weaponMissile.getWidth()), canvas.getHeight() / 2
						- HEIGHT_SMALL, null);
			}
			break;
		case 1:
			if (GameActivity.SCREEN_WIDTH >= 720) {
				canvas.drawBitmap(weaponFireball, canvas.getWidth() - 60,
						canvas.getHeight() / 2 - HEIGHT_LARGE, null);
			} else {
				canvas.drawBitmap(weaponFireball, canvas.getWidth()
						- (weaponFireball.getWidth()), canvas.getHeight() / 2
						- HEIGHT_SMALL, null);
			}
			break;
		case 2:
			if (GameActivity.SCREEN_WIDTH >= 720) {
				canvas.drawBitmap(weaponShiboleet, canvas.getWidth() - 60,
						canvas.getHeight() / 2 - HEIGHT_LARGE, null);
			} else {
				canvas.drawBitmap(weaponShiboleet, canvas.getWidth()
						- (weaponShiboleet.getWidth()), canvas.getHeight() / 2
						- HEIGHT_SMALL, null);
			}
			break;
		case 3:
			if (GameActivity.SCREEN_WIDTH >= 720) {
				canvas.drawBitmap(weaponLaser, canvas.getWidth() - 60,
						canvas.getHeight() / 2 - HEIGHT_LARGE, null);
			} else {
				canvas.drawBitmap(weaponLaser,
						canvas.getWidth() - (weaponLaser.getWidth()),
						canvas.getHeight() / 2 - HEIGHT_SMALL, null);
			}
			break;
		default:
			if (GameActivity.SCREEN_WIDTH >= 720) {
				canvas.drawBitmap(weaponMissile, canvas.getWidth() - 60,
						canvas.getHeight() / 2 - HEIGHT_LARGE, null);
			} else {
				canvas.drawBitmap(weaponMissile, canvas.getWidth()
						- (weaponMissile.getWidth()), canvas.getHeight() / 2
						- HEIGHT_SMALL, null);
			}
		}

		Paint textPaint = new Paint();
		textPaint.setARGB(255, 255, 255, 255);
		textPaint.setTextAlign(Paint.Align.CENTER);

		if (GameActivity.SCREEN_WIDTH >= 720) {
			textPaint.setTextSize(20);
		} else {
			textPaint.setTextSize(10);
		}
		textPaint.setTypeface(Typeface.DEFAULT_BOLD);
		if (GameActivity.SCREEN_WIDTH >= 720) {
			textPaint.setColor(Color.BLACK);
		} else {
			textPaint.setColor(Color.CYAN);
		}

		/**
		 * Ammos for missile.
		 */

		if (missileAmmo > 0) {
			if (GameActivity.SCREEN_WIDTH >= 720) {
				canvas.drawText(String.valueOf(missileAmmo),
						canvas.getWidth() - 30, canvas.getHeight() / 2 - 135,
						textPaint);
			} else {
				canvas.drawText(String.valueOf(missileAmmo),
						canvas.getWidth() - 30, canvas.getHeight() / 2 - 35,
						textPaint);
			}
		} else {
			if (GameActivity.SCREEN_WIDTH >= 720) {
				canvas.drawText("0", canvas.getWidth() - 25,
						canvas.getHeight() / 2 - 135, textPaint);
			} else {
				canvas.drawText("0", canvas.getWidth() - 25,
						canvas.getHeight() / 2 - 35, textPaint);
			}
		}

		/**
		 * Ammos for fireball.
		 */

		if (fireballAmmo > 0) {
			if (GameActivity.SCREEN_WIDTH >= 720) {
				canvas.drawText(String.valueOf(fireballAmmo),
						canvas.getWidth() - 30, canvas.getHeight() / 2 - 75,
						textPaint);
			} else {
				canvas.drawText(String.valueOf(fireballAmmo),
						canvas.getWidth() - 30, canvas.getHeight() / 2 - 15,
						textPaint);
			}
		} else {
			if (GameActivity.SCREEN_WIDTH >= 720) {
				canvas.drawText("0", canvas.getWidth() - 25,
						canvas.getHeight() / 2 - 75, textPaint);
			} else {
				canvas.drawText("0", canvas.getWidth() - 25,
						canvas.getHeight() / 2 - 15, textPaint);
			}
		}

		/**
		 * Ammos for shiboleet.
		 */

		if (shiboleetAmmo > 0) {
			if (GameActivity.SCREEN_WIDTH >= 720) {
				canvas.drawText(String.valueOf(shiboleetAmmo),
						canvas.getWidth() - 30, canvas.getHeight() / 2 - 10,
						textPaint);
			} else {
				canvas.drawText(String.valueOf(shiboleetAmmo),
						canvas.getWidth() - 30, canvas.getHeight() / 2,
						textPaint);
			}
		} else {
			if (GameActivity.SCREEN_WIDTH >= 720) {
				canvas.drawText("0", canvas.getWidth() - 25,
						canvas.getHeight() / 2 - 10, textPaint);
			} else {
				canvas.drawText("0", canvas.getWidth() - 25,
						canvas.getHeight() / 2, textPaint);
			}
		}

		/**
		 * Ammos for laser.
		 */
		if (laserAmmo > 0) {
			if (GameActivity.SCREEN_WIDTH >= 720) {
				canvas.drawText(String.valueOf(laserAmmo),
						canvas.getWidth() - 30, canvas.getHeight() / 2 + 55,
						textPaint);
			} else {
				canvas.drawText(String.valueOf(laserAmmo),
						canvas.getWidth() - 30, canvas.getHeight() / 2 + 20,
						textPaint);
			}
		} else {
			if (GameActivity.SCREEN_WIDTH >= 720) {
				canvas.drawText("0", canvas.getWidth() - 25,
						canvas.getHeight() / 2 + 55, textPaint);
			} else {
				canvas.drawText("0", canvas.getWidth() - 25,
						canvas.getHeight() / 2 + 20, textPaint);
			}
		}

	}
}
