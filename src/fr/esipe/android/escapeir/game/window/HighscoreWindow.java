package fr.esipe.android.escapeir.game.window;

import fr.esipe.android.escapeir.R;
import fr.esipe.android.escapeir.box2d.WorldBox2D;
import fr.esipe.android.escapeir.game.GameActivity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;

/**
 * HighscoreWindow - Displays the Highscore of the player.
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 */
public class HighscoreWindow {

	private static Bitmap window;

	public static void create(Context context) {
		window = BitmapFactory.decodeResource(context.getResources(),
				R.drawable.highscore);
	}

	/**
	 * Draw the window.
	 * 
	 * @param canvas
	 *            The canvas.
	 */
	public static void draw(Canvas canvas) {

		canvas.drawBitmap(window, 0, 0, null);

		Paint textPaint = new Paint();
		textPaint.setARGB(255, 255, 255, 255);
		textPaint.setTextAlign(Paint.Align.RIGHT);

		if (GameActivity.SCREEN_WIDTH >= 720) {
			textPaint.setTextSize(20);
			textPaint.setTypeface(Typeface.DEFAULT_BOLD);
			textPaint.setColor(Color.WHITE);

			canvas.drawText(String.valueOf(WorldBox2D.getHighscore()),
					canvas.getWidth() / 4 + 40, 37, textPaint);
		} else {
			textPaint.setTextSize(8);
			textPaint.setTypeface(Typeface.DEFAULT_BOLD);
			textPaint.setColor(Color.WHITE);

			canvas.drawText(String.valueOf(WorldBox2D.getHighscore()),
					canvas.getWidth() / 4 + 30, 15, textPaint);
		}

	}
}
