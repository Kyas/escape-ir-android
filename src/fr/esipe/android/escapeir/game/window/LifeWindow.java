package fr.esipe.android.escapeir.game.window;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import fr.esipe.android.escapeir.R;
import fr.esipe.android.escapeir.api.Player;
import fr.esipe.android.escapeir.game.GameActivity;

/**
 * LifeWindow - Displays the life of the player.
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 */
public class LifeWindow {

	/**
	 * The height of the weapon bar.
	 */
	private static int HEIGHT_LARGE = 200;
	private static int HEIGHT_SMALL = 50;

	private static Bitmap life0;
	private static Bitmap life1;
	private static Bitmap life2;
	private static Bitmap life3;
	private static Bitmap life4;
	private static Bitmap life5;
	private static Bitmap life6;
	private static Bitmap life7;
	private static Bitmap life8;
	private static Bitmap life9;
	private static Bitmap life10;

	public static void create(Context context) {

		life10 = BitmapFactory.decodeResource(context.getResources(),
				R.drawable.life_activity10);
		life9 = BitmapFactory.decodeResource(context.getResources(),
				R.drawable.life_activity9);
		life8 = BitmapFactory.decodeResource(context.getResources(),
				R.drawable.life_activity8);
		life7 = BitmapFactory.decodeResource(context.getResources(),
				R.drawable.life_activity7);
		life6 = BitmapFactory.decodeResource(context.getResources(),
				R.drawable.life_activity6);
		life5 = BitmapFactory.decodeResource(context.getResources(),
				R.drawable.life_activity5);
		life4 = BitmapFactory.decodeResource(context.getResources(),
				R.drawable.life_activity4);
		life3 = BitmapFactory.decodeResource(context.getResources(),
				R.drawable.life_activity3);
		life2 = BitmapFactory.decodeResource(context.getResources(),
				R.drawable.life_activity2);
		life1 = BitmapFactory.decodeResource(context.getResources(),
				R.drawable.life_activity1);
		life0 = BitmapFactory.decodeResource(context.getResources(),
				R.drawable.life_activity0);
	}

	/**
	 * Display the LifeWindow. It show different images of life depending to the
	 * player's life in the game.
	 * 
	 * @param canvas
	 *            graphics from Zen2.
	 */
	public static void draw(Canvas canvas) {

		int life = Player.getLife();

		if (life > 10) {
			if (GameActivity.SCREEN_WIDTH >= 720) {
				canvas.drawBitmap(life10, 0, canvas.getHeight() / 2
						- HEIGHT_LARGE, null);
			} else {
				canvas.drawBitmap(life10, 0, canvas.getHeight() / 2
						- HEIGHT_SMALL, null);
			}
		} else {
			switch (life) {
			case 10:
				if (GameActivity.SCREEN_WIDTH >= 720) {
					canvas.drawBitmap(life10, 0, canvas.getHeight() / 2
							- HEIGHT_LARGE, null);
				} else {
					canvas.drawBitmap(life10, 0, canvas.getHeight() / 2
							- HEIGHT_SMALL, null);
				}
				break;
			case 9:
				if (GameActivity.SCREEN_WIDTH >= 720) {
					canvas.drawBitmap(life9, 0, canvas.getHeight() / 2
							- HEIGHT_LARGE, null);
				} else {
					canvas.drawBitmap(life9, 0, canvas.getHeight() / 2
							- HEIGHT_SMALL, null);
				}
				break;
			case 8:
				if (GameActivity.SCREEN_WIDTH >= 720) {
					canvas.drawBitmap(life8, 0, canvas.getHeight() / 2
							- HEIGHT_LARGE, null);
				} else {
					canvas.drawBitmap(life8, 0, canvas.getHeight() / 2
							- HEIGHT_SMALL, null);
				}
				break;
			case 7:
				if (GameActivity.SCREEN_WIDTH >= 720) {
					canvas.drawBitmap(life7, 0, canvas.getHeight() / 2
							- HEIGHT_LARGE, null);
				} else {
					canvas.drawBitmap(life7, 0, canvas.getHeight() / 2
							- HEIGHT_SMALL, null);
				}
				break;
			case 6:
				if (GameActivity.SCREEN_WIDTH >= 720) {
					canvas.drawBitmap(life6, 0, canvas.getHeight() / 2
							- HEIGHT_LARGE, null);
				} else {
					canvas.drawBitmap(life6, 0, canvas.getHeight() / 2
							- HEIGHT_SMALL, null);
				}
				break;
			case 5:
				if (GameActivity.SCREEN_WIDTH >= 720) {
					canvas.drawBitmap(life5, 0, canvas.getHeight() / 2
							- HEIGHT_LARGE, null);
				} else {
					canvas.drawBitmap(life5, 0, canvas.getHeight() / 2
							- HEIGHT_SMALL, null);
				}
				break;
			case 4:
				if (GameActivity.SCREEN_WIDTH >= 720) {
					canvas.drawBitmap(life4, 0, canvas.getHeight() / 2
							- HEIGHT_LARGE, null);
				} else {
					canvas.drawBitmap(life4, 0, canvas.getHeight() / 2
							- HEIGHT_SMALL, null);
				}
				break;
			case 3:
				if (GameActivity.SCREEN_WIDTH >= 720) {
					canvas.drawBitmap(life3, 0, canvas.getHeight() / 2
							- HEIGHT_LARGE, null);
				} else {
					canvas.drawBitmap(life3, 0, canvas.getHeight() / 2
							- HEIGHT_SMALL, null);
				}
				break;
			case 2:
				if (GameActivity.SCREEN_WIDTH >= 720) {
					canvas.drawBitmap(life2, 0, canvas.getHeight() / 2
							- HEIGHT_LARGE, null);
				} else {
					canvas.drawBitmap(life2, 0, canvas.getHeight() / 2
							- HEIGHT_SMALL, null);
				}
				break;
			case 1:
				if (GameActivity.SCREEN_WIDTH >= 720) {
					canvas.drawBitmap(life1, 0, canvas.getHeight() / 2
							- HEIGHT_LARGE, null);
				} else {
					canvas.drawBitmap(life1, 0, canvas.getHeight() / 2
							- HEIGHT_SMALL, null);
				}
				break;
			case 0:
				if (GameActivity.SCREEN_WIDTH >= 720) {
					canvas.drawBitmap(life0, 0, canvas.getHeight() / 2
							- HEIGHT_LARGE, null);
				} else {
					canvas.drawBitmap(life0, 0, canvas.getHeight() / 2
							- HEIGHT_SMALL, null);
				}
				break;
			default:
				if (GameActivity.SCREEN_WIDTH >= 720) {
					canvas.drawBitmap(life0, 0, canvas.getHeight() / 2
							- HEIGHT_LARGE, null);
				} else {
					canvas.drawBitmap(life0, 0, canvas.getHeight() / 2
							- HEIGHT_SMALL, null);
				}
			}
		}

	}
}
