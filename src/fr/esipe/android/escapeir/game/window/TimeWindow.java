package fr.esipe.android.escapeir.game.window;

import java.util.Timer;
import java.util.TimerTask;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import fr.esipe.android.escapeir.R;
import fr.esipe.android.escapeir.builder.form.activity.CurrentLevel;
import fr.esipe.android.escapeir.game.GameActivity;

/**
 * FPSWindow - Displays the time of the game.
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 */

public class TimeWindow {

	private static Bitmap window;
	private static int time = 0;
	private static String TEXT = "";
	private static boolean finished;

	public static void create(Context context) {
		window = BitmapFactory.decodeResource(context.getResources(),
				R.drawable.time);
	}

	public static void init() {
		// Declare the timer
		Timer t = new Timer();
		t.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				// Called each time when 1000 milliseconds (1 second) (the
				// period parameter)
				time++;
				int seconds = (int) (time / 1000);
				int minutes = seconds / 60;
				seconds = seconds % 60;
				if (seconds == CurrentLevel.getCurrentLevel().getLevel()
						.getScenario().getTime()) {
					finished = true;
				}
				
				if (finished) {
					TEXT = "BOSS";
				} else {
					TEXT = String.format("%d:%02d", minutes, seconds);
				}

			}

		}, 0, 1);
	}

	/**
	 * Draw the window.
	 * 
	 * @param canvas
	 *            The canvas.
	 */
	public static void draw(Canvas canvas) throws Exception {
		float canvasWidth = canvas.getWidth();
		canvas.drawBitmap(window,
				canvasWidth - (GameActivity.SCREEN_WIDTH / 4), 0, null);

		Paint textPaint = new Paint();
		textPaint.setARGB(255, 255, 255, 255);
		textPaint.setTextAlign(Paint.Align.LEFT);

		if (GameActivity.SCREEN_WIDTH >= 720) {
			textPaint.setTextSize(20);
			textPaint.setTypeface(Typeface.DEFAULT_BOLD);
			if (finished) {
				textPaint.setColor(Color.RED);
			} else {
				textPaint.setColor(Color.WHITE);
			}

			canvas.drawText(TEXT,
					canvasWidth - (GameActivity.SCREEN_WIDTH / 4) + 20, 37,
					textPaint);
		} else {
			textPaint.setTextSize(8);
			textPaint.setTypeface(Typeface.DEFAULT_BOLD);
			if (finished) {
				textPaint.setColor(Color.RED);
			} else {
				textPaint.setColor(Color.WHITE);
			}

			canvas.drawText(TEXT, canvasWidth - (GameActivity.SCREEN_WIDTH / 4)
					+ 5, 15, textPaint);
		}
	}

	public static int getCurrentTime() {
		return time;
	}
}