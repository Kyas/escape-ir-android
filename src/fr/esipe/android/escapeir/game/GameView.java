package fr.esipe.android.escapeir.game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.jbox2d.common.Vec2;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import fr.esipe.android.escapeir.R;
import fr.esipe.android.escapeir.R.drawable;
import fr.esipe.android.escapeir.api.Boss;
import fr.esipe.android.escapeir.api.Enemy;
import fr.esipe.android.escapeir.api.Level;
import fr.esipe.android.escapeir.api.Map;
import fr.esipe.android.escapeir.api.Player;
import fr.esipe.android.escapeir.api.Scenario;
import fr.esipe.android.escapeir.api.Scenario.TimeEnemy;
import fr.esipe.android.escapeir.api.StartPositionEnum;
import fr.esipe.android.escapeir.api.weapon.Fireball;
import fr.esipe.android.escapeir.api.weapon.Laser;
import fr.esipe.android.escapeir.api.weapon.Missile;
import fr.esipe.android.escapeir.api.weapon.Shiboleet;
import fr.esipe.android.escapeir.box2d.BonusDrawableEntity;
import fr.esipe.android.escapeir.box2d.BossDrawableEntity;
import fr.esipe.android.escapeir.box2d.EnemyDrawableEntity;
import fr.esipe.android.escapeir.box2d.PlayerDrawableEntity;
import fr.esipe.android.escapeir.box2d.WeaponDrawableEntity;
import fr.esipe.android.escapeir.box2d.WorldBox2D;
import fr.esipe.android.escapeir.box2d.gesture.FactoryGesture;
import fr.esipe.android.escapeir.box2d.gesture.WeaponActive;
import fr.esipe.android.escapeir.builder.form.activity.CurrentLevel;
import fr.esipe.android.escapeir.game.window.HighscoreWindow;
import fr.esipe.android.escapeir.game.window.LifeWindow;
import fr.esipe.android.escapeir.game.window.TimeWindow;
import fr.esipe.android.escapeir.game.window.WeaponsWindow;
import fr.esipe.android.escapeir.menu.GameOverActivity;
import fr.esipe.android.escapeir.menu.VictoryActivity;
import fr.esipe.android.util.ScrollMap;

public class GameView extends SurfaceView implements SurfaceHolder.Callback {

	private Map background;
	private PlayerDrawableEntity playerDrawableEntity;
	private BossDrawableEntity bossDrawableEntity;
	final List<Vec2> list = Collections.synchronizedList(new ArrayList<Vec2>());
	final Level level = (CurrentLevel.getCurrentLevel()).getLevel();
	private Player player;
	private Boss boss;
	private Scenario scenario;
	private ArrayList<TimeEnemy> arrayList;
	final Paint paint = new Paint();

	boolean armed;
	boolean finalGesture;
	List<Vec2> listShoot = Collections.synchronizedList(new ArrayList<Vec2>());

	boolean lose;
	boolean win;

	// To determine is the game is stopped / paused. We restart the app.
	static boolean isGameViewStopped = false;

	// Set vertical speed for each layer.
	float vy = -0.4f;
	// Set y position for each layer.
	float y = 0f;
	// Gap for each layer.
	float gap = 0f;
	private static GameMapThread thread;
	// Create a parallax engine.
	ScrollMap scrollMap;

	private static int countPaint = 0;
	private static long millis = 0;
	private static int fps = -1;

	long nextFrameStart = System.nanoTime();
	long FRAME_PERIOD = 1000000000L / 60;

	int currentX, currentY;

	public GameView(Context context) throws Exception {
		super(context);
		WorldBox2D.createWorldBox(level);
		Random r = new Random();
		int a = Math.abs(r.nextInt() % 3);
		Log.d("a", a + "");
		if (a == 1) {
			background = new Map(context, "earth", R.drawable.earth);
		} else if (a == 2) {
			background = new Map(context, "moon", R.drawable.moon);
		} else {
			background = new Map(context, "jupiter", R.drawable.jupiter);
		}
		player = level.getPlayer();
		scrollMap = new ScrollMap(2400, (1 * 0.75f), background, vy, y, gap);

		playerDrawableEntity = new PlayerDrawableEntity(player, getResources());
		WorldBox2D.setPlayer(playerDrawableEntity);

		scenario = level.getScenario();
		boss = level.getBoss();
		arrayList = scenario.getPlay();

		this.win = false;
		this.lose = false;

		// adding the callback (this) to the surface holder to intercept events
		// This line sets the current class (MainGamePanel) as the handler for
		// the events happening on the actual surface
		getHolder().addCallback(this);
		// create the game loop thread
		thread = new GameMapThread(this.getContext(), getHolder(), scrollMap);
		// make the GamePanel focusable so it can handle events.
		setFocusable(true);
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {

	}

	public void surfaceCreated(SurfaceHolder holder) {
		if (thread.getState() == Thread.State.TERMINATED) {
			try {
				thread = new GameMapThread(this.getContext(), holder, scrollMap);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		thread.setRunning(true);
		thread.start();
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		// tell the thread to shut down and wait for it to finish
		// this is a clean shutdown
		boolean retry = true;
		thread.setRunning(false);
		while (retry) {
			try {
				thread.join();
				retry = false;
			} catch (InterruptedException e) {
				// try again shutting down the thread
			}
		}

	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		super.onTouchEvent(event);
		int action = event.getAction() & MotionEvent.ACTION_MASK;
		Log.d("Event", event.getX() + " " + event.getY());
		switch (action) {
		case MotionEvent.ACTION_DOWN: {
			finalGesture = false;
			event.getPointerId(event.getActionIndex());
			playerDrawableEntity.setDectectGesture(false);
			synchronized (list) {
				list.clear();
			}
			// event.getPointerId(event.getActionIndex());
			if (GameActivity.SCREEN_WIDTH >= 720) {
				int mouseX = (int) event.getX(), mouseY = (int) event.getY();
				if (mouseX > GameActivity.SCREEN_WIDTH - 100
						&& mouseX < GameActivity.SCREEN_WIDTH
						&& mouseY > GameActivity.SCREEN_HEIGHT / 2 - 200
						&& mouseY < GameActivity.SCREEN_HEIGHT / 2 + 70) {
					// Appel d'une methode dans MouseFactory.
					WorldBox2D.setWeaponActive(MouseFactory.eventOnWeapon(
							this.getContext(), event));
				}
			} else {
				int mouseX = (int) event.getX(), mouseY = (int) event.getY();
				if (mouseX > GameActivity.SCREEN_WIDTH - 50
						&& mouseX < GameActivity.SCREEN_WIDTH
						&& mouseY > GameActivity.SCREEN_HEIGHT / 2 - 40
						&& mouseY < GameActivity.SCREEN_HEIGHT / 2 + 30) {
					// Appel d'une methode dans MouseFactory.
					WorldBox2D.setWeaponActive(MouseFactory.eventOnWeapon(
							this.getContext(), event));
				}
			}
			break;
		}
		case MotionEvent.ACTION_MOVE: {
			list.add(new Vec2(event.getX(), event.getY()));
			Log.d("List", list.toString());
			if (FactoryGesture.GestureFactory(list) != null) {
				paint.setColor(Color.GREEN);
			} else {
				paint.setColor(Color.RED);
			}

			listShoot.add(new Vec2(event.getX(), event.getY()));
			break;
		}
		case MotionEvent.ACTION_UP: {
			synchronized (list) {
				if (FactoryGesture.GestureFactory(list) != null) {
					playerDrawableEntity.setList(list);
					playerDrawableEntity.setDectectGesture(true);
				} else {
					if (GameActivity.SCREEN_WIDTH >= 720) {
						Vec2 vec = playerDrawableEntity.getPosition();
						float a = event.getX() - vec.x;
						float b = event.getY() - vec.y;
						Vec2 direction;
						if (a > b) {
							direction = new Vec2((a / Math.abs(a)) * 100,
									(b / Math.abs(a)) * 100);
						} else {
							direction = new Vec2((a / Math.abs(b)) * 100,
									(b / Math.abs(b)) * 100);
						}
						playerDrawableEntity.setDirection(direction);
					} else {
						Vec2 vec = playerDrawableEntity.getPosition();
						float a = event.getX() - vec.x;
						float b = event.getY() - vec.y;
						Vec2 direction;
						if (a > b) {
							direction = new Vec2((a / Math.abs(a)) * 20,
									(b / Math.abs(a)) * 20);
						} else {
							direction = new Vec2((a / Math.abs(b)) * 20,
									(b / Math.abs(b)) * 20);
						}
						playerDrawableEntity.setDirection(direction);
					}
				}
				finalGesture = true;
			}
			Log.d("List", list.toString());
			break;
		}
		}
		return true;
	}

	class GameMapThread extends Thread {

		private SurfaceHolder surfaceHolder;
		private ScrollMap scrollMap;
		private Context context;
		private int compteur;

		// flag to hold game state
		private boolean running = true;

		public GameMapThread(Context context, SurfaceHolder surfaceHolder,
				ScrollMap scrollMap) throws Exception {
			super();
			this.surfaceHolder = surfaceHolder;
			this.scrollMap = scrollMap;
			this.context = context;

			HighscoreWindow.create(context);
			TimeWindow.create(context);
			LifeWindow.create(context);
			WeaponsWindow.create(context);
			TimeWindow.init();
		}

		public void setRunning(boolean running) {
			this.running = running;
		}

		@Override
		public void run() {
			Canvas canvas;
			while (!WorldBox2D.isLose() && !WorldBox2D.isWin() && running) {

				WorldBox2D.updateMap(context);
				updateEnemisDrawableEntity();

				canvas = null;
				// try locking the canvas for exclusive pixel editing on the
				// surface
				scrollMap.setMoveTop();
				scrollMap.move();

				// Tell the engine to move layers to the top.
				// Actually performs the movement.

				try {
					canvas = surfaceHolder.lockCanvas();
					synchronized (surfaceHolder) {
						// update game state
						// render state to the screen
						// draws the canvas on the panel
						try {
							scrollMap.render(canvas);

							playerDrawableEntity.draw(canvas);
							if (playerDrawableEntity.isDectectGesture()) {
								playerDrawableEntity.shift();
							}

							if (TimeWindow.getCurrentTime() / 1000 > scenario
									.getTime()) {
								bossDrawableEntity.draw(canvas);
								bossDrawableEntity.shift();
							}

							for (WeaponDrawableEntity w : WorldBox2D
									.getWeaponsList()) {
								w.draw(canvas);
							}

							for (EnemyDrawableEntity e : WorldBox2D
									.getEnemyList()) {
								e.draw(canvas);
								e.shift();
							}

							for (BonusDrawableEntity b : WorldBox2D
									.getBonusList()) {
								b.draw(canvas);
							}

							synchronized (list) {

								for (int i = 0; i < list.size(); i++) {
									canvas.drawCircle(list.get(i).x,
											list.get(i).y, 3, paint);
								}
							}

							synchronized (listShoot) {

								if (!listShoot.isEmpty()
										&& finalGesture == true) {
									armed = WeaponActive.detectGesture(
											listShoot.get(0),
											playerDrawableEntity);
									if (armed == true) {
										Vec2 path = WeaponActive
												.calculPath(listShoot,
														playerDrawableEntity);
										if (path.x != 0 && path.y != 0) {
											WorldBox2D.shootWeaponFromPlayer(
													WorldBox2D
															.getWeaponActive(),
													path, playerDrawableEntity,
													getResources());
											armed = false;
										}
									}
									listShoot.clear();
									finalGesture = false;
								}
							}

							long currentMillis = System.currentTimeMillis();
							if (countPaint == 0) {
								millis = System.currentTimeMillis();
								countPaint = 1;
							} else if (System.currentTimeMillis() - millis < 1000) {
								countPaint++;
							} else {
								estimateFPS(currentMillis);
							}

							TimeWindow.draw(canvas);
							LifeWindow.draw(canvas);
							HighscoreWindow.draw(canvas);
							WeaponsWindow.draw(player, canvas,
									WorldBox2D.getWeaponActive());
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				} finally {
					// in case of an exception the surface is not left in
					// an inconsistent state
					if (canvas != null) {
						surfaceHolder.unlockCanvasAndPost(canvas);
					}
				} // end finally
			}

			if (WorldBox2D.isWin()) {
				this.running = false;
				Intent intent = new Intent(getContext(), VictoryActivity.class)
						.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
								| Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(intent);
			}

			if (WorldBox2D.isLose()) {
				this.running = false;
				Intent intent = new Intent(getContext(), GameOverActivity.class)
						.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
								| Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(intent);
			}
		}

		private void updateEnemisDrawableEntity() {
			for (int i = 0; i < arrayList.size(); i++) {
				if ((arrayList.get(i).getTime() * 1000) >= compteur
						&& (arrayList.get(i).getTime() * 1000) < TimeWindow
								.getCurrentTime()) {
					Log.d("draw", "add");
					EnemyDrawableEntity eme = new EnemyDrawableEntity(arrayList
							.get(i).getEnemy(), getResources(),
							getPosition(arrayList.get(i).getEnemy()
									.getStartPosition()));
					WorldBox2D.getEnemyList().add(eme);
				}
			}
			for (int i = 0; i < WorldBox2D.getEnemyList().size(); i++) {
				EnemyDrawableEntity ede = WorldBox2D.getEnemyList().get(i);
				Enemy enemy = ede.getEnemy();

				if (compteur % (enemy.getShootTime() * 1000) < 30) {
					int typeW = enemy.giveWeapon();
					WeaponDrawableEntity wde;
					Vec2 positionShoot = new Vec2(ede.getPosition().x
							+ (ede.getSize().x / 2), ede.getPosition().y
							+ (ede.getSize().y / 2));
					if (typeW == 0) {
						wde = new WeaponDrawableEntity(new Missile(
								drawable.icon_missile, false), getResources(),
								positionShoot, new Vec2(0, 100));
					} else if (typeW == 1) {
						wde = new WeaponDrawableEntity(new Fireball(
								drawable.icon_fireball, false), getResources(),
								positionShoot, new Vec2(0, 100));
					} else if (typeW == 2) {
						wde = new WeaponDrawableEntity(new Shiboleet(
								drawable.icon_shiboleet, false),
								getResources(), positionShoot, new Vec2(0, 100));
					} else {
						wde = new WeaponDrawableEntity(new Laser(
								drawable.icon_missile, false), getResources(),
								positionShoot, new Vec2(0, 100));
					}
					WorldBox2D.addWeapon(wde);
				}

			}

			if (TimeWindow.getCurrentTime() / 1000 == scenario.getTime() - 1) {
				bossDrawableEntity = new BossDrawableEntity(boss,
						getResources(), new Vec2(GameActivity.SCREEN_WIDTH / 2,
								GameActivity.SCREEN_HEIGHT / 5));

				WorldBox2D.setBoss(bossDrawableEntity);
			}

			if (TimeWindow.getCurrentTime() / 1000 > scenario.getTime()) {

				if (compteur % (boss.getShoot() * 1000) < 30) {
					int typeW = boss.giveWeapon();
					WeaponDrawableEntity wde;
					Vec2 positionShoot = new Vec2(
							bossDrawableEntity.getPosition().x
									+ (bossDrawableEntity.getSize().x / 2),
							bossDrawableEntity.getPosition().y
									+ (bossDrawableEntity.getSize().y / 2));
					if (typeW == 0) {
						wde = new WeaponDrawableEntity(new Missile(
								drawable.icon_missile, false), getResources(),
								positionShoot, new Vec2(0, 100));
					} else if (typeW == 1) {
						wde = new WeaponDrawableEntity(new Fireball(
								drawable.icon_fireball, false), getResources(),
								positionShoot, new Vec2(0, 100));
					} else if (typeW == 2) {
						wde = new WeaponDrawableEntity(new Shiboleet(
								drawable.icon_shiboleet, false),
								getResources(), positionShoot, new Vec2(0, 100));
					} else {
						wde = new WeaponDrawableEntity(new Laser(
								drawable.icon_missile, false), getResources(),
								positionShoot, new Vec2(0, 1000));
					}
					WorldBox2D.addWeapon(wde);
				}
			}

			compteur = TimeWindow.getCurrentTime();

		}

		public Vec2 getPosition(StartPositionEnum startPosition) {
			if (startPosition.getValue().equals("top - left")) {
				return new Vec2(30, 30);
			} else if (startPosition.getValue().equals("top - center")) {
				return new Vec2(GameActivity.SCREEN_WIDTH / 2, 30);
			} else if (startPosition.getValue().equals("top - right")) {
				return new Vec2(GameActivity.SCREEN_WIDTH - 60, 30);
			} else if (startPosition.getValue().equals("center - left")) {
				return new Vec2(30, GameActivity.SCREEN_HEIGHT / 2);
			} else if (startPosition.getValue().equals("center")) {
				return new Vec2(GameActivity.SCREEN_WIDTH / 2,
						GameActivity.SCREEN_HEIGHT / 2);

			} else if (startPosition.getValue().equals("center - right")) {
				return new Vec2(GameActivity.SCREEN_WIDTH - 60,
						GameActivity.SCREEN_HEIGHT / 2);
			} else if (startPosition.getValue().equals("bottom - left")) {
				return new Vec2(30, GameActivity.SCREEN_HEIGHT - 60);
			} else if (startPosition.getValue().equals("bottom - center")) {
				return new Vec2(GameActivity.SCREEN_WIDTH / 2,
						GameActivity.SCREEN_HEIGHT - 60);
			} else {
				return new Vec2(GameActivity.SCREEN_WIDTH - 60,
						GameActivity.SCREEN_HEIGHT - 60);
			}
		}
	}

	public static int getFPS() {
		return fps;
	}

	private static int estimateFPS(long currentMillis) {
		float tmp = ++countPaint;
		fps = (int) (tmp / (currentMillis - millis) * 1000);
		millis = currentMillis;
		countPaint = 0;
		return fps;
	}

	public static void setGameViewStopped(boolean stopped) {
		GameView.isGameViewStopped = stopped;
	}

	public static boolean isGameViewStopped() {
		return GameView.isGameViewStopped;
	}

}
