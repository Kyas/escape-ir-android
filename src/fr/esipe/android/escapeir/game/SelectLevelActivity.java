package fr.esipe.android.escapeir.game;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.TextView;
import fr.esipe.android.escapeir.R;
import fr.esipe.android.escapeir.game.adapter.SelectLevelAdapter;

/**
 * Class SelectLevelActivity - Select a level to start the game
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 */
public class SelectLevelActivity extends Activity implements OnClickListener {

	
	private SelectLevelAdapter adapter;
	private List<String> listLevel;
	private ListView listView;
	private TextView textView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.game_select_level_activity);

	
		
		listView = (ListView) findViewById(R.id.game_select_level_list);
		listLevel = new ArrayList<String>();
		textView = (TextView) findViewById(R.id.game_select_level_list_empty);		
		
		getApplicationContext();
		File f=getApplicationContext().getDir("levels",Context.MODE_PRIVATE);
		String[] listFiles=f.list();
		for(int i=0;i<listFiles.length;i++){
			if(listFiles[i].contains(".xml")){
				listLevel.add(listFiles[i].substring(0,listFiles[i].length()-4));
			}
		}
		
		if(listLevel.size()!=0){
			textView.setText("");
		}
		
		adapter = new SelectLevelAdapter(this, listLevel);
		listView.setAdapter(adapter);
	}

	@Override
	public void onClick(View v) {

	}

}
