package fr.esipe.android.escapeir.game;

import android.content.Context;
import android.view.MotionEvent;

/**
 * Used for click event in the MenuActivity.
 * 
 * @author Celine Perillous <cperillous@etudiant.univ-mlv.fr>
 * @author Jeremy Lor <jlor@etudiant.univ-mlv.fr>
 * 
 */
public class MouseFactory {

	/**
	 * Event of the menu.
	 * 
	 * @param event
	 *            The event
	 * @return A number depending of the zone of a click.
	 */
	public static int eventOnMenuActivity(MotionEvent event) {
		int mouseX = (int) event.getX(), mouseY = (int) event.getY();
		if ((mouseX > 200 && mouseX < 430) && (mouseY > 280 && mouseY < 340)) {
			return 1;
		} else if ((mouseX > 200 && mouseX < 430)
				&& (mouseY > 365 && mouseY < 425)) {
			return 2;
		} else if ((mouseX > 200 && mouseX < 430)
				&& (mouseY > 465 && mouseY < 530)) {
			return 3;
		}
		return 0;
	}

	/**
	 * Event of the menu activity level.
	 * 
	 * @param event
	 *            The event
	 * @return A number depending of the zone of a click.
	 */
	public static int eventOnMenuActivityLevel(MotionEvent event) {
		int mouseX = (int) event.getX(), mouseY = (int) event.getY();
		if ((mouseX > 60 && mouseX < 265) && (mouseY > 165 && mouseY < 310)) {
			return 1;
		} else if ((mouseX > 320 && mouseX < 540)
				&& (mouseY > 250 && mouseY < 400)) {
			return 2;
		} else if ((mouseX > 110 && mouseX < 330)
				&& (mouseY > 405 && mouseY < 555)) {
			return 3;
		} else if ((mouseX > 410 && mouseX < 560)
				&& (mouseY > 515 && mouseY < 560)) {
			return 4;

		}
		return 0;
	}

	/**
	 * Event of the menu activity options.
	 * 
	 * @param event
	 *            The event
	 * @return A number depending of the zone of a click.
	 */
	public static boolean eventOnMenuActivityOptions(MotionEvent event) {
		int mouseX = (int) event.getX(), mouseY = (int) event.getY();
		if ((mouseX > 410 && mouseX < 560) && (mouseY > 515 && mouseY < 560)) {
			return true;
		}
		return false;
	}

	/**
	 * Event of the menu activity credits.
	 * 
	 * @param event
	 *            The event
	 * @return A number depending of the zone of a click.
	 */
	public static boolean eventOnMenuActivityCredits(MotionEvent event) {
		int mouseX = (int) event.getX(), mouseY = (int) event.getY();
		if ((mouseX > 190 && mouseX < 410) && (mouseY > 385 && mouseY < 440)) {
			return true;
		}
		return false;
	}

	/**
	 * Used for the weapon Bar.
	 * 
	 * @param event
	 *            The event.
	 * @return the weapon Active.
	 */
	public static int eventOnWeapon(Context context, MotionEvent event) {
		if (GameActivity.SCREEN_WIDTH >= 720) {
			int actual = -1;
			int mouseY = (int) event.getY();
			if (mouseY > GameActivity.SCREEN_HEIGHT / 2 - 200
					&& mouseY <= GameActivity.SCREEN_HEIGHT / 2 - 125) {
				actual = 0;
			} else if (mouseY > GameActivity.SCREEN_HEIGHT / 2 - 125
					&& mouseY <= GameActivity.SCREEN_HEIGHT / 2 - 50) {
				actual = 1;
			} else if (mouseY > GameActivity.SCREEN_HEIGHT / 2 - 50
					&& mouseY <= GameActivity.SCREEN_HEIGHT / 2 + 25) {
				actual = 2;
			} else if (mouseY > GameActivity.SCREEN_HEIGHT / 2 + 25
					&& mouseY <= GameActivity.SCREEN_HEIGHT / 2 + 110) {
				actual = 3;
			} else {
				// Error.
				System.out.println("error");
				actual = -1;
			}
			return actual;
		} else {
			int actual = -1;
			int mouseY = (int) event.getY();
			if (mouseY > GameActivity.SCREEN_HEIGHT / 2 - 40
					&& mouseY <= GameActivity.SCREEN_HEIGHT / 2 - 30) {
				actual = 0;
			} else if (mouseY > GameActivity.SCREEN_HEIGHT / 2 - 20
					&& mouseY <= GameActivity.SCREEN_HEIGHT / 2 - 10) {
				actual = 1;
			} else if (mouseY > GameActivity.SCREEN_HEIGHT / 2 - 10
					&& mouseY <= GameActivity.SCREEN_HEIGHT / 2 + 5) {
				actual = 2;
			} else if (mouseY > GameActivity.SCREEN_HEIGHT / 2 + 5
					&& mouseY <= GameActivity.SCREEN_HEIGHT / 2 + 20) {
				actual = 3;
			} else {
				// Error.
				System.out.println("error");
				actual = -1;
			}
			return actual;
		}
	}
}
