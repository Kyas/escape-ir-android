package fr.esipe.android.escapeir.game.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import fr.esipe.android.escapeir.R;
import fr.esipe.android.escapeir.api.Map;
import fr.esipe.android.escapeir.builder.XMLTools;
import fr.esipe.android.escapeir.builder.form.activity.CurrentLevel;
import fr.esipe.android.escapeir.game.GameActivity;

/**
 * Class SelectLevelAdapter - Display a row of the list of level (name +
 * description)
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 */
public class SelectLevelAdapter extends ArrayAdapter<String> {

	/**
	 * The Documents List.
	 */
	private List<String> levels;

	/**
	 * The Inflater.
	 */
	private LayoutInflater inflater;

	/**
	 * The View of the Document.
	 */
	private View rowView;

	/**
	 * Static class (doesn't depend on the instance of the current class). Used
	 * for store references.
	 */
	static class ViewHolder {
		public TextView nameView;
		public TextView descriptionView;
		public ImageView iconView;
	}

	public SelectLevelAdapter(Context context, List<String> levels) {
		super(context, R.layout.game_row_select_level, levels);
		this.levels = levels;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Log.d("debug", "ici");
		ViewHolder holder;
		rowView = convertView;

		if (rowView == null) {

			// Creation of the View object from the Layout ressource
			rowView = inflater.inflate(R.layout.game_row_select_level, null);

			holder = new ViewHolder();

			// Display of the corresponding name of the document
			holder.nameView = (TextView) rowView
					.findViewById(R.id.game_select_level_name);

			

			// Display of the corresponding icon of the document
			holder.iconView = (ImageView) rowView
					.findViewById(R.id.game_select_level_icon);

			rowView.setTag(holder);
		} else {
			holder = (ViewHolder) rowView.getTag();
		}

		// Get the identifying document by its position in the List
		final String level = levels.get(position);
		Log.d("debug", level);

		holder.nameView.setText(level);
		// TODO A développer sur la description
		// holder.descriptionView.setText("Player" + level.getPlayer());

		holder.iconView.setImageResource(R.drawable.icon_map_empty);
		// Get the identifying enemy by its position in the List
		try {
			ArrayList<Map> maps=XMLTools.fileToLevelLitleMap(getContext(), (String)getItem(position)+".xml");
			Random r=new Random();
			if(maps.size()>0){
				int a= Math.abs(r.nextInt()%maps.size());
				holder.iconView.setImageResource(maps.get(a).getPath());
			}
			
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		final int positionToLoad = position;

		holder.nameView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				CurrentLevel.removeCurrentLevel();
				try {
					CurrentLevel.setCurrentLevel(XMLTools.fileToLevel(
							getContext(), (String)getItem(positionToLoad) + ".xml"));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				Intent intent = new Intent(getContext(), GameActivity.class)
						.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
								| Intent.FLAG_ACTIVITY_NEW_TASK);
				getContext().startActivity(intent);
			}
		});

		holder.iconView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				CurrentLevel.removeCurrentLevel();
				try {
					CurrentLevel.setCurrentLevel(XMLTools.fileToLevel(
							getContext(),  (String)getItem(positionToLoad) + ".xml"));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				Intent intent = new Intent(getContext(), GameActivity.class)
						.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
								| Intent.FLAG_ACTIVITY_NEW_TASK);
				getContext().startActivity(intent);
			}
		});

		// TODO Ajouter l'icon dans le model Level.
		// if (level.getIcon != null) {
		// File f = new File(level.getIcon());
		// if (f.isFile()) {
		// Uri uri = Uri.fromFile(f);
		// holder.iconView.setImageURI(uri);
		// }
		// }
		return rowView;
	}
}
