package fr.esipe.android.escapeir.game;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import fr.esipe.android.escapeir.EscapeIRApp;
import fr.esipe.android.escapeir.box2d.WorldBox2D;
import fr.esipe.android.escapeir.menu.GameOverActivity;
import fr.esipe.android.escapeir.menu.VictoryActivity;

/**
 * Class GameActivity - Scenario activity
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 */
public class GameActivity extends Activity {

	/**
	 * The screen size.
	 */
	private static DisplayMetrics metrics = new DisplayMetrics();

	/**
	 * The screen width.
	 */
	public static int SCREEN_WIDTH;

	/**
	 * The screen height.
	 */
	public static int SCREEN_HEIGHT;

	/**
	 * The gameView.
	 */
	private GameView gameView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		try {
			getWindowManager().getDefaultDisplay().getMetrics(metrics);
			SCREEN_HEIGHT = metrics.heightPixels;
			SCREEN_WIDTH = metrics.widthPixels;

			// making it full screen
			getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
					WindowManager.LayoutParams.FLAG_FULLSCREEN);

			gameView = new GameView(this);

			// set our MainGamePanel as the View
			setContentView(gameView);

		} catch (Exception e) {
			System.out.println("Game stopped.");
		}

	}

	@Override
	protected void onResume() {
		super.onResume();

		if (WorldBox2D.isWin()) {
			finish();

			startActivity(new Intent(this, VictoryActivity.class)
					.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_NEW_TASK));
		}

		if (WorldBox2D.isLose()) {
			finish();

			startActivity(new Intent(this, GameOverActivity.class)
					.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_NEW_TASK));
		}

		if (GameView.isGameViewStopped()) {
			finish();

			startActivity(new Intent(this, EscapeIRApp.class)
					.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_NEW_TASK));
		}
	}

	@Override
	protected void onPause() {
		super.onPause();

		GameView.setGameViewStopped(true);
	}
}
