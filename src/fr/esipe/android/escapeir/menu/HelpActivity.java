package fr.esipe.android.escapeir.menu;

import android.app.Activity;
import android.os.Bundle;
import fr.esipe.android.escapeir.R;

/**
 * Class HelpActivity - An activity to help to the user how to play the game
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 */
public class HelpActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu_help_activity);
	}

}
