package fr.esipe.android.escapeir.menu;

import android.app.Activity;
import android.os.Bundle;
import fr.esipe.android.escapeir.R;

/**
 * Class CreditsActivity - Shows credits
 * 
 * @author J. LOR C. PERILLOUS
 * @version 1.0.0
 */
public class CreditsActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu_credits_activity);
	}

}