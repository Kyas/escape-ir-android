package fr.esipe.android.escapeir.menu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import fr.esipe.android.escapeir.EscapeIRApp;
import fr.esipe.android.escapeir.R;

public class GameOverActivity extends Activity implements OnClickListener {

	private Button buttonMenuEscape;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu_gameover_activity);

		buttonMenuEscape = (Button) findViewById(R.id.menu_button_escapeir);
		buttonMenuEscape.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		startActivity(new Intent(this, EscapeIRApp.class)
				.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
						| Intent.FLAG_ACTIVITY_NEW_TASK));
	}

}
