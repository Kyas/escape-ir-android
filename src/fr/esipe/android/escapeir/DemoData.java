package fr.esipe.android.escapeir;

import java.util.ArrayList;
import java.util.List;

import fr.esipe.android.escapeir.api.Enemy;
import fr.esipe.android.escapeir.api.Map;
import fr.esipe.android.escapeir.api.StartPositionEnum;

public class DemoData {

	public static List<Map> maps;
	public static List<Map> mapsImport;

	public static void init() {
		maps = demoMaps();
		mapsImport = demoMapsImport();
	}

	/**
	 * DEMO : Ajout de maps pour tester.
	 */
	public static List<Map> demoMaps() {
		List<Map> demoListMaps = new ArrayList<Map>();
		Map map1 = new Map("earth_mini", R.drawable.earth_mini);
		Map map2 = new Map("moon_mini", R.drawable.moon_mini);
		Map map3 = new Map("jupiter_mini", R.drawable.jupiter_mini);
		demoListMaps.add(map1);
		demoListMaps.add(map2);
		demoListMaps.add(map3);

		return demoListMaps;
	}

	public static List<Map> demoMapsImport() {
		List<Map> demoListMapsImport = new ArrayList<Map>();
		Map map1 = new Map("earth_mini", R.drawable.earth_mini);
		Map map2 = new Map("moon_mini", R.drawable.moon_mini);
		Map map3 = new Map("jupiter_mini", R.drawable.jupiter_mini);
		demoListMapsImport.add(map1);
		demoListMapsImport.add(map2);
		demoListMapsImport.add(map3);

		return demoListMapsImport;
	}

}
