package fr.esipe.android.util;

import android.graphics.Canvas;
import android.graphics.Rect;
import fr.esipe.android.escapeir.api.Map;

/**
 * This class is used to do a Parallax scrolling for our differents maps.
 * 
 * @author Celine Perillous <cperillous@etudiant.univ-mlv.fr>
 * @author Jeremy Lor <jlor@etudiant.univ-mlv.fr>
 */
public class ScrollMap {

	/** Layers used to execute parallax scrolling. */
	private Layer layer;

	/**
	 * Creates a new parallax engine.
	 * 
	 * @param pScreenHeight
	 *            Screen height in pixels.
	 * @param pDy
	 *            Movement factor in pixels.
	 * @param pImages
	 *            Images used to compose parallax layers.
	 * @param pVy
	 *            Velocity for layers movement.
	 * @param gap
	 *            Gap between layers.
	 */
	public ScrollMap(int pScreenHeight, float pDy, Map pImages, float pVy,
			float gap) {
		this(pScreenHeight, pDy, pImages, pVy, 0f, gap);
	}

	/**
	 * Creates a new parallax scrolling engine.
	 * 
	 * @param pScreenHeight
	 *            Screen width in pixels.
	 * @param pDy
	 *            Movement factor in pixels.
	 * @param pImages
	 *            Images used to compose parallax layers.
	 * @param pVy
	 *            Velocity for layers movement.
	 * @param pY
	 *            Y position to render the image layers.
	 */
	public ScrollMap(int pScreenHeight, float pDy, Map pImages, float pVy,
			float pY, float gap) {
		this.layer = new Layer(pScreenHeight, 0, pY, pImages, pVy * pDy, gap);
	}

	/**
	 * Move layers to the top.
	 */
	public void setMoveTop() {
		this.layer.setTopDirection();
	}

	/**
	 * Stop layers movement.
	 */
	public void stop() {
		this.layer.stop();
	}

	/**
	 * Move layers.
	 */
	public void move() {
		this.layer.move();
	}

	/**
	 * The display order is important. Display layers from the back to the front
	 * of the scene.
	 * 
	 * @param g
	 *            Graphics context.
	 * @throws Exception
	 */
	public void render(Canvas canvas) throws Exception {
		this.layer.render(canvas);
	}

	/**
	 * Represents an vertical parallax layer.
	 * 
	 */
	private class Layer {

		/** Image for the layer. */
		private Map image;

		/** Width of the image in pixels. */
		private int width;

		/** Height of the image in pixels. */
		private int height;

		/** Height of the screen in pixels. */
		private int screenHeight;

		private Rect src;

		private Rect dst;

		/**
		 * X position in screen where the bottom side of the image should be
		 * drawn.
		 */
		private float x;

		/**
		 * Y position in screen where the bottom side of the image should be
		 * drawn.
		 */
		private float y;

		/** Number of pixels the layer will move with the height. */
		private float dy;

		/** Gap between layers. */
		private float gap;

		/** Flag indicating if the layer is moving to the top. */
		private boolean isMovingTop;

		/**
		 * Creates a new layer at origin (0,0).
		 * 
		 * @param pScreenHeight
		 *            Screen height in pixels.
		 * @param pImage
		 *            Image for the layer.
		 * @param pDy
		 *            Movement factor in pixels.
		 */
		private Layer(int pScreenHeight, Map pImage, float pDy) {
			this(pScreenHeight, 0, 0, pImage, pDy, 0);
		}

		/**
		 * Creates a new layer in a specific (x,y) position.
		 * 
		 * @param pScreenHeight
		 *            Screen width in pixels.
		 * @param pX
		 *            The x position in the screen where the start of the image
		 *            should be drawn.
		 * @param pY
		 *            The y position in the screen where the start of the image
		 *            should be drawn. It can range between -height to height-1
		 *            , so can have a value beyond the confines of the screen
		 *            (0-pScreenHeight). <br>
		 *            As y varies, the on-screen layer will usually be a
		 *            combination of its tail followed by its head.
		 * @param pImage
		 *            Image for the layer.
		 * @param pDy
		 *            Movement factor in pixels.
		 */
		private Layer(int pScreenHeight, float pX, float pY, Map pImage,
				float pDy, float gap) {
			this.screenHeight = pScreenHeight;
			this.image = pImage;
			this.width = pImage.getBitmap().getWidth();
			this.height = pImage.getBitmap().getHeight();
			this.dy = pDy;
			this.isMovingTop = false;
			this.x = pX;
			this.y = pY;
			this.gap = gap;
			this.src = new Rect();
			this.dst = new Rect();
		}

		/**
		 * Make the layer move to the top.
		 */
		private void setTopDirection() {
			this.isMovingTop = true;
		}

		/**
		 * Stop layer movement.
		 */
		private void stop() {
			this.isMovingTop = false;
		}

		/**
		 * Increment the y value depending on the movement flags. It can range
		 * between -height to height-1, which is the height of the image.
		 */
		private void move() {
			if (isMovingTop)
				this.y = (this.y - this.dy) % (this.height - gap);
		}

		/**
		 * Performs the layer rendering.
		 * 
		 * @param canvas
		 *            Graphics context.
		 * @throws Exception
		 */
		private void render(Canvas canvas) throws Exception {
			if (y == 0) {
				// Draws image head at (0,0).
				draw(canvas, screenHeight, 0, 0, screenHeight);
			} else if ((y > 0) && (y < screenHeight)) {
				// Draws image tail at (0,0) and image head at (y,0).
				// Tail.
				draw(canvas, 0, y, height - y, height);
				// Head.
				draw(canvas, y, screenHeight, 0, screenHeight - y);
			} else if (y >= screenHeight) {
				// Draws only image tail at (0,0).
				draw(canvas, 0, screenHeight, height - y, height - y
						+ screenHeight);
			} else if ((y < 0) && (y >= screenHeight - height)) {
				// Draws only image body.
				draw(canvas, 0, screenHeight, -y, screenHeight - y);
			} else if (y < screenHeight - height) {
				// Draws image tail at (0,0) and image head at (height+y,0)
				// Tail.
				draw(canvas, 0, height + y, -y, height);
				// Head.
				draw(canvas, gap + height + y, gap + screenHeight, 0,
						screenHeight - height - y);
			}
		}

		/**
		 * Actually draws the layer.
		 * 
		 * @param g
		 *            Graphics context.
		 * @param destY1
		 *            Destination Y1 position in the graphics context.
		 * @param destY2
		 *            Destination Y2 position in the graphics context.
		 * @param imageY1
		 *            Source Y1 position of image's layer.
		 * @param imageY2
		 *            Source Y2 position of image's layer.
		 */
		private void draw(Canvas canvas, float destY1, float destY2,
				float imageY1, float imageY2) throws Exception {
			src.set(0, (int) (imageY1), (int) (width), (int) (imageY2));
			dst.set((int) (x), (int) (destY1), (int) (x + width),
					(int) (destY2));

			canvas.drawARGB(0, 0, 0, 0);
			canvas.drawBitmap(image.getBitmap(), src, dst, null);

		}
	}
}
